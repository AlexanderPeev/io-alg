Benchmarking for index = 0, fanout: 8, nodesize: 50000, buffer: 25000, block: 16 [34mAverage result: 49.952824[0m
Benchmarking for index = 1, fanout: 8, nodesize: 50000, buffer: 25000, block: 64 [34mAverage result: 50.661847[0m
Benchmarking for index = 2, fanout: 8, nodesize: 50000, buffer: 25000, block: 128 [34mAverage result: 50.669291[0m
Benchmarking for index = 3, fanout: 8, nodesize: 50000, buffer: 37500, block: 16 [34mAverage result: 43.952174[0m
Benchmarking for index = 4, fanout: 8, nodesize: 50000, buffer: 37500, block: 64 [34mAverage result: 44.618161[0m
Benchmarking for index = 5, fanout: 8, nodesize: 50000, buffer: 37500, block: 128 [34mAverage result: 44.655909[0m
Benchmarking for index = 6, fanout: 8, nodesize: 50000, buffer: 50000, block: 16 [34mAverage result: 36.942511[0m
Benchmarking for index = 7, fanout: 8, nodesize: 50000, buffer: 50000, block: 64 [34mAverage result: 37.015511[0m
Benchmarking for index = 8, fanout: 8, nodesize: 50000, buffer: 50000, block: 128 [34mAverage result: 37.163410[0m
Benchmarking for index = 27, fanout: 8, nodesize: 100000, buffer: 50000, block: 16 [34mAverage result: 47.173988[0m
Benchmarking for index = 28, fanout: 8, nodesize: 100000, buffer: 50000, block: 64 [34mAverage result: 47.318356[0m
Benchmarking for index = 29, fanout: 8, nodesize: 100000, buffer: 50000, block: 128 [34mAverage result: 47.621699[0m
Benchmarking for index = 30, fanout: 8, nodesize: 100000, buffer: 75000, block: 16 [34mAverage result: 41.696350[0m
Benchmarking for index = 31, fanout: 8, nodesize: 100000, buffer: 75000, block: 64 [34mAverage result: 41.745188[0m
Benchmarking for index = 32, fanout: 8, nodesize: 100000, buffer: 75000, block: 128 [34mAverage result: 42.069039[0m
Benchmarking for index = 33, fanout: 8, nodesize: 100000, buffer: 100000, block: 16 [34mAverage result: 33.837446[0m
Benchmarking for index = 34, fanout: 8, nodesize: 100000, buffer: 100000, block: 64 [34mAverage result: 33.982917[0m
Benchmarking for index = 35, fanout: 8, nodesize: 100000, buffer: 100000, block: 128 [34mAverage result: 34.181549[0m
Benchmarking for index = 51, fanout: 8, nodesize: 150000, buffer: 75000, block: 16 [34mAverage result: 47.217407[0m
Benchmarking for index = 52, fanout: 8, nodesize: 150000, buffer: 75000, block: 64 [34mAverage result: 47.023315[0m
Benchmarking for index = 53, fanout: 8, nodesize: 150000, buffer: 75000, block: 128 [34mAverage result: 47.550883[0m
Benchmarking for index = 54, fanout: 8, nodesize: 150000, buffer: 100000, block: 16 [34mAverage result: 43.801338[0m
Benchmarking for index = 55, fanout: 8, nodesize: 150000, buffer: 100000, block: 64 [34mAverage result: 43.980281[0m
Benchmarking for index = 56, fanout: 8, nodesize: 150000, buffer: 100000, block: 128 [34mAverage result: 44.309395[0m
Benchmarking for index = 57, fanout: 8, nodesize: 150000, buffer: 150000, block: 16 [34mAverage result: 32.302448[0m
Benchmarking for index = 58, fanout: 8, nodesize: 150000, buffer: 150000, block: 64 [34mAverage result: 32.309212[0m
Benchmarking for index = 59, fanout: 8, nodesize: 150000, buffer: 150000, block: 128 [34mAverage result: 32.775603[0m
Benchmarking for index = 75, fanout: 8, nodesize: 200000, buffer: 100000, block: 16 [34mAverage result: 45.413177[0m
Benchmarking for index = 76, fanout: 8, nodesize: 200000, buffer: 100000, block: 64 [34mAverage result: 45.524861[0m
Benchmarking for index = 77, fanout: 8, nodesize: 200000, buffer: 100000, block: 128 [34mAverage result: 45.823897[0m
Benchmarking for index = 78, fanout: 8, nodesize: 200000, buffer: 150000, block: 16 [34mAverage result: 40.164133[0m
Benchmarking for index = 79, fanout: 8, nodesize: 200000, buffer: 150000, block: 64 [34mAverage result: 40.334365[0m
Benchmarking for index = 80, fanout: 8, nodesize: 200000, buffer: 150000, block: 128 [34mAverage result: 40.601498[0m
Benchmarking for index = 81, fanout: 8, nodesize: 200000, buffer: 200000, block: 16 [34mAverage result: 32.249986[0m
Benchmarking for index = 82, fanout: 8, nodesize: 200000, buffer: 200000, block: 64 [34mAverage result: 32.239372[0m
Benchmarking for index = 83, fanout: 8, nodesize: 200000, buffer: 200000, block: 128 [34mAverage result: 32.580709[0m
Benchmarking for index = 84, fanout: 12, nodesize: 50000, buffer: 25000, block: 16 [34mAverage result: 46.873746[0m
Benchmarking for index = 85, fanout: 12, nodesize: 50000, buffer: 25000, block: 64 [34mAverage result: 46.975086[0m
Benchmarking for index = 86, fanout: 12, nodesize: 50000, buffer: 25000, block: 128 [34mAverage result: 47.459275[0m
Benchmarking for index = 87, fanout: 12, nodesize: 50000, buffer: 37500, block: 16 [34mAverage result: 40.950067[0m
Benchmarking for index = 88, fanout: 12, nodesize: 50000, buffer: 37500, block: 64 [34mAverage result: 41.068072[0m
Benchmarking for index = 89, fanout: 12, nodesize: 50000, buffer: 37500, block: 128 [34mAverage result: 41.519047[0m
Benchmarking for index = 90, fanout: 12, nodesize: 50000, buffer: 50000, block: 16 [34mAverage result: 33.959576[0m
Benchmarking for index = 91, fanout: 12, nodesize: 50000, buffer: 50000, block: 64 [34mAverage result: 34.019749[0m
Benchmarking for index = 92, fanout: 12, nodesize: 50000, buffer: 50000, block: 128 [34mAverage result: 34.492987[0m
Benchmarking for index = 111, fanout: 12, nodesize: 100000, buffer: 50000, block: 16 [34mAverage result: 45.791841[0m
Benchmarking for index = 112, fanout: 12, nodesize: 100000, buffer: 50000, block: 64 [34mAverage result: 45.943960[0m
Benchmarking for index = 113, fanout: 12, nodesize: 100000, buffer: 50000, block: 128 [34mAverage result: 46.576825[0m
Benchmarking for index = 114, fanout: 12, nodesize: 100000, buffer: 75000, block: 16 [34mAverage result: 38.455942[0m
Benchmarking for index = 115, fanout: 12, nodesize: 100000, buffer: 75000, block: 64 [34mAverage result: 38.602881[0m
Benchmarking for index = 116, fanout: 12, nodesize: 100000, buffer: 75000, block: 128 [34mAverage result: 39.099507[0m
Benchmarking for index = 117, fanout: 12, nodesize: 100000, buffer: 100000, block: 16 [34mAverage result: 32.544559[0m
Benchmarking for index = 118, fanout: 12, nodesize: 100000, buffer: 100000, block: 64 [34mAverage result: 32.885599[0m
Benchmarking for index = 119, fanout: 12, nodesize: 100000, buffer: 100000, block: 128 [34mAverage result: 33.329517[0m
Benchmarking for index = 135, fanout: 12, nodesize: 150000, buffer: 75000, block: 16 [34mAverage result: 42.498503[0m
Benchmarking for index = 136, fanout: 12, nodesize: 150000, buffer: 75000, block: 64 [34mAverage result: 42.930075[0m
Benchmarking for index = 137, fanout: 12, nodesize: 150000, buffer: 75000, block: 128 [34mAverage result: 43.398016[0m
Benchmarking for index = 138, fanout: 12, nodesize: 150000, buffer: 100000, block: 16 [34mAverage result: 41.922636[0m
Benchmarking for index = 139, fanout: 12, nodesize: 150000, buffer: 100000, block: 64 [34mAverage result: 42.151880[0m
Benchmarking for index = 140, fanout: 12, nodesize: 150000, buffer: 100000, block: 128 [34mAverage result: 42.837371[0m
Benchmarking for index = 141, fanout: 12, nodesize: 150000, buffer: 150000, block: 16 [34mAverage result: 31.686969[0m
Benchmarking for index = 142, fanout: 12, nodesize: 150000, buffer: 150000, block: 64 [34mAverage result: 31.985077[0m
Benchmarking for index = 143, fanout: 12, nodesize: 150000, buffer: 150000, block: 128 [34mAverage result: 32.393500[0m
Benchmarking for index = 159, fanout: 12, nodesize: 200000, buffer: 100000, block: 16 [34mAverage result: 43.939487[0m
Benchmarking for index = 160, fanout: 12, nodesize: 200000, buffer: 100000, block: 64 [34mAverage result: 44.234481[0m
Benchmarking for index = 161, fanout: 12, nodesize: 200000, buffer: 100000, block: 128 [34mAverage result: 44.664288[0m
Benchmarking for index = 162, fanout: 12, nodesize: 200000, buffer: 150000, block: 16 [34mAverage result: 38.434232[0m
Benchmarking for index = 163, fanout: 12, nodesize: 200000, buffer: 150000, block: 64 [34mAverage result: 38.709413[0m
Benchmarking for index = 164, fanout: 12, nodesize: 200000, buffer: 150000, block: 128 [34mAverage result: 39.121377[0m
Benchmarking for index = 165, fanout: 12, nodesize: 200000, buffer: 200000, block: 16 [34mAverage result: 32.447635[0m
Benchmarking for index = 166, fanout: 12, nodesize: 200000, buffer: 200000, block: 64 [34mAverage result: 32.564209[0m
Benchmarking for index = 167, fanout: 12, nodesize: 200000, buffer: 200000, block: 128
[34mAverage result: 32.875564[0m
Benchmarking for index = 168, fanout: 32, nodesize: 50000, buffer: 25000, block: 16 [34mAverage result: 44.163462[0m
Benchmarking for index = 169, fanout: 32, nodesize: 50000, buffer: 25000, block: 64 [34mAverage result: 45.600900[0m
Benchmarking for index = 170, fanout: 32, nodesize: 50000, buffer: 25000, block: 128 [34mAverage result: 45.791156[0m
Benchmarking for index = 171, fanout: 32, nodesize: 50000, buffer: 37500, block: 16 [34mAverage result: 38.986204[0m
Benchmarking for index = 172, fanout: 32, nodesize: 50000, buffer: 37500, block: 64 [34mAverage result: 40.443093[0m
Benchmarking for index = 173, fanout: 32, nodesize: 50000, buffer: 37500, block: 128 [34mAverage result: 40.578008[0m
Benchmarking for index = 174, fanout: 32, nodesize: 50000, buffer: 50000, block: 16 [34mAverage result: 31.516280[0m
Benchmarking for index = 175, fanout: 32, nodesize: 50000, buffer: 50000, block: 64 [34mAverage result: 33.064631[0m
Benchmarking for index = 176, fanout: 32, nodesize: 50000, buffer: 50000, block: 128 [34mAverage result: 33.167322[0m
Benchmarking for index = 195, fanout: 32, nodesize: 100000, buffer: 50000, block: 16 [34mAverage result: 42.269312[0m
Benchmarking for index = 196, fanout: 32, nodesize: 100000, buffer: 50000, block: 64 [34mAverage result: 43.409236[0m
Benchmarking for index = 197, fanout: 32, nodesize: 100000, buffer: 50000, block: 128 [34mAverage result: 43.797428[0m
Benchmarking for index = 198, fanout: 32, nodesize: 100000, buffer: 75000, block: 16 [34mAverage result: 36.710454[0m
Benchmarking for index = 199, fanout: 32, nodesize: 100000, buffer: 75000, block: 64 [34mAverage result: 37.811645[0m
Benchmarking for index = 200, fanout: 32, nodesize: 100000, buffer: 75000, block: 128 [34mAverage result: 38.127618[0m
Benchmarking for index = 201, fanout: 32, nodesize: 100000, buffer: 100000, block: 16 [34mAverage result: 30.460880[0m
Benchmarking for index = 202, fanout: 32, nodesize: 100000, buffer: 100000, block: 64 [34mAverage result: 31.641545[0m
Benchmarking for index = 203, fanout: 32, nodesize: 100000, buffer: 100000, block: 128 [34mAverage result: 32.026314[0m
Benchmarking for index = 219, fanout: 32, nodesize: 150000, buffer: 75000, block: 16 [34mAverage result: 42.097780[0m
Benchmarking for index = 220, fanout: 32, nodesize: 150000, buffer: 75000, block: 64 [34mAverage result: 42.840954[0m
Benchmarking for index = 221, fanout: 32, nodesize: 150000, buffer: 75000, block: 128 [34mAverage result: 43.416839[0m
Benchmarking for index = 222, fanout: 32, nodesize: 150000, buffer: 100000, block: 16 [34mAverage result: 39.150165[0m
Benchmarking for index = 223, fanout: 32, nodesize: 150000, buffer: 100000, block: 64 [34mAverage result: 39.834728[0m
Benchmarking for index = 224, fanout: 32, nodesize: 150000, buffer: 100000, block: 128 [34mAverage result: 40.440890[0m
Benchmarking for index = 225, fanout: 32, nodesize: 150000, buffer: 150000, block: 16 [34mAverage result: 28.788469[0m
Benchmarking for index = 226, fanout: 32, nodesize: 150000, buffer: 150000, block: 64 [34mAverage result: 29.491589[0m
Benchmarking for index = 227, fanout: 32, nodesize: 150000, buffer: 150000, block: 128 [34mAverage result: 30.140625[0m
Benchmarking for index = 243, fanout: 32, nodesize: 200000, buffer: 100000, block: 16 [34mAverage result: 41.393832[0m
Benchmarking for index = 244, fanout: 32, nodesize: 200000, buffer: 100000, block: 64 [34mAverage result: 41.739316[0m
Benchmarking for index = 245, fanout: 32, nodesize: 200000, buffer: 100000, block: 128 [34mAverage result: 42.331526[0m
Benchmarking for index = 246, fanout: 32, nodesize: 200000, buffer: 150000, block: 16 [34mAverage result: 35.217181[0m
Benchmarking for index = 247, fanout: 32, nodesize: 200000, buffer: 150000, block: 64 [34mAverage result: 35.703564[0m
Benchmarking for index = 248, fanout: 32, nodesize: 200000, buffer: 150000, block: 128 [34mAverage result: 36.211803[0m
Benchmarking for index = 249, fanout: 32, nodesize: 200000, buffer: 200000, block: 16 [34mAverage result: 29.473109[0m
Benchmarking for index = 250, fanout: 32, nodesize: 200000, buffer: 200000, block: 64 [34mAverage result: 29.924426[0m
Benchmarking for index = 251, fanout: 32, nodesize: 200000, buffer: 200000, block: 128 Average result: 30.372333[0m


49.952824 | 50.661847 | 50.669291 | 43.952174 | 44.618161 | 44.655909 | 36.942511 | 37.015511 | 37.163410 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 |
-1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | 47.173988 | 47.318356 | 47.621699 | 41.696350 | 41.745188 | 42.069039 | 33.837446 | 33.982917 | 34.181549 | -1.000000 | -1.000000 | -1.000000 | -1.000000 |
-1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | 47.217407 | 47.023315 | 47.550883 | 43.801338 | 43.980281 | 44.309395 | 32.302448 | 32.309212 | 32.775603 |
-1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | 45.413177 | 45.524861 | 45.823897 | 40.164133 | 40.334365 |
40.601498 | 32.249986 | 32.239372 | 32.580709 | 46.873746 | 46.975086 | 47.459275 | 40.950067 | 41.068072 | 41.519047 | 33.959576 | 34.019749 | 34.492987 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 |
-1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | 45.791841 | 45.943960 | 46.576825 | 38.455942 | 38.602881 | 39.099507 | 32.544559 | 32.885599 | 33.329517 |
-1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | 42.498503 | 42.930075 | 43.398016 | 41.922636 | 42.151880 |
42.837371 | 31.686969 | 31.985077 | 32.393500 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | 43.939487 |
44.234481 | 44.664288 | 38.434232 | 38.709413 | 39.121377 | 32.447635 | 32.564209 | 32.875564 | 44.163462 | 45.600900 | 45.791156 | 38.986204 | 40.443093 | 40.578008 | 31.516280 | 33.064631 | 33.167322 | -1.000000 | -1.000000 | -1.000000 |
-1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | 42.269312 | 43.409236 | 43.797428 | 36.710454 | 37.811645 |
38.127618 | 30.460880 | 31.641545 | 32.026314 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | 42.097780 |
42.840954 | 43.416839 | 39.150165 | 39.834728 | 40.440890 | 28.788469 | 29.491589 | 30.140625 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 |
-1.000000 | -1.000000 | -1.000000 | 41.393832 | 41.739316 | 42.331526 | 35.217181 | 35.703564 | 36.211803 | 29.473109 | 29.924426 | 30.372333 |
[32mDone benchmarking heap[0m
[34m##########################################################################################[0m
[32mDone benchmarking streams[0m
