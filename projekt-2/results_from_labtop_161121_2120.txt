Running Benchmarks
Initializing streams...
Initialized streams...
##########################################################################################
Benchmarking heap
Mon Nov 21 20:18:02 2016
Benchmarking for index = 0, fanout: 4, nodesize: 50000, buffer: 25000, block: 4
------
Tmp result 0
45.201869
Average result: 45.201869
Mon Nov 21 20:18:48 2016
Benchmarking for index = 1, fanout: 4, nodesize: 50000, buffer: 25000, block: 16
------
Tmp result 0
51.061302
Average result: 51.061302
Mon Nov 21 20:19:39 2016
Benchmarking for index = 2, fanout: 4, nodesize: 50000, buffer: 25000, block: 64
------
Tmp result 0
39.739210
Average result: 39.739210
Mon Nov 21 20:20:19 2016
Benchmarking for index = 3, fanout: 4, nodesize: 50000, buffer: 37500, block: 4
------
Tmp result 0
42.466018
Average result: 42.466018
Mon Nov 21 20:21:02 2016
Benchmarking for index = 4, fanout: 4, nodesize: 50000, buffer: 37500, block: 16
------
Tmp result 0
36.486713
Average result: 36.486713
Mon Nov 21 20:21:39 2016
Benchmarking for index = 5, fanout: 4, nodesize: 50000, buffer: 37500, block: 64
------
Tmp result 0
34.793800
Average result: 34.793800
Mon Nov 21 20:22:13 2016
Benchmarking for index = 6, fanout: 4, nodesize: 50000, buffer: 50000, block: 4
------
Tmp result 0
29.021103
Average result: 29.021103
Mon Nov 21 20:22:42 2016
Benchmarking for index = 7, fanout: 4, nodesize: 50000, buffer: 50000, block: 16
------
Tmp result 0
28.942828
Average result: 28.942828
Mon Nov 21 20:23:11 2016
Benchmarking for index = 8, fanout: 4, nodesize: 50000, buffer: 50000, block: 64
------
Tmp result 0
29.388176
Average result: 29.388176
Mon Nov 21 20:23:41 2016
Benchmarking for index = 27, fanout: 4, nodesize: 100000, buffer: 50000, block: 4
------
Tmp result 0
35.575126
Average result: 35.575126
Mon Nov 21 20:24:16 2016
Benchmarking for index = 28, fanout: 4, nodesize: 100000, buffer: 50000, block: 16
------
Tmp result 0
35.373363
Average result: 35.373363
Mon Nov 21 20:24:52 2016
Benchmarking for index = 29, fanout: 4, nodesize: 100000, buffer: 50000, block: 64
------
Tmp result 0
35.643106
Average result: 35.643106
Mon Nov 21 20:25:27 2016
Benchmarking for index = 30, fanout: 4, nodesize: 100000, buffer: 75000, block: 4
------
Tmp result 0
31.672364
Average result: 31.672364
Mon Nov 21 20:25:59 2016
Benchmarking for index = 31, fanout: 4, nodesize: 100000, buffer: 75000, block: 16
------
Tmp result 0
52.309050
Average result: 52.309050
Mon Nov 21 20:26:56 2016
Benchmarking for index = 32, fanout: 4, nodesize: 100000, buffer: 75000, block: 64
------
Tmp result 0
42.405278
Average result: 42.405278
Mon Nov 21 20:27:41 2016
Benchmarking for index = 33, fanout: 4, nodesize: 100000, buffer: 100000, block: 4
------
Tmp result 0
27.675864
Average result: 27.675864
Mon Nov 21 20:28:08 2016
Benchmarking for index = 34, fanout: 4, nodesize: 100000, buffer: 100000, block: 16
------
Tmp result 0
26.068632
Average result: 26.068632
Mon Nov 21 20:28:34 2016
Benchmarking for index = 35, fanout: 4, nodesize: 100000, buffer: 100000, block: 64
------
Tmp result 0
35.254626
Average result: 35.254626
Mon Nov 21 20:29:11 2016
Benchmarking for index = 51, fanout: 4, nodesize: 150000, buffer: 75000, block: 4
------
Tmp result 0
38.519658
Average result: 38.519658
Mon Nov 21 20:29:50 2016
Benchmarking for index = 52, fanout: 4, nodesize: 150000, buffer: 75000, block: 16
------
Tmp result 0
40.266782
Average result: 40.266782
Mon Nov 21 20:30:31 2016
Benchmarking for index = 53, fanout: 4, nodesize: 150000, buffer: 75000, block: 64
------
Tmp result 0
35.711164
Average result: 35.711164
Mon Nov 21 20:31:06 2016
Benchmarking for index = 54, fanout: 4, nodesize: 150000, buffer: 100000, block: 4
------
Tmp result 0
32.597970
Average result: 32.597970
Mon Nov 21 20:31:39 2016
Benchmarking for index = 55, fanout: 4, nodesize: 150000, buffer: 100000, block: 16
------
Tmp result 0
32.444145
Average result: 32.444145
Mon Nov 21 20:32:11 2016
Benchmarking for index = 56, fanout: 4, nodesize: 150000, buffer: 100000, block: 64
------
Tmp result 0
32.958924
Average result: 32.958924
Mon Nov 21 20:32:44 2016
Benchmarking for index = 57, fanout: 4, nodesize: 150000, buffer: 150000, block: 4
------
Tmp result 0
26.161784
Average result: 26.161784
Mon Nov 21 20:33:11 2016
Benchmarking for index = 58, fanout: 4, nodesize: 150000, buffer: 150000, block: 16
------
Tmp result 0
25.067555
Average result: 25.067555
Mon Nov 21 20:33:36 2016
Benchmarking for index = 59, fanout: 4, nodesize: 150000, buffer: 150000, block: 64
------
Tmp result 0
28.322507
Average result: 28.322507
Mon Nov 21 20:34:04 2016
Benchmarking for index = 75, fanout: 4, nodesize: 200000, buffer: 100000, block: 4
------
Tmp result 0
34.276807
Average result: 34.276807
Mon Nov 21 20:34:39 2016
Benchmarking for index = 76, fanout: 4, nodesize: 200000, buffer: 100000, block: 16
------
Tmp result 0
34.116106
Average result: 34.116106
Mon Nov 21 20:35:13 2016
Benchmarking for index = 77, fanout: 4, nodesize: 200000, buffer: 100000, block: 64
------
Tmp result 0
34.100341
Average result: 34.100341
Mon Nov 21 20:35:47 2016
Benchmarking for index = 78, fanout: 4, nodesize: 200000, buffer: 150000, block: 4
------
Tmp result 0
29.233338
Average result: 29.233338
Mon Nov 21 20:36:16 2016
Benchmarking for index = 79, fanout: 4, nodesize: 200000, buffer: 150000, block: 16
------
Tmp result 0
30.672205
Average result: 30.672205
Mon Nov 21 20:36:47 2016
Benchmarking for index = 80, fanout: 4, nodesize: 200000, buffer: 150000, block: 64
------
Tmp result 0
29.571677
Average result: 29.571677
Mon Nov 21 20:37:17 2016
Benchmarking for index = 81, fanout: 4, nodesize: 200000, buffer: 200000, block: 4
------
Tmp result 0
25.410209
Average result: 25.410209
Mon Nov 21 20:37:42 2016
Benchmarking for index = 82, fanout: 4, nodesize: 200000, buffer: 200000, block: 16
------
Tmp result 0
38.498847
Average result: 38.498847
Mon Nov 21 20:38:22 2016
Benchmarking for index = 83, fanout: 4, nodesize: 200000, buffer: 200000, block: 64
------
Tmp result 0
25.010447
Average result: 25.010447
Mon Nov 21 20:38:47 2016
Benchmarking for index = 84, fanout: 8, nodesize: 50000, buffer: 25000, block: 4
------
Tmp result 0
33.510065
Average result: 33.510065
Mon Nov 21 20:39:20 2016
Benchmarking for index = 85, fanout: 8, nodesize: 50000, buffer: 25000, block: 16
------
Tmp result 0
34.125221
Average result: 34.125221
Mon Nov 21 20:39:54 2016
Benchmarking for index = 86, fanout: 8, nodesize: 50000, buffer: 25000, block: 64
------
Tmp result 0
34.189930
Average result: 34.189930
Mon Nov 21 20:40:29 2016
Benchmarking for index = 87, fanout: 8, nodesize: 50000, buffer: 37500, block: 4
------
Tmp result 0
31.398414
Average result: 31.398414
Mon Nov 21 20:41:00 2016
Benchmarking for index = 88, fanout: 8, nodesize: 50000, buffer: 37500, block: 16
------
Tmp result 0
32.240647
Average result: 32.240647
Mon Nov 21 20:41:33 2016
Benchmarking for index = 89, fanout: 8, nodesize: 50000, buffer: 37500, block: 64
------
Tmp result 0
31.774953
Average result: 31.774953
Mon Nov 21 20:42:05 2016
Benchmarking for index = 90, fanout: 8, nodesize: 50000, buffer: 50000, block: 4
------
Tmp result 0
24.979704
Average result: 24.979704
Mon Nov 21 20:42:30 2016
Benchmarking for index = 91, fanout: 8, nodesize: 50000, buffer: 50000, block: 16
------
Tmp result 0
26.161341
Average result: 26.161341
Mon Nov 21 20:42:56 2016
Benchmarking for index = 92, fanout: 8, nodesize: 50000, buffer: 50000, block: 64
------
Tmp result 0
26.750636
Average result: 26.750636
Mon Nov 21 20:43:23 2016
Benchmarking for index = 111, fanout: 8, nodesize: 100000, buffer: 50000, block: 4
------
Tmp result 0
31.871999
Average result: 31.871999
Mon Nov 21 20:43:55 2016
Benchmarking for index = 112, fanout: 8, nodesize: 100000, buffer: 50000, block: 16
------
Tmp result 0
32.531951
Average result: 32.531951
Mon Nov 21 20:44:27 2016
Benchmarking for index = 113, fanout: 8, nodesize: 100000, buffer: 50000, block: 64
------
Tmp result 0
32.813231
Average result: 32.813231
Mon Nov 21 20:45:00 2016
Benchmarking for index = 114, fanout: 8, nodesize: 100000, buffer: 75000, block: 4
------
Tmp result 0
28.532055
Average result: 28.532055
Mon Nov 21 20:45:29 2016
Benchmarking for index = 115, fanout: 8, nodesize: 100000, buffer: 75000, block: 16
------
Tmp result 0
27.557969
Average result: 27.557969
Mon Nov 21 20:45:56 2016
Benchmarking for index = 116, fanout: 8, nodesize: 100000, buffer: 75000, block: 64
------
Tmp result 0
27.713271
Average result: 27.713271
Mon Nov 21 20:46:24 2016
Benchmarking for index = 117, fanout: 8, nodesize: 100000, buffer: 100000, block: 4
------
Tmp result 0
22.623553
Average result: 22.623553
Mon Nov 21 20:46:47 2016
Benchmarking for index = 118, fanout: 8, nodesize: 100000, buffer: 100000, block: 16
------
Tmp result 0
22.645121
Average result: 22.645121
Mon Nov 21 20:47:09 2016
Benchmarking for index = 119, fanout: 8, nodesize: 100000, buffer: 100000, block: 64
------
Tmp result 0
23.081540
Average result: 23.081540
Mon Nov 21 20:47:32 2016
Benchmarking for index = 135, fanout: 8, nodesize: 150000, buffer: 75000, block: 4
------
Tmp result 0
31.540791
Average result: 31.540791
Mon Nov 21 20:48:04 2016
Benchmarking for index = 136, fanout: 8, nodesize: 150000, buffer: 75000, block: 16
------
Tmp result 0
31.382678
Average result: 31.382678
Mon Nov 21 20:48:35 2016
Benchmarking for index = 137, fanout: 8, nodesize: 150000, buffer: 75000, block: 64
------
Tmp result 0
31.636269
Average result: 31.636269
Mon Nov 21 20:49:07 2016
Benchmarking for index = 138, fanout: 8, nodesize: 150000, buffer: 100000, block: 4
------
Tmp result 0
29.030298
Average result: 29.030298
Mon Nov 21 20:49:36 2016
Benchmarking for index = 139, fanout: 8, nodesize: 150000, buffer: 100000, block: 16
------
Tmp result 0
40.204560
Average result: 40.204560
Mon Nov 21 20:50:17 2016
Benchmarking for index = 140, fanout: 8, nodesize: 150000, buffer: 100000, block: 64
------
Tmp result 0
37.093194
Average result: 37.093194
Mon Nov 21 20:50:55 2016
Benchmarking for index = 141, fanout: 8, nodesize: 150000, buffer: 150000, block: 4
------
Tmp result 0
22.133491
Average result: 22.133491
Mon Nov 21 20:51:17 2016
Benchmarking for index = 142, fanout: 8, nodesize: 150000, buffer: 150000, block: 16
------
Tmp result 0
22.358339
Average result: 22.358339
Mon Nov 21 20:51:40 2016
Benchmarking for index = 143, fanout: 8, nodesize: 150000, buffer: 150000, block: 64
------
Tmp result 0
22.038147
Average result: 22.038147
Mon Nov 21 20:52:02 2016
Benchmarking for index = 159, fanout: 8, nodesize: 200000, buffer: 100000, block: 4
------
Tmp result 0
29.936848
Average result: 29.936848
Mon Nov 21 20:52:32 2016
Benchmarking for index = 160, fanout: 8, nodesize: 200000, buffer: 100000, block: 16
------
Tmp result 0
39.201746
Average result: 39.201746
Mon Nov 21 20:53:12 2016
Benchmarking for index = 161, fanout: 8, nodesize: 200000, buffer: 100000, block: 64
------
Tmp result 0
42.258739
Average result: 42.258739
Mon Nov 21 20:53:57 2016
Benchmarking for index = 162, fanout: 8, nodesize: 200000, buffer: 150000, block: 4
------
Tmp result 0
32.494520
Average result: 32.494520
Mon Nov 21 20:54:30 2016
Benchmarking for index = 163, fanout: 8, nodesize: 200000, buffer: 150000, block: 16
------
Tmp result 0
30.558098
Average result: 30.558098
Mon Nov 21 20:55:01 2016
Benchmarking for index = 164, fanout: 8, nodesize: 200000, buffer: 150000, block: 64
------
Tmp result 0
28.528371
Average result: 28.528371
Mon Nov 21 20:55:29 2016
Benchmarking for index = 165, fanout: 8, nodesize: 200000, buffer: 200000, block: 4
------
Tmp result 0
22.478118
Average result: 22.478118
Mon Nov 21 20:55:52 2016
Benchmarking for index = 166, fanout: 8, nodesize: 200000, buffer: 200000, block: 16
------
Tmp result 0
22.267566
Average result: 22.267566
Mon Nov 21 20:56:14 2016
Benchmarking for index = 167, fanout: 8, nodesize: 200000, buffer: 200000, block: 64
------
Tmp result 0
22.243699
Average result: 22.243699
Mon Nov 21 20:56:36 2016
Benchmarking for index = 168, fanout: 12, nodesize: 50000, buffer: 25000, block: 4
------
Tmp result 0
31.450016
Average result: 31.450016
Mon Nov 21 20:57:08 2016
Benchmarking for index = 169, fanout: 12, nodesize: 50000, buffer: 25000, block: 16
------
Tmp result 0
31.465761
Average result: 31.465761
Mon Nov 21 20:57:39 2016
Benchmarking for index = 170, fanout: 12, nodesize: 50000, buffer: 25000, block: 64
------
Tmp result 0
31.966601
Average result: 31.966601
Mon Nov 21 20:58:11 2016
Benchmarking for index = 171, fanout: 12, nodesize: 50000, buffer: 37500, block: 4
------
Tmp result 0
27.856074
Average result: 27.856074
Mon Nov 21 20:58:39 2016
Benchmarking for index = 172, fanout: 12, nodesize: 50000, buffer: 37500, block: 16
------
Tmp result 0
27.791937
Average result: 27.791937
Mon Nov 21 20:59:07 2016
Benchmarking for index = 173, fanout: 12, nodesize: 50000, buffer: 37500, block: 64
------
Tmp result 0
28.282437
Average result: 28.282437
Mon Nov 21 20:59:35 2016
Benchmarking for index = 174, fanout: 12, nodesize: 50000, buffer: 50000, block: 4
------
Tmp result 0
22.154440
Average result: 22.154440
Mon Nov 21 20:59:58 2016
Benchmarking for index = 175, fanout: 12, nodesize: 50000, buffer: 50000, block: 16
------
Tmp result 0
22.351240
Average result: 22.351240
Mon Nov 21 21:00:20 2016
Benchmarking for index = 176, fanout: 12, nodesize: 50000, buffer: 50000, block: 64
------
Tmp result 0
22.584306
Average result: 22.584306
Mon Nov 21 21:00:43 2016
Benchmarking for index = 195, fanout: 12, nodesize: 100000, buffer: 50000, block: 4
------
Tmp result 0
30.250672
Average result: 30.250672
Mon Nov 21 21:01:13 2016
Benchmarking for index = 196, fanout: 12, nodesize: 100000, buffer: 50000, block: 16
------
Tmp result 0
30.229934
Average result: 30.229934
Mon Nov 21 21:01:43 2016
Benchmarking for index = 197, fanout: 12, nodesize: 100000, buffer: 50000, block: 64
------
Tmp result 0
30.903094
Average result: 30.903094
Mon Nov 21 21:02:14 2016
Benchmarking for index = 198, fanout: 12, nodesize: 100000, buffer: 75000, block: 4
------
Tmp result 0
25.631861
Average result: 25.631861
Mon Nov 21 21:02:40 2016
Benchmarking for index = 199, fanout: 12, nodesize: 100000, buffer: 75000, block: 16
------
Tmp result 0
25.568168
Average result: 25.568168
Mon Nov 21 21:03:05 2016
Benchmarking for index = 200, fanout: 12, nodesize: 100000, buffer: 75000, block: 64
------
Tmp result 0
26.070548
Average result: 26.070548
Mon Nov 21 21:03:31 2016
Benchmarking for index = 201, fanout: 12, nodesize: 100000, buffer: 100000, block: 4
------
Tmp result 0
21.515642
Average result: 21.515642
Mon Nov 21 21:03:53 2016
Benchmarking for index = 202, fanout: 12, nodesize: 100000, buffer: 100000, block: 16
------
Tmp result 0
21.595966
Average result: 21.595966
Mon Nov 21 21:04:14 2016
Benchmarking for index = 203, fanout: 12, nodesize: 100000, buffer: 100000, block: 64
------
Tmp result 0
21.895433
Average result: 21.895433
Mon Nov 21 21:04:36 2016
Benchmarking for index = 219, fanout: 12, nodesize: 150000, buffer: 75000, block: 4
------
Tmp result 0
28.600813
Average result: 28.600813
Mon Nov 21 21:05:05 2016
Benchmarking for index = 220, fanout: 12, nodesize: 150000, buffer: 75000, block: 16
------
Tmp result 0
28.739851
Average result: 28.739851
Mon Nov 21 21:05:34 2016
Benchmarking for index = 221, fanout: 12, nodesize: 150000, buffer: 75000, block: 64
------
Tmp result 0
29.046125
Average result: 29.046125
Mon Nov 21 21:06:03 2016
Benchmarking for index = 222, fanout: 12, nodesize: 150000, buffer: 100000, block: 4
------
Tmp result 0
28.128756
Average result: 28.128756
Mon Nov 21 21:06:31 2016
Benchmarking for index = 223, fanout: 12, nodesize: 150000, buffer: 100000, block: 16
------
Tmp result 0
28.271030
Average result: 28.271030
Mon Nov 21 21:06:59 2016
Benchmarking for index = 224, fanout: 12, nodesize: 150000, buffer: 100000, block: 64
------
Tmp result 0
28.310613
Average result: 28.310613
Mon Nov 21 21:07:28 2016
Benchmarking for index = 225, fanout: 12, nodesize: 150000, buffer: 150000, block: 4
------
Tmp result 0
22.395126
Average result: 22.395126
Mon Nov 21 21:07:50 2016
Benchmarking for index = 226, fanout: 12, nodesize: 150000, buffer: 150000, block: 16
------
Tmp result 0
34.473954
Average result: 34.473954
Mon Nov 21 21:08:25 2016
Benchmarking for index = 227, fanout: 12, nodesize: 150000, buffer: 150000, block: 64
------
Tmp result 0
28.834631
Average result: 28.834631
Mon Nov 21 21:08:55 2016
Benchmarking for index = 243, fanout: 12, nodesize: 200000, buffer: 100000, block: 4
------
Tmp result 0
29.392888
Average result: 29.392888
Mon Nov 21 21:09:25 2016
Benchmarking for index = 244, fanout: 12, nodesize: 200000, buffer: 100000, block: 16
------
Tmp result 0
29.151569
Average result: 29.151569
Mon Nov 21 21:09:54 2016
Benchmarking for index = 245, fanout: 12, nodesize: 200000, buffer: 100000, block: 64
------
Tmp result 0
38.916950
Average result: 38.916950
Mon Nov 21 21:10:34 2016
Benchmarking for index = 246, fanout: 12, nodesize: 200000, buffer: 150000, block: 4
------
Tmp result 0
27.267351
Average result: 27.267351
Mon Nov 21 21:11:02 2016
Benchmarking for index = 247, fanout: 12, nodesize: 200000, buffer: 150000, block: 16
------
Tmp result 0
25.989867
Average result: 25.989867
Mon Nov 21 21:11:28 2016
Benchmarking for index = 248, fanout: 12, nodesize: 200000, buffer: 150000, block: 64
------
Tmp result 0
35.911435
Average result: 35.911435
Mon Nov 21 21:12:05 2016
Benchmarking for index = 249, fanout: 12, nodesize: 200000, buffer: 200000, block: 4
------
Tmp result 0
23.438575
Average result: 23.438575
Mon Nov 21 21:12:29 2016
Benchmarking for index = 250, fanout: 12, nodesize: 200000, buffer: 200000, block: 16
------
Tmp result 0
23.415720
Average result: 23.415720
Mon Nov 21 21:12:53 2016
Benchmarking for index = 251, fanout: 12, nodesize: 200000, buffer: 200000, block: 64
------
Tmp result 0
23.294772
Average result: 23.294772
45.201869 | 51.061302 | 39.739210 | 42.466018 | 36.486713 | 34.793800 | 29.021103 | 28.942828 | 29.388176 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 |
-1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | 35.575126 | 35.373363 | 35.643106 | 31.672364 | 52.309050 | 42.405278 | 27.675864 | 26.068632 | 35.254626 | -1.000000 | -1.000000 | -1.000000 | -1.000000 |
-1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | 38.519658 | 40.266782 | 35.711164 | 32.597970 | 32.444145 | 32.958924 | 26.161784 | 25.067555 | 28.322507 |
-1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | 34.276807 | 34.116106 | 34.100341 | 29.233338 | 30.672205 |
29.571677 | 25.410209 | 38.498847 | 25.010447 | 33.510065 | 34.125221 | 34.189930 | 31.398414 | 32.240647 | 31.774953 | 24.979704 | 26.161341 | 26.750636 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 |
-1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | 31.871999 | 32.531951 | 32.813231 | 28.532055 | 27.557969 | 27.713271 | 22.623553 | 22.645121 | 23.081540 |
-1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | 31.540791 | 31.382678 | 31.636269 | 29.030298 | 40.204560 |
37.093194 | 22.133491 | 22.358339 | 22.038147 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | 29.936848 |
39.201746 | 42.258739 | 32.494520 | 30.558098 | 28.528371 | 22.478118 | 22.267566 | 22.243699 | 31.450016 | 31.465761 | 31.966601 | 27.856074 | 27.791937 | 28.282437 | 22.154440 | 22.351240 | 22.584306 | -1.000000 | -1.000000 | -1.000000 |
-1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | 30.250672 | 30.229934 | 30.903094 | 25.631861 | 25.568168 |
26.070548 | 21.515642 | 21.595966 | 21.895433 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | 28.600813 |
28.739851 | 29.046125 | 28.128756 | 28.271030 | 28.310613 | 22.395126 | 34.473954 | 28.834631 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 | -1.000000 |
-1.000000 | -1.000000 | -1.000000 | 29.392888 | 29.151569 | 38.916950 | 27.267351 | 25.989867 | 35.911435 | 23.438575 | 23.415720 | 23.294772 |
Done benchmarking heap
##########################################################################################
Done benchmarking streams

Process finished with exit code 0
