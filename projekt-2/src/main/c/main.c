#include <stdio.h>
#include <string.h>

#include "tests/test.h"
#include "tests/testCases.h"
#include "tests/testExternalSort.h"

int main(int argc, char const *argv[]) {
//    test_stream();

    if (argc >= 2 && strcmp(argv[1], "test") == 0) {
        return run_tests();
    } else {
        return run_benchmark();
    }
}
