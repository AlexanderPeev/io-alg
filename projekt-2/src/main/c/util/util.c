#ifndef UTIL_C
#define UTIL_C

#include <errno.h>
#include <fcntl.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>

#include "util.h"

#ifndef errno
#define errno fallback_errno()
#endif

#ifndef O_BINARY
#define O_BINARY 0
#endif

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

#define FORMAT_BUFFER_SIZE 2048

unsigned long error_lines_logged = 0L;
char* format_buffer = NULL;

void ensure_format_buffer_initialized() {
  if(format_buffer == NULL) {
    format_buffer = (char*)calloc(FORMAT_BUFFER_SIZE, sizeof(char));
  }
}

char* fC(char* format, char arg) {
  ensure_format_buffer_initialized();
  sprintf(format_buffer, format, arg);
  return format_buffer;
}

char* fD(char* format, double arg) {
  ensure_format_buffer_initialized();
  sprintf(format_buffer, format, arg);
  return format_buffer;
}


char* fS(char* format, char* arg) {
  ensure_format_buffer_initialized();
  sprintf(format_buffer, format, arg);
  return format_buffer;
}

char* fS2(char* format, char* arg1, char* arg2) {
  ensure_format_buffer_initialized();
  sprintf(format_buffer, format, arg1, arg2);
  return format_buffer;
}

char* fI(char* format, int arg) {
  ensure_format_buffer_initialized();
  sprintf(format_buffer, format, arg);
  return format_buffer;
}

char* fL(char* format, long arg) {
  ensure_format_buffer_initialized();
  sprintf(format_buffer, format, arg);
  return format_buffer;
}

void logNewLine() {
  printf("%s", "\n");
  fflush(stdout);
}

void logLine(char* message) {
  printf("%s", message);
  logNewLine();
}

void logErrorLine(char* message) {
  ++error_lines_logged;

  printf(ANSI_COLOR_RED);
  printf("%s", message);
  printf(ANSI_COLOR_RESET);
  logNewLine();
}

void logWarningLine(char* message) {
  printf(ANSI_COLOR_YELLOW);
  printf("%s", message);
  printf(ANSI_COLOR_RESET);
  logNewLine();
}

void logSuccessLine(char* message) {
  printf(ANSI_COLOR_GREEN);
  printf("%s", message);
  printf(ANSI_COLOR_RESET);
  logNewLine();
}

void logInfoLine(char* message) {
  printf(ANSI_COLOR_BLUE);
  printf("%s", message);
  printf(ANSI_COLOR_RESET);
  logNewLine();
}

int file_size_from_file_descriptor(int fd) {
  struct stat s;
  if (fstat(fd, &s) == -1) {
    int saveErrno = errno;
    fprintf(stderr, "fstat(%d) returned errno=%d.", fd, saveErrno);
    return(-1);
  }
  return (int) s.st_size;
}

int total_decimal_digits (int number) {
  return (int)floor(log10(abs(number))) + 1;
}

int util_min (int a, int b) {
  return a <= b ? a : b;
}

long util_long_min (long a, long b) {
  return a <= b ? a : b;
}

char* create_null_terminated_string(int length) {
  return (char*)calloc(length + 1, sizeof(char));
}

void free_string(char* nullTerminatedString) {
  if(nullTerminatedString != NULL) {
    free(nullTerminatedString);
  }
  else {
    logWarningLine("STRING was null in free_string");
  }
}

int fallback_errno() {
  return 0;
}

BOOL file_exists(char* filename) {
  FILE* file = fopen(filename, "r");

  if(file == NULL) {
    return FALSE;
  }
  else {
    fclose(file);
    return TRUE;
  }
}

long file_size(char* filename) {
  long fileSize = -1L;
  int handle;
  struct stat st;

  handle = open(filename, O_RDONLY | O_BINARY, S_IREAD);

  if(handle != -1) {
    if(fstat(handle, &st) != -1) {
      fileSize = st.st_size;
    }
    else {
      logErrorLine(fI("ERROR during fstat in file_size: %d", errno));
    }

    if(close(handle) == -1) {
      logWarningLine(fI("ERROR during close in file_size: %d", errno));
    }
  }
  else {
    logErrorLine(fI("ERROR during open in file_size: %d", errno));
  }

  return fileSize;
}

unsigned long total_error_lines_logged() {
  return error_lines_logged;
}

#endif /* UTIL_C */
