#ifndef TEST_HEAP_ROUNDTRIP_C
#define TEST_HEAP_ROUNDTRIP_C

#include "test.h"
#include "testUtil.h"
#include "../dataTypes/element.h"
#include "../dataTypes/heap.h"

void testHeapRoundtrip(InputStream is, OutputStream os) {
    heap* uriah;
    int buffer_size = 4096;
    int M = buffer_size * buffer_size;
    int fanout = 4;
    ELEMENT actual;

    logInfoLine(fS2("Starting HEAP ROUNDTRIP test for implementation (%s, %s)", is.name, os.name));

    uriah = createEmptyHeap(buffer_size, M, fanout, is, os);

    add(uriah, SOME_ELEMENT);

    actual = removeMin(uriah);

    assert_elements_equal_with_message("Heap Roundtrip", SOME_ELEMENT, actual);

    freeHeap(uriah);

    logInfoLine(fS2("Finished with HEAP ROUNDTRIP test for implementation (%s, %s)", is.name, os.name));
}


#endif /* TEST_HEAP_ROUNDTRIP_C */
