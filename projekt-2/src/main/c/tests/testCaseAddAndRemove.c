//
// Created by Karen on 8/11/2016.
//
#include "testCases.h"
#include "../dataTypes/heap.h"
#include "testUtil.h"

void shuffle(ELEMENT *array, size_t n)
{
    if (n > 1)
    {
        size_t i;
        for (i = 0; i < n - 1; i++)
        {
            size_t j = i + rand() / (RAND_MAX / (n - i) + 1);
            int t = array[j];
            array[j] = array[i];
            array[i] = t;
        }
    }
}

//add and remove elements in heap with buffer size and M = 5 and fanout = 2
void test_add_and_remove(InputStream is, OutputStream os){
    heap* h;
    ELEMENT actual;
    int i;
    ELEMENT array[30] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29};

    shuffle(array, 30);

    h = createEmptyHeap(5, 5, 2, is, os);

    for(i = 0; i < 30; i++){
        add(h, array[i]);
    }

    for(i = 0; i < 30; i++){
        actual = removeMin(h);
        assert_elements_equal(i, actual);
    }


    freeHeap(h);
}

