//
// Created by Karen on 20/11/2016.
//

//#include <mem.h>
//#include <mem.h>
#include "test.h"

int node_size_test(){
  int fanout = 4;
  int input_size = 5000000;
  int nodesizes[] = {5000,10000,25000,50000};
  double tmp, result;
  int i;
  int m;
  int runs = 1;
  char* output_file_name = create_null_terminated_string(256);
  char* file_name = "input-node-test.txt";

  init_streams();

  generate_random_elements_of_size_and_name(os_c, input_size, file_name);


  for(i = 0; i < 4; i++){
    result = 0;
    for (m = 0; m < runs; ++m) {
      //print(tmp result with i, j, k and l

      if(m == 0) {
        printf("Benchmarking for index = %d, fanout: %d, nodesize: %d, buffer: %d, block: %d\n", i, fanout, nodesizes[i], nodesizes[i], 128);
        logInfoLine("------");
      }
      //  Output file name
      output_file_name[0] = '\0';
      strcat(output_file_name, "output");
//              snprintf(&output_file_name[4], 256-4, "%d", index);
      strcat(output_file_name, ".txt");

      tmp = benchmark_heap_with_params(fanout, nodesizes[i], nodesizes[i], 128, ris_c, os_c, file_name, output_file_name);

      logInfoLine(fI("Tmp result %d ", m));
      logInfoLine(fD("%f", tmp));
      result  += tmp;
    }
    result /= runs;
    logInfoLine(fD("Average result: %f", result));

  }

  printf("\n");
  free(output_file_name);
}

int buffer_size_test(){
  int fanout = 4;
  int input_size = 1000000;
  int nodesize = 5000;
  int buffersizes[] = {5000};
  double tmp, result;
  int i;
  int m;
  int runs = 2;
  char* output_file_name = create_null_terminated_string(256);
  char* file_name = "input-node-test.txt";

  init_streams();

  generate_random_elements_of_size_and_name(os_c, input_size, file_name);


  for(i = 0; i < 1; i++){
    result = 0;
    for (m = 0; m < runs; ++m) {
      //print(tmp result with i, j, k and l

      if(m == 0) {
        printf("Benchmarking for index = %d, fanout: %d, nodesize: %d, buffer: %d, block: %d\n", i, fanout, nodesize, buffersizes[i], 128);
        logInfoLine("------");
      }
      //  Output file name
      output_file_name[0] = '\0';
      strcat(output_file_name, "output");
//              snprintf(&output_file_name[4], 256-4, "%d", index);
      strcat(output_file_name, ".txt");

      tmp = benchmark_heap_with_params(fanout, nodesize, buffersizes[i], 128, ris_c, os_c, file_name, output_file_name);

      logInfoLine(fI("Tmp result %d ", m));
      logInfoLine(fD("%f", tmp));
      result  += tmp;
    }
    result /= runs;
    logInfoLine(fD("Average result: %f", result));

  }

  printf("\n");
  free(output_file_name);
}


int fanout_size_test(){
  int fanout[] = {4,8,12,64};
  int input_size = 1000000;
  int nodesize = 5000;
  int buffersize = 5000;
  double tmp, result;
  int i;
  int m;
  int runs = 2;
  char* output_file_name = create_null_terminated_string(256);
  char* file_name = "input-node-test.txt";

  init_streams();

  generate_random_elements_of_size_and_name(os_c, input_size, file_name);


  for(i = 0; i < 4; i++){
    result = 0;
    for (m = 0; m < runs; ++m) {
      //print(tmp result with i, j, k and l

      if(m == 0) {
        printf("Benchmarking for index = %d, fanout: %d, nodesize: %d, buffer: %d, block: %d\n", i, fanout[i], nodesize, buffersize, 128);
        logInfoLine("------");
      }
      //  Output file name
      output_file_name[0] = '\0';
      strcat(output_file_name, "output");
//              snprintf(&output_file_name[4], 256-4, "%d", index);
      strcat(output_file_name, ".txt");

      tmp = benchmark_heap_with_params(fanout[i], nodesize, buffersize, 128, ris_c, os_c, file_name, output_file_name);

      logInfoLine(fI("Tmp result %d ", m));
      logInfoLine(fD("%f", tmp));
      result  += tmp;
    }
    result /= runs;
    logInfoLine(fD("Average result: %f", result));

  }

  printf("\n");
  free(output_file_name);
}
