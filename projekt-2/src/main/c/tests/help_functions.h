//
// Created by Karen on 16/11/2016.
//

#ifndef IO_ALG_PROJEKT_2_HELP_FUNCTIONS_H
#define IO_ALG_PROJEKT_2_HELP_FUNCTIONS_H

#include "../dataTypes/element.h"
#include "../dataTypes/heap.h"

BOOL heap_contains_element(heap* h, ELEMENT element);

BOOL search_in_child(heap* h, node* n, ELEMENT element);

#endif //IO_ALG_PROJEKT_2_HELP_FUNCTIONS_H
