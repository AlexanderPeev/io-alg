//
// Created by Karen on 25/10/2016.
//

#ifndef IO_ALG_PROJEKT_2_TEST_H
#define IO_ALG_PROJEKT_2_TEST_H

#include "../streams/streams.h"

void test_implementation(InputStream is, OutputStream os);
int run_tests();
int run_benchmark();
int node_size_test();

void compare_streams_partial(InputStream is, OutputStream os);

double benchmark_heap_with_params(int fanout, int nodesize, int buffersize, int blocksize, InputStream is, OutputStream os, char *input_file_name, char *output_file_name);
void generate_random_elements_of_size_and_name(OutputStream os, int size, char *name);
int buffer_size_test();
int fanout_size_test();
void test_stream();
void compare_streams();
void test_pq();
void test_mergesort(InputStream is, OutputStream os);

#endif //IO_ALG_PROJEKT_2_TEST_H
