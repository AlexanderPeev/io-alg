#ifndef TEST_CASE_DUAL_ROUNDTRIP_C
#define TEST_CASE_DUAL_ROUNDTRIP_C

#include "test.h"
#include "testUtil.h"
#include "../streams/streams.h"

void test_reverse_dual_roundtrip(InputStream is, OutputStream os) {
  char filenameFormat[] = "./reverse-dual-roundtrip-%c.txt";
  ELEMENT element = SOME_ELEMENT;
  ELEMENT otherElement = SOME_OTHER_ELEMENT;
  ELEMENT* actual;
  HANDLE handle;

  os_create create = os.create;
  os_write write = os.write;
  os_close oclose = os.close;

  is_open open = is.open;
  is_read_next read_next = is.read_next;
  is_close iclose = is.close;

  logInfoLine(fS2("Starting REVERSE DUAL ROUNDTRIP test for implementation (%s, %s)", is.name, os.name));

  handle = (*create)(fS(filenameFormat, os.code));
  (*write)(handle, element);
  (*write)(handle, otherElement);
  (*oclose)(handle);

  handle = (*open)(fC(filenameFormat, os.code));

  actual = (*read_next)(handle);
  assert_elements_equal_with_message("Reverse dual roundtrip - reverse first", otherElement, *actual);
  free_element(actual);

  actual = (*read_next)(handle);
  assert_elements_equal_with_message("Reverse dual roundtrip - reverse second", element, *actual);
  free_element(actual);

  (*iclose)(handle);

  logInfoLine(fS2("Finished with REVERSE DUAL ROUNDTRIP test for implementation (%s, %s)", is.name, os.name));
}

#endif /* TEST_CASE_DUAL_ROUNDTRIP_C */
