#include <string.h>

#include "../streams/streams.h"
#include "../util/util.h"
#include "testUtil.h"

BOOL test_assert_failed = FALSE;

BOOL are_equal(ELEMENT a, ELEMENT b) {
  if(memcmp(&a, &b, element_byte_size()) == 0) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

void assert_elements_equal(ELEMENT expected, ELEMENT actual) {
  assert_elements_equal_with_message("", expected, actual);
}

void assert_elements_equal_with_message(char* message, ELEMENT expected, ELEMENT actual) {
  char* sExpected;
  char* sActual;
  if(!are_equal(expected, actual)) {
    test_assert_failed = TRUE;
    sExpected = visualize_element(expected);
    sActual = visualize_element(actual);

    logErrorLine(fS("ASSERT FAILED! Elements are not equal: %s", message));
    logErrorLine(fS("\t\tExpected: %s", sExpected));
    logErrorLine(fS("\t\tActual:   %s", sActual));

    free(sExpected);
    free(sActual);
  }
}

void assert_bool_equal(BOOL expected, BOOL actual) {
  assert_bool_equal_with_message("", expected, actual);
}

void assert_bool_equal_with_message(char* message, BOOL expected, BOOL actual) {
  char* sExpected;
  char* sActual;
  if(expected != actual) {
    test_assert_failed = TRUE;
    sExpected = visualize_bool(expected);
    sActual = visualize_bool(actual);

    logErrorLine(fS("ASSERT FAILED! Boolean values are not equal: %s", message));
    logErrorLine(fS("\t\tExpected: %s", sExpected));
    logErrorLine(fS("\t\tActual:   %s", sActual));
  }
}

void assert_strings_equal(char* expected, char* actual) {
  assert_strings_equal_with_message("", expected, actual);
}

void assert_strings_equal_with_message(char* message, char* expected, char* actual) {
  if(strcmp(expected, actual) != 0) {
    test_assert_failed = TRUE;

    logErrorLine(fS("ASSERT FAILED! String values are not equal: %s", message));
    logErrorLine(fS("\t\tExpected: %s", expected));
    logErrorLine(fS("\t\tActual:   %s", actual));
  }
}

int assert_result_code() {
  return test_assert_failed || total_error_lines_logged() > 0L ? -1 : 0;
}

int benchmark_result_code() {
  return is_handle_error_raised_ever() || total_error_lines_logged() > 0L ? -1 : 0;
}
