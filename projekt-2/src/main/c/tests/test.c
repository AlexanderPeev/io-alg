#ifndef TEST_C
#define TEST_C

//#include <mem.h>
#include "test.h"
#include "testCases.h"
#include "testUtil.h"
#include "../streams/streams.h"
#include "testExternalSort.h"
#include "../sort/merge.h"
#include "../dataTypes/buffer2.h"
#include "../streams/randomElementGenerator.h"

void test_implementation(InputStream is, OutputStream os) {
  logInfoLine(fS2("Starting tests for implementation (%s, %s)", is.name, os.name));

  testHeapRoundtrip(is, os);

  test_reverse_roundtrip(is, os);
  test_reverse_eof(is, os);
  test_reverse_dual_roundtrip(is, os);
  test_reverse_multiple_streams(is, os);
  // TODO

  compare_streams_partial(ris_d, os_d);

  logInfoLine(fS2("Finished with tests for implementation (%s, %s)", is.name, os.name));
  logNewLine();
}

int run_tests() {
  logInfoLine("Running tests");

  init_streams();

  logInfoLine("Initialized streams... ");

  // test_remove_min(ris_a,os_a);

  //external_sort_test2(ris_c, os_c, 100,100,10);
  //test_external_sort(ris_c, os_c, 2000, 10, 10, 12);

  test_external_sort(ris_b, os_b, 10000, 100, 100, 10);

  testIsLeaf();
  testPreviousNode();

  test_multiple_insert_and_remove_mins();


  logInfoLine("Testing implementations... ");

  test_implementation(ris_a, os_a);
  test_implementation(ris_b, os_b);
  test_implementation(ris_c, os_c);
  if (is_stream_d_valid()) {
    test_implementation(ris_d, os_d);
  } else {
    logWarningLine(fS2("Skipping tests for implementation (%s, %s) as it is not valid", is_d.name, os_d.name));
  }

  // TODO

  logInfoLine("Done running tests");
  return assert_result_code();
}

int run_benchmarks_for_heap(InputStream is, OutputStream os, char *file_name);

double benchmark_heap_with_params(int i, int i1, int i2, int i3, InputStream is, OutputStream os, char *input_file_name,
                                  char *output_file_name);

void generate_random_elements_of_size_and_name(OutputStream os, int size, char *name);

void printCurrentTimeStamp();

void benchmark_sorting(InputStream is, OutputStream os, int M, int d, InputStream ris, OutputStream os2);

double benchmark_is_os(int count, InputStream is, OutputStream os);

int run_benchmark() {
//  int i;
//  bufferheap2* bh;
//  HANDLE input;
//  ELEMENT* element_pointer;
//  ELEMENT element;
//  long before = 0;
//  long after = 0;
//  double time;
  InputStream selected_is;
  InputStream selected_ris;
  OutputStream selected_os;
  double tmp_a, tmp_b, tmp_c, tmp_d;
  double old_res;
  double best;

  int fanout, buffersize, blocksize, nodesize;

  char *file_name = "test-1.txt";
  char *output_file_name = "out-1.txt";

  logInfoLine("Running Benchmarks");

  logSuccessLine("Initializing streams... ");
  init_streams();
  logSuccessLine("Initialized streams... ");
//  generate_random_elements_of_size_and_name(os_c, 100000000, file_name);

//  logInfoLine("##########################################################################################");
//  logSuccessLine("Benchmarking heap");
//
//
//  run_benchmarks_for_heap(ris_c, os_c, file_name);
//  logSuccessLine("Done benchmarking heap");
//  logInfoLine("##########################################################################################");

  blocksize = 16;
  nodesize = 150000;
  buffersize = nodesize;
  fanout = 32;

//
//  tmp_c = benchmark_heap_with_params(fanout, nodesize, buffersize, blocksize, ris_c, os_c, file_name, output_file_name);
//  logSuccessLine(fD("Result from stream c: %lf", tmp_c));

//  tmp_d = benchmark_heap_with_params(fanout, nodesize, buffersize, blocksize, ris_d, os_d, file_name, output_file_name);
//  logSuccessLine(fD("Result from stream d: %lf", tmp_d));
//
//  old_res = benchmark_heap_with_params(fanout, nodesize, buffersize, 128, ris_d, os_d, file_name, output_file_name);
//  logSuccessLine(fD("Result from stream OLD with blocksize 128: %lf", old_res));

  benchmark_sorting(is_d, os_d, 128 * 1024 * 1024, 1019, ris_c, os_c);

//  tmp_b = benchmark_heap_with_params(fanout, nodesize, buffersize, blocksize, ris_b, os_b, file_name, output_file_name);
  logSuccessLine(fD("Result from stream b: %lf", tmp_b));

  logSuccessLine("Done benchmarking streams");
  return benchmark_result_code();
}

void generate_random_elements_of_size_and_name(OutputStream os, int size, char *name) {
  int i;
  HANDLE handle;
  ELEMENT element;

  handle = (*(os.create))(name);
  for (i = 0; i < size; i++) {
    element = random_element();
    (*(os.write))(handle, element);
  }
  (*(os.close))(handle);
}

int run_benchmarks_for_heap(InputStream is, OutputStream os, char *file_name) {
  int countFanoutsizes = 3;
  int fanoutsizes[] = {8, 12, 32};

  int countNodesizes = 4;
  int nodesizes[] = {50000, 100000, 150000, 200000};

  int countBuffersizes = 7;
  int buffersizes[] = {25000, 37500, 50000, 75000, 100000, 150000, 200000};

  int countBlocksizes = 3;
  int blocksizes[] = {16, 64, 128};

  char *output_file_name = create_null_terminated_string(256);
  int strIdx = 0;
//  set_block_size_factor(128);

  int l1 = 1;
  int l2 = countBlocksizes;
  int l3 = l2 * countBuffersizes;
  int l4 = l3 * countNodesizes;
  int total_size = countFanoutsizes * countNodesizes * countBuffersizes * countBlocksizes;

  double results[total_size];
  double result, tmp;
  int index;
  int runs = 1;

  int i, j, k, l, m;

  for (i = 0; i < countFanoutsizes; ++i) {

    for (j = 0; j < countNodesizes; ++j) {

      for (k = 0; k < countBuffersizes; ++k) {

        for (l = 0; l < countBlocksizes; ++l) {
          index = i * l4 + j * l3 + k * l2 + l * l1;
          result = 0;
          if (buffersizes[k] <= nodesizes[j] && buffersizes[k] >= nodesizes[j] / 2) {
            for (m = 0; m < runs; ++m) {
              //print(tmp result with i, j, k and l

              if (m == 0) {
                printCurrentTimeStamp();
                printf("Benchmarking for index = %d, fanout: %d, nodesize: %d, buffer: %d, block: %d\n", index,
                       fanoutsizes[i], nodesizes[j], buffersizes[k], blocksizes[l]);
                logInfoLine("------");
              }
              //  Output file name
              output_file_name[0] = '\0';
              strcat(output_file_name, "output");
//              snprintf(&output_file_name[4], 256-4, "%d", index);
              strcat(output_file_name, ".txt");

              tmp = benchmark_heap_with_params(fanoutsizes[i], nodesizes[j], buffersizes[k], blocksizes[l], is, os,
                                               file_name, output_file_name);

              logInfoLine(fI("Tmp result %d ", m));
              logInfoLine(fD("%f", tmp));
              result += tmp;
            }
            result /= runs;
            logInfoLine(fD("Average result: %f", result));
          } else {
            result = -1;
          }
          results[index] = result;
        }
      }
    }
  }

  for (i = 0; i < total_size; i++) {
    printf("%f | ", results[i]);
    if (i % 20 == 19) printf("\n");
  }
  printf("\n");
  free(output_file_name);
}

void printCurrentTimeStamp() {
  time_t current_time;
  char *c_time_string;

  current_time = time(NULL);

  /* Convert to local time format. */
  c_time_string = ctime(&current_time);

  printf("%s", c_time_string);
}

double
benchmark_heap_with_params(int fanout, int nodesize, int buffersize, int blocksize, InputStream is, OutputStream os,
                           char *input_file_name, char *output_file_name) {
  long before, after;
  set_block_size_factor(blocksize);

  before = clock();
  external_sort_file(is, os, input_file_name, output_file_name, buffersize, fanout, nodesize);
  after = clock();

//  TODO:
//  cleanup();

  return (double) (after - before) / CLOCKS_PER_SEC;
}

double benchmark_merge_sort(InputStream is, OutputStream os, char *fileName, int N, int M, int d) {
  clock_t begin;
  clock_t end;
  double time_spent;

  HANDLE input;
  HANDLE output;

  input = (*is.open)(fileName);
  output = (*os.create)(fC("./benchmark-output-%c.txt", os.code));

#ifdef _WIN32
  //for stream B, we have to set the maximum files that can be opened by fopen (normally only 512, can be increased up to 2048)
    _setmaxstdio(1100);
#endif

  begin = clock();

  my_fun_mergesort2(input, output, N, M, d, is, os);

  end = clock();
  time_spent = (double) (end - begin) / CLOCKS_PER_SEC;

  (*is.close)(input);
  (*os.close)(output);

  return time_spent;
}

double benchmark_merge_sorts(InputStream is, OutputStream os, char *fileName, int runs, int N, int M, int d) {
  double result = 0;
  int i;

  for (i = 0; i < runs; i++) {
    result += benchmark_merge_sort(is, os, fileName, N, M, d);
  }

  return result / runs;
}


double benchmark_sorting_algorithm(char *fileName, InputStream is, int N, void (*sort)(ELEMENT *, int)) {
  int i;
  ELEMENT *elt;
  clock_t begin;
  clock_t end;
  ELEMENT *elements;

  HANDLE input;

  input = (*is.open)(fileName);
  elements = calloc(N, element_byte_size());

  for (i = 0; i < N; i++) {
    elt = (*is.read_next)(input);
    elements[i] = *elt;
    free_element(elt);
  }

  begin = clock();
  (*sort)(elements, N);
  end = clock();

  (*is.close)(input);
  free(elements);

  return (double) (end - begin) / CLOCKS_PER_SEC;
}

double benchmark_sorting_algorithms(char *fileName, InputStream is, int runs, int N, void (*sort)(ELEMENT *, int)) {
  double result = 0;
  double tmp;
  int i;

  for (i = 0; i < runs; i++) {
    logInfoLine(fI("Run number: ", i));
    tmp = benchmark_sorting_algorithm(fileName, is, N, sort);
    logInfoLine(fD("Runningtime: %lf", tmp));
    result += tmp;
  }

  return result / runs;
}

void benchmark_sorting(InputStream is, OutputStream os, int M, int d, InputStream ris, OutputStream os2) {
  int N[] = {1000000, 10000000, 250000000, 500000000};
  int countN = 4;
  int i, j;
  int runs = 1;
  char *fileNameFormat = "./final-my_fun_merge-%d.txt";
//  char mergeTestFileName = create_null_terminated_string(50);
  char mergeTestFileName[50];
  double runningTime;
  long double tmp;
  time_t clk;

  logInfoLine("Creating files");
  for (i = 0; i < countN; i++) {
    logInfoLine(fI("Creating %d elements", N[i]));
    sprintf(mergeTestFileName, fileNameFormat, N[i]);
    generate_random_elements_of_size_and_name(os_c, N[i], mergeTestFileName);
    logInfoLine(fI("Wrote %d elements", N[i]));
  }

  for (i = 0; i < countN; i++) {
    logInfoLine("############################################################");
    logInfoLine(fI("Testing %d elements", N[i]));
    sprintf(mergeTestFileName, fileNameFormat, N[i]);

    runningTime = 0;
    for (j = 0; j < runs; j++) {
      logInfoLine("Heap");
      clk = time(NULL);
      printf("%s", ctime(&clk));
      tmp = benchmark_heap_with_params(1019, 100000000, 100000000, 32, ris, os2, mergeTestFileName,
                                       "heap-my_fun_merge-output.txt");
      clk = time(NULL);
      printf("%s", ctime(&clk));
      logInfoLine(fD("Tmp result = %lf", tmp));
      runningTime += tmp;
    }
    runningTime = runningTime / runs;
    logInfoLine(fD("Running time (avg  1) for external heap sort: %lf", runningTime));

//    if(i % 2 == 0){

//      runningTime = benchmark_merge_sorts(is, os, mergeTestFileName, runs, N[i], M, d);
//      logInfoLine(fD("Running time (avg  1) for my_fun_merge sort: %lf", runningTime));

    logInfoLine("Testing Quick sort");
    clk = time(NULL);
    printf("%s", ctime(&clk));
    runningTime = benchmark_sorting_algorithms(mergeTestFileName, is, runs, N[i], (&quicksort));
    clk = time(NULL);
    printf("%s", ctime(&clk));
    logInfoLine(fD("Running time (avg  3) for quick sort: %lf", runningTime));

    if(N[i] < 250000000){

      logInfoLine("Testing Heap sort");
      clk = time(NULL);
      printf("%s", ctime(&clk));
      runningTime = benchmark_sorting_algorithms(mergeTestFileName, is, runs, N[i], (&my_fun_heapsort));
      clk = time(NULL);
      printf("%s", ctime(&clk));
      logInfoLine(fD("Running time (avg  3) for heap sort:  %lf", runningTime));
    }

    remove(mergeTestFileName);
  }

  logInfoLine("Finished with benchmark. ");
}


#endif /* TEST_C */
