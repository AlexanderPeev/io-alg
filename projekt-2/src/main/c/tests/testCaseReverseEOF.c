#ifndef TEST_CASE_EOF_C
#define TEST_CASE_EOF_C

#include "test.h"
#include "testUtil.h"
#include "../streams/streams.h"

void test_reverse_eof(InputStream is, OutputStream os) {
  char filenameFormat[] = "./reverse-eof-%c.txt";
  ELEMENT element = SOME_ELEMENT;
  ELEMENT* read;
  BOOL actual;
  HANDLE handle;

  os_create create = os.create;
  os_write write = os.write;
  os_close oclose = os.close;

  is_open open = is.open;
  is_read_next read_next = is.read_next;
  is_end_of_stream end_of_stream = is.end_of_stream;
  is_close iclose = is.close;

  logInfoLine(fS2("Starting REVERSE EOF test for implementation (%s, %s)", is.name, os.name));

  handle = (*create)(fC(filenameFormat, os.code));
  (*write)(handle, element);
  (*write)(handle, element);
  (*oclose)(handle);

  handle = (*open)(fC(filenameFormat, os.code));

  actual = (*end_of_stream)(handle);
  assert_bool_equal_with_message("REVERSE EOF should NOT be reached at start: ", FALSE, actual);

  read = (*read_next)(handle);

  actual = (*end_of_stream)(handle);
  assert_bool_equal_with_message("REVERSE EOF should NOT be reached after one read: ", FALSE, actual);

  free_element(read);

  read = (*read_next)(handle);

  actual = (*end_of_stream)(handle);
  assert_bool_equal_with_message("REVERSE EOF should be reached after two reads: ", TRUE, actual);

  free_element(read);

  (*iclose)(handle);

  logInfoLine(fS2("Finished with REVERSE EOF test for implementation (%s, %s)", is.name, os.name));
}

#endif /* TEST_CASE_EOF_C */
