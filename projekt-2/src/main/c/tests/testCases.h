#ifndef TEST_CASES_H
#define TEST_CASES_H

#include "../streams/streams.h"

void testIsLeaf();
void testPreviousNode();
void test_add_and_remove(InputStream is, OutputStream os);
void test_remove_min(InputStream is, OutputStream os);
void test_multiple_insert_and_remove_mins();

void testHeapRoundtrip(InputStream is, OutputStream os);

void test_reverse_dual_roundtrip(InputStream is, OutputStream os);
void test_reverse_eof(InputStream is, OutputStream os);
void test_reverse_multiple_streams(InputStream is, OutputStream os);
void test_reverse_roundtrip(InputStream is, OutputStream os);

#endif /* TEST_CASES_H */
