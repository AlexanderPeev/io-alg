//
// Created by Karen on 8/11/2016.
//
#include "testCases.h"
#include "../dataTypes/heap.h"
#include "../streams/outputStream.h"
#include "testUtil.h"
#include "../streams/inputStream.h"
#include "../util/util.h"

void test_remove_min(InputStream is, OutputStream os){
    heap* h;
    HANDLE handle;
    ELEMENT elements[] = {0,1,2,4,5,8,9,10,12,15,19,20,21,23,28,30,32,37,42,50};
    int elements_size = 20;
    int i;
    ELEMENT actual;
    char* element_assert_format = "Element with index: %d";
    char* message = create_null_terminated_string(50);

    logInfoLine("Starting test remove min. ");

    h = createEmptyHeap(4, 4, 2, is, os);

    //initialze buffer
    bh2_push(h->buffer, 1);
    bh2_push(h->buffer, 5);
    bh2_push(h->buffer, 9);

    //initialize root
    bh2_push(h->root, 0);
    bh2_push(h->root, 2);
    bh2_push(h->root, 4);

    handle = (*(os.create))("0.txt");
    (*(os.write))(handle, 20);
    (*(os.write))(handle, 12);
    (*(os.close))(handle);

    handle = (*(os.create))("1.txt");
    (*(os.write))(handle, 19);
    (*(os.write))(handle, 15);
    (*(os.write))(handle, 10);
    (*(os.write))(handle, 8);
    (*(os.close))(handle);

    handle = (*(os.create))("0-0.txt");
    (*(os.write))(handle, 37);
    (*(os.write))(handle, 32);
    (*(os.write))(handle, 28);
    (*(os.write))(handle, 23);
    (*(os.close))(handle);

    handle = (*(os.create))("0-1.txt");
    (*(os.write))(handle, 50);
    (*(os.write))(handle, 42);
    (*(os.write))(handle, 30);
    (*(os.write))(handle, 21);
    (*(os.close))(handle);

    h->lastnode = getNodeFromString("0-1");

    logInfoLine("Init done. ");

    for(i = 0; i < elements_size; i++){
        sprintf(message, element_assert_format, i);
        logInfoLine(message);

        actual = removeMin(h);
        assert_elements_equal_with_message(message, elements[i], actual);
    }

    free_string(message);
    free(h);

    logInfoLine("Finished test remove min. ");
}
