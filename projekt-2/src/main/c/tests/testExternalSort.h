//
// Created by Karen on 15/11/2016.
//

#include "../streams/inputStream.h"
#include "../streams/outputStream.h"
#include "../sort/externalSort.h"
#include "../sort/sort.h"
#include "testUtil.h"

#ifndef IO_ALG_PROJEKT_2_TESTEXTERNALSORT_H
#define IO_ALG_PROJEKT_2_TESTEXTERNALSORT_H

void test_external_sort(InputStream is, OutputStream os, int n, int buffersize, int M, int fanout);

void external_sort_test2(InputStream is, OutputStream os, int bufferSize, int M, int fanout);

#endif //IO_ALG_PROJEKT_2_TESTEXTERNALSORT_H
