//
// Created by Karen on 2/11/2016.
//

#include <string.h>
#include "testCases.h"
#include "../dataTypes/node.h"
#include "../dataTypes/heap.h"
#include "testUtil.h"

void testPreviousNode(){
    char* nodestring;
    char result[256];
    node* n;
    node* previous;
    heap* h = createEmptyHeap(1,1,4,is_a,os_a);

    logInfoLine("Testing isPreviousNode...");

    logInfoLine("Test1: ");
    //test1
    nodestring = "0-0-0-0";
    n = getNodeFromString(nodestring);

    previous = getPreviousNode(n, h);
    getStringFromNode(previous, result, 256);
    assert_strings_equal_with_message("Test 1", "3-3-3", result);
    freeNode(n);
    free(previous);


    logInfoLine("Test2: ");
    //test2
    nodestring = "";
    n = getNodeFromString(nodestring);

    previous = getPreviousNode(n, h);
    getStringFromNode(previous, result, 256);
    assert_strings_equal_with_message("Test 2", "", result);
    freeNode(n);
    free(previous);

    logInfoLine("Test3: ");
    //test3
    nodestring = "1-2-1-2";
    n = getNodeFromString(nodestring);

    previous = getPreviousNode(n, h);
    getStringFromNode(previous, result, 256);
    assert_strings_equal_with_message("Test 3", "1-2-1-1", result);
    freeNode(n);
    free(previous);

    logInfoLine("Test4: ");
    //test4
    nodestring = "1-2-3-0";
    n = getNodeFromString(nodestring);

    previous = getPreviousNode(n, h);
    getStringFromNode(previous, result, 256);
    assert_strings_equal_with_message("Test 4", "1-2-2-3", result);
    freeNode(n);
    free(previous);

    logInfoLine("Finished Testing isPreviousNode...");
}
