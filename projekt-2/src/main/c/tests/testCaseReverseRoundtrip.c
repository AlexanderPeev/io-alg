#ifndef TEST_CASE_ROUNDTRIP_C
#define TEST_CASE_ROUNDTRIP_C

#include "test.h"
#include "testUtil.h"
#include "../streams/streams.h"

void test_reverse_roundtrip(InputStream is, OutputStream os) {
  char filenameFormat[] = "./reverse-roundtrip-%c.txt";
  ELEMENT element = SOME_ELEMENT;
  ELEMENT* actual;
  HANDLE handle;

  os_create create = os.create;
  os_write write = os.write;
  os_close oclose = os.close;

  is_open open = is.open;
  is_read_next read_next = is.read_next;
  is_close iclose = is.close;

  logInfoLine(fS2("Starting REVERSE ROUNDTRIP test for implementation (%s, %s)", is.name, os.name));

  handle = (*create)(fC(filenameFormat, os.code));
  (*write)(handle, element);
  (*oclose)(handle);

  handle = (*open)(fC(filenameFormat, os.code));
  actual = (*read_next)(handle);
  (*iclose)(handle);

  assert_elements_equal_with_message("Simple reverse roundtrip", element, *actual);

  free_element(actual);

  logInfoLine(fS2("Finished with REVERSE ROUNDTRIP test for implementation (%s, %s)", is.name, os.name));
}

#endif /* TEST_CASE_ROUNDTRIP_C */
