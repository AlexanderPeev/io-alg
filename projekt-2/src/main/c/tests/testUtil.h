#include "../streams/streams.h"
#include "../util/util.h"

BOOL are_equal(ELEMENT a, ELEMENT b);
void assert_elements_equal(ELEMENT expected, ELEMENT actual);
void assert_elements_equal_with_message(char* message, ELEMENT expected, ELEMENT actual);
void assert_bool_equal(BOOL expected, BOOL actual);
void assert_bool_equal_with_message(char* message, BOOL expected, BOOL actual);
void assert_strings_equal(char* expected, char* actual);
void assert_strings_equal_with_message(char* message, char* expected, char* actual);

int assert_result_code();
int benchmark_result_code();
