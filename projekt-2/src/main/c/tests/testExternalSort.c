//
// Created by Karen on 15/11/2016.
//
//#include <mem.h>
#include "testExternalSort.h"
#include "../dataTypes/heap.h"
#include "../dataTypes/buffer.h"
#include "../dataTypes/handle.h"
#include "../streams/inputStream.h"
#include "help_functions.h"


void test_external_sort(InputStream is, OutputStream os, int n, int buffersize, int M, int fanout){
  ELEMENT* elements = (ELEMENT*) calloc(n, element_byte_size());
  int i;
  HANDLE handle;
  ELEMENT element;
  ELEMENT expected;
  ELEMENT* element_pointer;

  logInfoLine("Starting test external sort...");

  handle = (*(os.create))("input-external-sort.txt");
  for(i = 0; i < n; i++){
    element = random_element();
    elements[i] = element;
    (*(os.write))(handle, element);
  }
  (*(os.close))(handle);

  external_sort_file(is, os, "input-external-sort.txt", "output-external-sort.txt", buffersize, fanout, M);

  my_fun_heapsort(elements, n);

  handle = (*(is.open))("output-external-sort.txt");
  for(i = 0; i < n; i++){
    element_pointer = (*(is.read_next))(handle);
    expected = elements[n-i-1];
   // printf("%d:       %d - %d\n",i, *element_pointer, expected);
    assert_elements_equal(expected, *element_pointer);

    free_element(element_pointer);
  }
  (*(is.close))(handle);

  logInfoLine("Finished test external sort...");


  free(elements);
}

void external_sort_test2(InputStream is, OutputStream os, int bufferSize, int M, int fanout){
  heap* h = createEmptyHeap(bufferSize, M, fanout, is, os);
  HANDLE input;
  ELEMENT* element_pointer;
  ELEMENT element;
  ELEMENT* elements;
  long size;
  int i = 0;
  input = (*(is.open))("input-external-sort.txt");
  size = input->handle->fileSize/4;

  elements = (ELEMENT*) calloc(size, element_byte_size());

  while(!(*(is.end_of_stream))(input)){
    element_pointer = (*(is.read_next))(input);
    add(h, *element_pointer);
    elements[i] = *element_pointer;
    i++;

    free_element(element_pointer);
  }
  logInfoLine("Added elements");

  (*(is.close))(input);

  quicksort(elements, 10000);

  i = 0;

  while(!heap_is_empty(h)){
    element = removeMin(h);
    if(!are_equal(element, elements[i])){
      printf("First fault after removing %dth element.\n", i);
      printf("Should be %d, but was %d.\n", elements[i], element);
    }/*
    if(i > 1000){
      if(heap_contains_element(h, 0)){
        printf("Eerste keer 0 gespot als i = %d\n", i);
      }
    }*/

    i++;

  }
  logInfoLine("Removed elements");

}





