#ifndef TEST_CASE_MULTIPLE_STREAMS_C
#define TEST_CASE_MULTIPLE_STREAMS_C

#include <string.h>

#include "test.h"
#include "testUtil.h"
#include "../dataTypes/handle.h"
#include "../streams/streams.h"

void test_reverse_multiple_streams(InputStream is, OutputStream os) {
  const int totalStreams = 8;
  const int totalElements = 3;
  const int totalWrites = 2048;

  int i;
  int j;
  int totalDigitsInTotalStreams = total_decimal_digits(totalStreams);
  int totalDigitsInTotalWrites = total_decimal_digits(totalWrites);
  char filenameFormat[] = "./reverse-multiple-streams-%03d-%c.txt";
  char messageFormat[] = "Reverse multiple streams - stream: %3d, write: %3d, implementation: %c";
  ELEMENT elements[] = {element_for_number(1), element_for_number(2), element_for_number(3)};
  ELEMENT* actual;
  HANDLE* handles = create_handle_array(totalStreams);
  char* filename = create_null_terminated_string(strlen(filenameFormat)
                                                  + totalDigitsInTotalStreams);
  char* message = create_null_terminated_string(strlen(messageFormat)
                                                 + totalDigitsInTotalStreams
                                                 + totalDigitsInTotalWrites);

  os_create create = os.create;
  os_write write = os.write;
  os_close oclose = os.close;

  is_open open = is.open;
  is_read_next read_next = is.read_next;
  is_close iclose = is.close;

  logInfoLine(fS2("Starting REVERSE MULTIPLE STREAMS test for implementation (%s, %s)", is.name, os.name));

  for (i = 0; i < totalStreams; i++) {
    sprintf(filename, filenameFormat, i + 1, os.code);
    handles[i] = (*create)(filename);
  }

  for (j = 0; j < totalWrites; j++) {
    for (i = 0; i < totalStreams; i++) {
      (*write)(handles[i], elements[(j + i) % totalElements]);
    }
  }

  for (i = 0; i < totalStreams; i++) {
    (*oclose)(handles[i]);
  }

  logInfoLine(fS2("Writing is done, switching to reading: REVERSE MULTIPLE STREAMS test for implementation (%s, %s)", is.name, os.name));

  for (i = 0; i < totalStreams; i++) {
    sprintf(filename, filenameFormat, i + 1, os.code);
    handles[i] = (*open)(filename);
  }

  for (j = 0; j < totalWrites; j++) {
    for (i = 0; i < totalStreams; i++) {
      sprintf(message, messageFormat, i + 1, j, os.code);
      actual = (*read_next)(handles[i]);
      assert_elements_equal_with_message(message, elements[(totalWrites - j - 1 + i) % totalElements], *actual);
      free_element(actual);
    }
  }

  for (i = 0; i < totalStreams; i++) {
    (*iclose)(handles[i]);
  }

  free_string(filename);
  free_string(message);
  free_handle_array(handles);

  logInfoLine(fS2("Finished with REVERSE MULTIPLE STREAMS test for implementation (%s, %s)", is.name, os.name));
}

#endif /* TEST_CASE_MULTIPLE_STREAMS_C */

