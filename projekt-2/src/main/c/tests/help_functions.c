//
// Created by Karen on 16/11/2016.
//
#include "help_functions.h"
#include "testUtil.h"

BOOL heap_contains_element(heap* h, ELEMENT element){
  ELEMENT tmp;

  node* root = getNodeFromString("");
  int children = numberOfChilds(root, h);
  int i;
  char string[256];
  node* child;
  BOOL result = FALSE;

  for(i = 0; i < h->root->n; i++){
    tmp = h->root->array[i];
    if(are_equal(tmp, element)){
      printf("Element found in root.\n");
      return TRUE;
    }
  }

  for(i = 0; i < h->buffer->n; i++){
    tmp = h->buffer->array[i];
    if(are_equal(tmp, element)){
      printf("Element found in buffer.\n");
      return TRUE;
    }
  }

  for(i = 0; i < children; i++){
    sprintf(string, "%d", i);
    child = getNodeFromString(string);
    result = result || search_in_child(h, child, element);
    if(result) return TRUE;
    freeNode(child);
  }

  freeNode(root);

  return FALSE;
}

BOOL search_in_child(heap* h, node* n, ELEMENT element){
  char nodename[20];
  HANDLE handle;
  ELEMENT* pointer;
  ELEMENT e;
  node* child;
  int i;
  int children = numberOfChilds(n, h);
  BOOL result = FALSE;
  char childname[256];
  char filename[256];

  getStringFromNode(n, nodename, 20);
  sprintf(filename, "%s.txt", nodename);

  handle = (*(h->is.open))(filename);

  while(!result && !(*(h->is.end_of_stream))(handle)){
    pointer = (*(h->is.read_next))(handle);
    e = *pointer;
    if(are_equal(e, element)){
      printf("Element found in node %s\n", nodename);
      result = TRUE;
    }
    free_element(pointer);
  }
  (*(h->is.close))(handle);

  if(result) return TRUE;

  for(i = 0; i < children; i++){
    sprintf(childname, "%s-%d.txt", nodename, i);
    child = getNodeFromString(childname);
    result = result || search_in_child(h, child, element);
    freeNode(child);

    if(result) return TRUE;
  }

  return result;
}
