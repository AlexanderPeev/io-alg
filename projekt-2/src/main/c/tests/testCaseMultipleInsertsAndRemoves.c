//
// Created by Karen on 2/11/2016.
//

#include "testCases.h"
#include "../dataTypes/heap.h"
#include "testUtil.h"
#include "../streams/inputStream.h"

void test_multiple_insert_and_remove_mins(){
    heap* h = createEmptyHeap(1,1,4,ris_a,os_a);
    int i;


    logInfoLine("Testing multiple inserts and remove mins...");

    for(i = 0; i < 20; i ++){
        add(h, element_for_number(i));
    }

    for(i = 0; i < 20; i++ ){
        ELEMENT e = removeMin(h);
        assert_elements_equal(e, i);
    }

    logInfoLine("Finished multiple inserts and remove mins test");
}

ELEMENT getFirstElement(heap* h, char* filename){
  HANDLE handle;
  ELEMENT e;
  handle = (*(h->is.open))(filename);
  e = *((*(h->is.read_next))(handle));
  (*(h->is.close))(handle);

  return e;
}
