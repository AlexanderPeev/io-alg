//
// Created by Karen on 20/11/2016.
//
//#include <mem.h>
#include "test.h"
#include "../streams/inputStream.h"
#include "../streams/outputStream.h"
#include "../dataTypes/handle.h"

void test_stream_partial(InputStream is, OutputStream os);

void test_stream(){
  init_streams();

  test_stream_partial(ris_a, os_a);
  test_stream_partial(ris_b, os_b);
  test_stream_partial(ris_c, os_c);
 // test_stream_partial(ris_d, os_d);

}

void test_stream_partial(InputStream is, OutputStream os){
  int inputsize[] = {50000,100000};
  int inputsize_size = 2;
  int fanout = 4;
  int nodesize = 5000;
  int buffersize = 5000;
  double tmp, result;
  int i;
  int m;
  int runs = 1;
  char* output_file_name = create_null_terminated_string(256);
  char* file_name = "input-stream-test.txt";

  printf("Running tests for stream %c.\n", is.code);


  for(i = 0; i < inputsize_size; i++){
    generate_random_elements_of_size_and_name(os, inputsize[i], file_name);
    result = 0;
    for (m = 0; m < runs; ++m) {
      //print(tmp result with i, j, k and l
      if(m == 0) {
        printf("Benchmarking for index = %d, input size: %d, fanout: %d, nodesize: %d, buffer: %d, block: %d\n",i, inputsize[i], fanout, nodesize, buffersize, 128);
        logInfoLine("------");
      }
      //  Output file name
      output_file_name[0] = '\0';
      strcat(output_file_name, "output");
//              snprintf(&output_file_name[4], 256-4, "%d", index);
      strcat(output_file_name, ".txt");

      tmp = benchmark_heap_with_params(fanout, nodesize, buffersize, 128, is, os, file_name, output_file_name);

      logInfoLine(fI("Tmp result %d ", m));
      logInfoLine(fD("%f", tmp));
      result  += tmp;
    }
    result /= runs;
    logInfoLine(fD("Average result: %f", result));

  }

  printf("\n");
  free(output_file_name);
}

void compare_streams_partial(InputStream is, OutputStream os){
  long before, after;
  int input = 1000000;
  int i;
  HANDLE handle;
  set_block_size_factor(128);
  ELEMENT* e;
  double time;

  printf("Testing stream %s\n", is.name);

  handle = os.create("compare-stream.txt");
  before = clock();
  for(i = 0; i < input; i++){
    os.write(handle, random_element());
  }
  after = clock();
  os.close(handle);
  time = (double)(after-before)/CLOCKS_PER_SEC;
  printf("Time to write %d elements: %f\n",input, time);

  handle = is.open("compare-stream.txt");
  i = 0;
  before = clock();
  while(!is.end_of_stream(handle)){
    e = is.read_next(handle);
    free(e);
    i++;
  }
  after = clock();
  time = (double)(after-before)/CLOCKS_PER_SEC;
  printf("Time to read %d elements: %f\n",i, time);

//  TODO:
//  cleanup();

}

void compare_streams(){
  init_streams();

  // compare_streams_partial(is_a, os_a);
  // compare_streams_partial(ris_a, os_a);
  compare_streams_partial(is_b, os_b);
  compare_streams_partial(ris_b, os_b);
  compare_streams_partial(is_c, os_c);
  compare_streams_partial(ris_c, os_c);
}
