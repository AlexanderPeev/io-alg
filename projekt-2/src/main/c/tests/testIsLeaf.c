//
// Created by Karen on 25/10/2016.
//

#include "test.h"
#include "testUtil.h"
#include "../dataTypes/heap.h"

void testIsLeaf(){
    node* lastLeaf;
    node* n;
    char *c1, *c2;

    logInfoLine("Start of test is leaf.");

    //test1 true
    c1 = "2-2-2";
    c2 = "2-2-1";
    lastLeaf = getNodeFromString(c2);
    n = getNodeFromString(c1);
    logInfoLine("Test1: ");
    assert_bool_equal(TRUE, isLeaf(n, lastLeaf));
    freeNode(lastLeaf);
    freeNode(n);

    //test2 true
    c1 = "0-2-1";
    c2 = "0-2-1";

    lastLeaf = getNodeFromString(c2);
    n = getNodeFromString(c1);
    logInfoLine("Test2: ");
    assert_bool_equal(TRUE, isLeaf(n, lastLeaf));
    freeNode(lastLeaf);
    freeNode(n);

    //test3 true
    c1 = "0-2-1";
    c2 = "1-1";

    lastLeaf = getNodeFromString(c1);
    n = getNodeFromString(c2);
    logInfoLine("Test3: ");
    assert_bool_equal(TRUE, isLeaf(n, lastLeaf));
    freeNode(lastLeaf);
    freeNode(n);

    //test4 false
    c1 = "0-2-1";
    c2 = "1";

    lastLeaf = getNodeFromString(c1);
    n = getNodeFromString(c2);
    logInfoLine("Test4: ");
    assert_bool_equal(FALSE, isLeaf(n, lastLeaf));
    freeNode(lastLeaf);
    freeNode(n);

    //test5 false
    c1 = "0-2-1";
    c2 = "1-1";

    lastLeaf = getNodeFromString(c1);
    n = getNodeFromString(c2);
    logInfoLine("Test5: ");
    assert_bool_equal(TRUE, isLeaf(n, lastLeaf));
    freeNode(lastLeaf);
    freeNode(n);

    logInfoLine("End of test is leaf.");
}
