//
// Created by Karen on 15/11/2016.
//

#include "../../../../../projekt-1/src/main/c/inputStream.h"
#include "../streams/outputStream.h"
#include "../dataTypes/heap.h"

#ifndef IO_ALG_PROJEKT_2_EXTERNALSORT_H
#define IO_ALG_PROJEKT_2_EXTERNALSORT_H

void external_sort_file(InputStream is, OutputStream os, char* to_sort, char* result, int bufferSize, int fanout, int M);


#endif //IO_ALG_PROJEKT_2_EXTERNALSORT_H
