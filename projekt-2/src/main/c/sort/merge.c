#ifndef MERGE_C
#define MERGE_C

#include <string.h>
#include "merge.h"
#include "sort.h"
#include "../dataTypes/priorityQueue.h"

#define MIN(x, y) (((x) < (y)) ? (x) : (y))

//merges some given sorted inputstreams and writes them to an output stream
void my_fun_merge(InputStream is, OutputStream os, HANDLES input, HANDLE output) {
    int i;
    ELEMENT* el;
    pq_element* element = (pq_element*)calloc(1, sizeof(pq_element));
    is_end_of_stream  end_of_stream = is.end_of_stream;
    is_read_next read_next = is.read_next;

    os_write write = os.write;

    pq_ptr pq = pq_new(input->total);

    for(i = 0; i < input->total; i++){
        el = (*read_next)(input->handles[i]);
        element->data = *el;
        element->inputStream = i;
        pq_push(pq, element);
        free_element(el);
    }

    while(pq->n > 0){
        pq_pop(pq, element);
        (*write)(output, element->data);

        if((*end_of_stream)(input->handles[element->inputStream]) == 0){
            el = (*read_next)(input->handles[element->inputStream]);
            element->data = *el;
            pq_push(pq, element);
            free_element(el);
        }
    }

    free(pq->pq_array);
    free(pq);
    free(element);
}

//we assume that N is the amount of numbers in the file
void my_fun_mergesort2(HANDLE input, HANDLE output, int N, int M, int d, InputStream is, OutputStream os){
    int firstStream = 0;
    ELEMENT* buffer;
    int size;
    int numberOfStreams = 0;
    HANDLE handle;
    int i;
    int remainingStreams;
    int streamsToMerge;
    HANDLE outputStream;
    HANDLES handles;
    char* filenameFormat = "./stream%d.txt";
    char filename[50];
    ELEMENT* read;

    os_create create = os.create;
    os_write write = os.write;
    os_close oclose = os.close;

    is_open open = is.open;
    is_read_next next = is.read_next;
    is_close iclose = is.close;
    is_end_of_stream eof = is.end_of_stream;

    handles = create_handles(d);

    buffer = create_element_array(M);

    //write the parts of the file to streams
    while(!((*eof)(input)) && numberOfStreams*M < N){
        //read the next part
        size = 0;
        while (!((*eof)(input)) && size < M){
            read = ((*next)(input));
            buffer[size] = *read;
            free_element(read);
            size++;
        }

        //write it to a new stream
        if(size > 0){
            numberOfStreams++;

            //sort it internally
            quicksort(buffer, size);

            sprintf(filename, filenameFormat, numberOfStreams);
            handle = (*create)(filename);
            for(i = 0; i < size; i++){
                (*write)(handle, buffer[i]);
            }
            (*oclose)(handle);
        }
    }

    if(numberOfStreams > 0) {
      remainingStreams = numberOfStreams - firstStream;

      while(remainingStreams > 1){
          streamsToMerge = MIN(d, remainingStreams);

          for(i = 0; i < streamsToMerge; i++){
              sprintf(filename, filenameFormat, firstStream + i + 1);
              handles->handles[i] = (*open)(filename);
          }
          handles->total = streamsToMerge;

          numberOfStreams++;
          if(remainingStreams <= d){
            my_fun_merge(is, os, handles, output);
          } else{
              sprintf(filename, filenameFormat, numberOfStreams);
              outputStream = (*create)(filename);

            my_fun_merge(is, os, handles, outputStream);
              (*oclose)(outputStream);
          }

          //remove other unused streams
          for(i = 0; i < streamsToMerge; i++){
              (*iclose)(handles->handles[i]);
              sprintf(filename, filenameFormat, firstStream + i + 1);
              remove(filename);
          }

          firstStream += streamsToMerge;
          remainingStreams = numberOfStreams - firstStream;
      }

    }

    free_element_array(buffer);
    free_handles(handles);
}

#endif /* MERGE_C */
