#ifndef MERGE_H
#define MERGE_H

#include "../streams/inputStream.h"
#include "../streams/outputStream.h"

void my_fun_merge(InputStream is, OutputStream os, HANDLES input, HANDLE output);

void my_fun_mergesort2(HANDLE input, HANDLE output, int N, int M, int d, InputStream is, OutputStream os);

#endif /* MERGE_H */
