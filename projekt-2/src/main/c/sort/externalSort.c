//
// Created by Karen on 15/11/2016.
//
//#include <mem.h>
//#include <mem.h>
#include "externalSort.h"
#include "../streams/inputStream.h"
#include "../streams/outputStream.h"
#include "../dataTypes/handle.h"
#include "../dataTypes/heap.h"
#include "../dataTypes/buffer.h"

void external_sort_file(InputStream is, OutputStream os, char* to_sort, char* result, int bufferSize, int fanout, int M){
  heap* h = createEmptyHeap(bufferSize, M, fanout, is, os);
  HANDLE input;
  HANDLE output;
  ELEMENT* element_pointer;
  ELEMENT element;
  int i = 0;
//  long before, after;
//  double time;

  input = (*(is.open))(to_sort);
//  before = clock();
  while(!(*(is.end_of_stream))(input)){
    element_pointer = (*(is.read_next))(input);
    add(h, *element_pointer);
    i++;
    /* if(!satisfies_heap_conditions(h)){
       printf("Goes wrong after adding element number %d\n", i);
     }*/

    free_element(element_pointer);
  }
//  after = clock();
//  time = (double)(after-before)/CLOCKS_PER_SEC;
//  logWarningLine(fD("Insert took: %lf", time));

//  logInfoLine("Added elements");

  (*(is.close))(input);

  output = (*(os.create))(result);

//  before = clock();
  while(!heap_is_empty(h)){
    element = removeMin(h);
    (*(os.write))(output, element);
    i++;

  }
  freeHeap(h);
  (*(os.close))(output);
}

int get_heap_size(heap* h){
  char nodename[20];
  node* n = h->lastnode;
  char file_name[256];
  int size = 0;
  HANDLE handle;
  node* tmp;

  size += h->buffer->n;
  size += h->root->n;

  while(!isNodeRoot(n)){
    getStringFromNode(n, nodename, 20);
    strcat(nodename, ".txt");


    handle = (*(h->is.open))(nodename);
    size += handle->handle->fileSize/4;
    (*(h->is.close))(handle);

    tmp = n;
    n = getPreviousNode(tmp, h);
    if(!equalNode(tmp, h->lastnode)) freeNode(tmp);
  }
  if(!equalNode(n, h->lastnode)) freeNode(n);

  return size;
}

BOOL satisfies_heap_conditions(heap* h){
  ELEMENT max = 0;
  ELEMENT tmp;
  node* root = getNodeFromString("");
  int children = numberOfChilds(root, h);
  int i;
  char string[256];
  node* child;
  BOOL result = TRUE;

  for(i = 0; i < h->root->n; i++){
    tmp = h->root->array[i];
    max = max > tmp ? max : tmp;
  }

  for(i = 0; i < children; i++){
    sprintf(string, "%d", i);
    child = getNodeFromString(string);
    result = result && satisfy_node(h, child, max);
    if(!result) return FALSE;
    freeNode(child);
  }

  freeNode(root);

  return result;

}

BOOL satisfy_node(heap* h, node* n, int min){
  char nodename[20];
  HANDLE handle;
  ELEMENT previous = min;
  ELEMENT* pointer;
  ELEMENT e;
  node* child;
  int i;
  int children = numberOfChilds(n, h);
  BOOL result = TRUE;
  char childname[256];
  char filename[256];

  getStringFromNode(n, nodename, 20);
  sprintf(filename, "%s.txt", nodename);

  handle = (*(h->is.open))(filename);

  if(handle->handle->fileSize/4 < h->M/2){
    printf("Not enough elements in node %s\n", nodename);
    return FALSE;
  }

  while(!(*(h->is.end_of_stream))(handle)){
    pointer = (*(h->is.read_next))(handle);
    e = *pointer;
    if(previous > e){
      printf("Not sorted in node %s\n", nodename);
      printf("%d comes before %d\n", previous, e);
      return FALSE;
    }
    if(e < min){
      printf("Too big element in node %s\n", nodename);
      printf("Biggest allowed %d, but has %d\n", min, e);
      return FALSE;
    }
    previous = e;

    free_element(pointer);
  }
  (*(h->is.close))(handle);

  for(i = 0; i < children; i++){
    sprintf(childname, "%s-%d.txt", nodename, i);
    child = getNodeFromString(childname);
    result = result && satisfy_node(h, child, previous);
    freeNode(child);
  }

  return result;
}

void printNode(char* nodename, heap* h){
  ELEMENT* pointer;
  ELEMENT e;
  HANDLE handle = (*(h->is.open))(nodename);
  while(!(*(h->is.end_of_stream))(handle)){
    pointer = (*(h->is.read_next))(handle);
    e = *pointer;
    printf("%d\n", e);
    free_element(pointer);
  }
  (*(h->is.close))(handle);
}

void printRoot(heap* h){
  int i;

  printf("Root: \n");
  for(i = 0; i < h->root->n; i++){
    printf("%d\n", h->root->array[i]);
  }
}
