#ifndef RANDOM_ELEMENT_GENERATOR_C
#define RANDOM_ELEMENT_GENERATOR_C

#include "randomElementGenerator.h"

void write_random_elements_to_file(char* file, OutputStream os, long total) {
  HANDLE handle;

  os_create create = os.create;
  os_close close = os.close;

  handle = (*create)(file);

  write_random_elements_to_handle(handle, os, total);

  (*close)(handle);
}


void write_random_elements_to_handle(HANDLE handle, OutputStream os, long total) {
  ELEMENT randomElement;
  long i;

  os_write write = os.write;

  for(i = 0; i < total; i++) {
    randomElement = random_element();
    (*write)(handle, randomElement);
  }
}

#endif /* RANDOM_ELEMENT_GENERATOR_C */
