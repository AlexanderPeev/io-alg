#ifndef REVERSE_INPUT_STREAM_B_H
#define REVERSE_INPUT_STREAM_B_H

#include "inputStream.h"

HANDLE ris_b_open(char* filename);

ELEMENT* ris_b_read_next(HANDLE handle);

BOOL ris_b_end_of_stream(HANDLE handle);

void ris_b_close(HANDLE handle);

#endif /* REVERSE_INPUT_STREAM_B_H */
