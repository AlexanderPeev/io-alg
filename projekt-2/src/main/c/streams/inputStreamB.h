#ifndef INPUT_STREAM_B_H
#define INPUT_STREAM_B_H

#include "inputStream.h"

HANDLE is_b_open(char* filename);

ELEMENT* is_b_read_next(HANDLE handle);

BOOL is_b_end_of_stream(HANDLE handle);

void is_b_close(HANDLE handle);

#endif /* INPUT_STREAM_B_H */
