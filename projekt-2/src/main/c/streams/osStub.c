#ifndef OS_STUB_C
#define OS_STUB_C

#include "streams.h"

#ifndef _WIN32
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>


int is_mmap_valid() {
  return 1;
}

int pagesize() {
  return getpagesize();
}

long long seek_to(int handle, long long offset) {
  return lseek(handle, offset, SEEK_SET);
}

long long seek_from_end(int handle, long long offset) {
  return lseek(handle, offset, SEEK_END);
}

void truncate_to(int handle, int size) {
  ftruncate(handle, size);
}

#endif

#ifdef _WIN32
#include <fcntl.h>
#include <io.h>
#include <stdlib.h>
#include <stdio.h>

#define off_t long long

void *mmap(void *addr,
           size_t length,
           int prot,
           int flags,
           int fd,
           off_t offset) {
  return calloc(length, 1);
}

int munmap(void *addr, size_t length) {
  free(addr);
  return 0;
}

int is_mmap_valid() {
  return 0;
}

int pagesize() {
  return 0; // IDGAF
}

long long seek_to(int handle, long long offset) {
  return _lseeki64(handle, offset, SEEK_SET);
}

long long seek_from_end(int handle, long long offset) {
  return _lseeki64(handle, offset, SEEK_END);
}

void truncate_to(int handle, int size) {
  _chsize(handle, size);
}

#endif /* WIN32 */


#ifndef O_BINARY
#define O_BINARY 0
#endif

void truncate_file_to(char* filename, int size) {
    int handle;
    int closeResult;

    handle = open(filename, O_RDWR | O_BINARY | O_CREAT, S_IWRITE | S_IREAD);

    if(handle == -1) {
      raise_handle_error_with_number(ERROR_CREATE_FAILED, "ERROR during open in truncate_file_to");
    }
    else {
      truncate_to(handle, size);

      closeResult = close(handle);

      if (closeResult == -1) {
        raise_handle_error_with_number(ERROR_OUTPUT_CLOSE_FAILED, "ERROR during close in truncate_file_to");
      }
    }
}


#endif /* OS_STUB_C */
