#ifndef IO_ALG_OUTPUTSTREAM_D_H
#define IO_ALG_OUTPUTSTREAM_D_H

#include "outputStream.h"
BOOL is_stream_d_valid();

int os_d_block_size();
int os_d_block_bytes();
BOOL os_d_buffer_full(MAP_HANDLE mapHandle);
BOOL os_d_buffer_not_empty(MAP_HANDLE mapHandle);
void os_d_buffer_added(MAP_HANDLE mapHandle);
BOOL os_d_is_initial_write(MAP_HANDLE mapHandle);
void* os_d_buffer_write_start(MAP_HANDLE mapHandle);
void os_d_write_block(MAP_HANDLE mapHandle);
void os_d_unmap(MapHandle *mapHandle);
void os_d_map_next(MapHandle *mapHandle);

HANDLE os_d_create(char* filename);

void os_d_write(HANDLE handle, ELEMENT element);

void os_d_close(HANDLE handle);


#endif //IO_ALG_OUTPUTSTREAM_D_H
