#ifndef OUTPUT_STREAM_B_C
#define OUTPUT_STREAM_B_C

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include "osStub.h"
#include "outputStream.h"
#include "outputStreamB.h"

void os_b_wrote_element(FILE_HANDLE fileHandle) {
  fileHandle->offset += element_byte_size();
}

void os_b_write_to_buffer(FILE_HANDLE fileHandle, DATA data, ELEMENT element) {
  data->byteSize = element_byte_size();
  data->data = fileHandle->buffer;

  marshal(data, &element);
}

HANDLE os_b_create(char* filename) {
  HANDLE handle;
  FILE_HANDLE fileHandle;

  handle = create_blank_handle();
  fileHandle = create_file_handle();

  truncate_file_to(filename, 0);

  handle->fileHandle = fileHandle;
  handle->data = create_blank_data();
  fileHandle->eof = FALSE;
  fileHandle->offset = 0;
  fileHandle->buffer = create_buffer(element_byte_size());
  fileHandle->file = fopen(filename, "w+b");

  if(handle->fileHandle->file == NULL) {
    raise_handle_error_with_number(ERROR_CREATE_FAILED_BLANK_RESULT, "ERROR during open in os_b_create - NULL handle");
    perror("WTF: ");
  }
  else if(ferror(handle->fileHandle->file)) {
    raise_handle_error_with_number(ERROR_CREATE_FAILED, "ERROR during open in os_b_create");
    perror("WTF: ");
  }
  return handle;
}

void os_b_write(HANDLE handle, ELEMENT element) {
    FILE_HANDLE fileHandle = handle->fileHandle;
    int elementsWritten = 0;
    int elementSize = element_byte_size();
    int elementsToWrite = 1;

    os_b_write_to_buffer(fileHandle, handle->data, element);

    elementsWritten = fwrite(fileHandle->buffer,
                             elementSize,
                             elementsToWrite,
                             fileHandle->file);

    if(elementsWritten < elementsToWrite) {
      raise_handle_error_with_number(ERROR_WRITE_FAILED, "ERROR during fwrite in os_b_write");
    }

    os_b_wrote_element(fileHandle);
}

void os_b_close(HANDLE handle) {
  FILE_HANDLE fileHandle = handle->fileHandle;

  if(fileHandle->file != NULL && fclose(fileHandle->file) != 0) {
    raise_handle_error_with_number(ERROR_OUTPUT_CLOSE_FAILED, "ERROR during close os_b_close");
  }

  free_buffer(fileHandle->buffer);
  free_file_handle(fileHandle);
  free_data(handle->data);
  free_handle(handle);
}

#endif /* OUTPUT_STREAM_B_C */
