#ifndef INPUT_STREAM_A_H
#define INPUT_STREAM_A_H

#include "inputStream.h"

HANDLE is_a_open(char* filename);

ELEMENT* is_a_read_next(HANDLE handle);

BOOL is_a_end_of_stream(HANDLE handle);

void is_a_close(HANDLE handle);

#endif /* INPUT_STREAM_A_H */
