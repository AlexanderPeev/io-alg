#ifndef OS_STUB_H
#define OS_STUB_H

#ifndef _WIN32
#include <sys/mman.h>
#endif /* WIN32 */

int pagesize();
int is_mmap_valid();

long long seek_to(int handle, long long offset);
long long seek_from_end(int handle, long long offset);
void truncate_to(int handle, int size);
void truncate_file_to(char* handle, int size);

#ifdef _WIN32
#include <fcntl.h>
#include <stdlib.h>

#define PROT_READ 0
#define PROT_WRITE 0
#define MAP_SHARED 0
#define MAP_FAILED 0
#define off_t long long

void *mmap(void *addr,
           size_t length,
           int prot,
           int flags,
           int fd,
           off_t offset);

int munmap(void *addr, size_t length);

#endif /* WIN32 */

#endif /* OS_STUB_H */
