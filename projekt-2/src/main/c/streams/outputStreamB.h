#ifndef OUTPUT_STREAM_B_H
#define OUTPUT_STREAM_B_H

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include "outputStream.h"

void os_b_write_to_buffer(FILE_HANDLE fileHandle, DATA data, ELEMENT element);

HANDLE os_b_create(char* filename);

void os_b_write(HANDLE handle, ELEMENT element);

void os_b_close(HANDLE handle);

#endif /* OUTPUT_STREAM_B_H */
