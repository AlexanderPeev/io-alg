#ifndef REVERSE_INPUT_STREAM_A_C
#define REVERSE_INPUT_STREAM_A_C

#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/stat.h>

#include "inputStream.h"
#include "osStub.h"
#include "reverseInputStreamA.h"

#ifndef O_BINARY
#define O_BINARY 0
#endif

HANDLE ris_a_open(char* filename) {
  HANDLE handle;
  SYSTEM_HANDLE systemHandle = create_system_handle();

  handle = create_blank_handle();

  handle->handle = systemHandle;
  handle->data = create_blank_data();
  systemHandle->eof = FALSE;
  systemHandle->offset = 0;
  systemHandle->loaded = 0;
  systemHandle->buffer = create_buffer(element_byte_size());
  systemHandle->fileSize = file_size(filename);
  systemHandle->bytesRead = 0L;
  systemHandle->handle = open(filename, O_RDONLY | O_BINARY, S_IREAD);

  if(systemHandle->handle == -1) {
    raise_handle_error_with_number(ERROR_OPEN_FAILED, "ERROR during open in ris_a_open");
    systemHandle->eof = TRUE;
  }

  return handle;
}

ELEMENT* ris_a_read_next(HANDLE handle) {
  ELEMENT* element;
  int bytesReadTotal = 0;
  int bytesReadLast = 0;
  int elementSize = element_byte_size();
  DATA data = handle->data;
  SYSTEM_HANDLE systemHandle = handle->handle;

  do {
    seek_from_end(systemHandle->handle, -(systemHandle->bytesRead + elementSize - bytesReadTotal));
    bytesReadLast = read(systemHandle->handle,
                         systemHandle->buffer + bytesReadTotal,
                         elementSize - bytesReadTotal);

    if(bytesReadLast > 0) {
      bytesReadTotal += bytesReadLast;
    }
  }
  while(bytesReadLast > 0 && bytesReadTotal < elementSize);

  if(bytesReadLast == 0) {
    systemHandle->eof = TRUE;
  }
  else if(bytesReadLast == -1) {
    raise_handle_error_with_number(ERROR_READ_FAILED, "ERROR during read in ris_a_read_next");
  }

  systemHandle->bytesRead += bytesReadTotal;

  data->byteSize = bytesReadTotal;
  data->data = (void*)systemHandle->buffer;

  element = create_blank_element();

  if(bytesReadTotal > 0) {
    unmarshal(element, data);
  }

  return element;
}

BOOL ris_a_end_of_stream(HANDLE handle) {
  SYSTEM_HANDLE systemHandle = handle->handle;
  return systemHandle->eof || systemHandle->fileSize <= systemHandle->bytesRead;
}

void ris_a_close(HANDLE handle) {
  SYSTEM_HANDLE systemHandle = handle->handle;
  int result = close(systemHandle->handle);

  if(result == -1) {
    raise_handle_error_with_number(ERROR_INPUT_CLOSE_FAILED, "ERROR during close in ris_a_close");
  }

  free_buffer(systemHandle->buffer);
  free_system_handle(systemHandle);
  free_data(handle->data);
  free_handle(handle);
}

#endif /* REVERSE_INPUT_STREAM_A_C */
