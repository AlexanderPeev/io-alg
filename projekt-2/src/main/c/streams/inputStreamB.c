#ifndef INPUT_STREAM_B_C
#define INPUT_STREAM_B_C

#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "inputStream.h"
#include "inputStreamB.h"

void is_b_bytes_read(FILE_HANDLE fileHandle, int bytesReadTotal) {
  fileHandle->offset += bytesReadTotal;
}

HANDLE is_b_open(char* filename) {
  HANDLE handle;
  FILE_HANDLE fileHandle;

  handle = create_blank_handle();
  fileHandle = create_file_handle();

  handle->fileHandle = fileHandle;
  handle->data = create_blank_data();
  fileHandle->eof = FALSE;
  fileHandle->offset = 0;
  fileHandle->buffer = create_buffer(element_byte_size());
  fileHandle->fileSize = file_size(filename);
  fileHandle->file = fopen(filename, "rb");

  if(handle->fileHandle->file == NULL) {
    raise_handle_error_with_number(ERROR_OPEN_FAILED, "ERROR during open in is_b_open");
    return handle;
  }

  fseek(handle->fileHandle->file, 0, SEEK_SET);
  return handle;
}

ELEMENT* is_b_read_next(HANDLE handle) {
  ELEMENT* element;
  int elementSize = element_byte_size();
  int elementsRead = 0;
  int elementsToRead = 1;
  DATA data = handle->data;
  FILE_HANDLE fileHandle = handle->fileHandle;

  elementsRead = fread(fileHandle->buffer,
                       elementSize,
                       elementsToRead,
                       fileHandle->file);

  if(elementsRead != elementsToRead) {
    if(feof(fileHandle->file)){
      fileHandle->eof = TRUE;
    } else {
      raise_handle_error_with_number(ERROR_READ_FAILED, "ERROR during fread is_b_read_next");
    }
  }

  is_b_bytes_read(fileHandle, elementsRead * elementSize);

  data->byteSize = elementSize;
  data->data = fileHandle->buffer;

  element = create_blank_element();

  if(data->byteSize > 0) {
    unmarshal(element, data);
  }

  return element;
}

BOOL is_b_end_of_stream(HANDLE handle) {
  return handle->fileHandle->eof
         || feof(handle->fileHandle->file)
         || handle->fileHandle->fileSize <= handle->fileHandle->offset;
}

void is_b_close(HANDLE handle) {
  FILE_HANDLE fileHandle = handle->fileHandle;

  if(fileHandle->file != NULL && fclose(fileHandle->file) != 0) {
    raise_handle_error_with_number(ERROR_INPUT_CLOSE_FAILED, "ERROR during close is_b_close");
  }

  free_buffer(fileHandle->buffer);
  free_file_handle(fileHandle);
  free_data(handle->data);
  free_handle(handle);
}

#endif /* INPUT_STREAM_B_C */
