#ifndef OUTPUT_STREAM_D_C
#define OUTPUT_STREAM_D_C

#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>

#include "osStub.h"
#include "outputStream.h"
#include "outputStreamD.h"

#ifndef O_BINARY
#define O_BINARY 0
#endif
#ifndef EWOULDBLOCK

#define EWOULDBLOCK 0
#endif

BOOL is_stream_d_valid() {
  return is_mmap_valid() == 1;
}

//in D, the block size must be a multiple of the page size: 4096
int os_d_block_bytes() {
  return pagesize() * BLOCK_SIZE_FACTOR;
}

int os_d_block_size() {
  return os_d_block_bytes() / element_byte_size();
}

void *os_d_empty_buffer = NULL;

void* os_d_get_empty_buffer() {
  if(os_d_empty_buffer == NULL) {
    os_d_empty_buffer = calloc((size_t) os_d_block_bytes(), 1);
  }
  return os_d_empty_buffer;
}

//returns if the buffer is full
BOOL os_d_buffer_full(MAP_HANDLE mapHandle) {
  return mapHandle->offset == os_d_block_bytes();
}

BOOL os_d_buffer_not_empty(MAP_HANDLE mapHandle){
  return mapHandle->offset > 0;
}

//adapts the offset after an element is written to the buffer
void os_d_buffer_added(MAP_HANDLE mapHandle) {
  mapHandle->offset += element_byte_size();
}

BOOL os_d_is_initial_write(MAP_HANDLE mapHandle){
  return mapHandle->offset == -1;
}

//returns the location where the next element should be written in the buffer
void* os_d_buffer_write_start(MAP_HANDLE mapHandle) {
  return (void*)(mapHandle->map + mapHandle->offset);
}

void os_d_write_to_map(MAP_HANDLE mapHandle, DATA data, ELEMENT element){
  data->byteSize = element_byte_size();
  data->data = os_d_buffer_write_start(mapHandle);

  marshal(data, &element);

  os_d_buffer_added(mapHandle);
}

void os_d_unmap(MAP_HANDLE mapHandle) {
  if (munmap(mapHandle->map, os_d_block_bytes()) == -1) {
    raise_handle_error_with_number(ERROR_UNMAP_FAILED, "ERROR during munmap in os_d_unmap");
  }
}

void os_d_write_block(MAP_HANDLE mapHandle){
  os_d_unmap(mapHandle);
  mapHandle->blocksRead += 1;
  mapHandle->offset = 0;
}

void os_d_map_next(MAP_HANDLE mapHandle) {
  long long startOfEmptyBlock = os_d_block_bytes() * (mapHandle->blocksRead);
  long long endOfEmptyBlock = startOfEmptyBlock+os_d_block_bytes();
  long long result = (long long) seek_to(mapHandle->handle, endOfEmptyBlock - 1);

  if (result == -1) {
    raise_handle_error_with_number(ERROR_SEEK_FAILED, "ERROR during seek in os_d_map_next");
    perror("Error calling seek to 'stretch' the file\n");
  }
  result = (int) write(mapHandle->handle, "", 1);
  if (result != 1) {
    raise_handle_error_with_number(ERROR_STRETCH_FAILED, "ERROR during write (stretch) in os_d_map_next");
  }

  seek_to(mapHandle->handle, 0);

  mapHandle->map = (char*) mmap(NULL, os_d_block_bytes(),
                                PROT_WRITE, MAP_SHARED, mapHandle->handle, startOfEmptyBlock);
  if (mapHandle->map == MAP_FAILED) {
    raise_handle_error_with_number(ERROR_MAP_FAILED, "ERROR during mmap in os_d_map_next");
  }
}

/**
 * Interface implementations
 * */

HANDLE os_d_create(char* filename){
  HANDLE handle;
  MAP_HANDLE mapHandle;

  handle = create_blank_handle();
  mapHandle = create_map_handle();

  handle->mapHandle = mapHandle;
  handle->data = create_blank_data();
  mapHandle->eof = FALSE;
  mapHandle->offset = -1;
  mapHandle->loaded = 0;
  mapHandle->handle = open(filename, O_RDWR | O_BINARY | O_CREAT, S_IWRITE | S_IREAD);
  mapHandle->fileSize = file_size(filename);
  mapHandle->blocksRead = 0;

  if(mapHandle->handle == -1) {
    raise_handle_error_with_number(ERROR_CREATE_FAILED, "ERROR during open in os_d_create");
    mapHandle->eof = TRUE;
  }
  else {
    truncate_to(mapHandle->handle, 0);
  }

  return handle;
}


void os_d_write(HANDLE handle, ELEMENT element) {
  MAP_HANDLE mapHandle = handle->mapHandle;
  if(os_d_is_initial_write(mapHandle)){
    os_d_map_next(mapHandle);
    mapHandle->offset = 0;
  }
  else if(os_d_buffer_full(mapHandle)) {
    os_d_write_block(mapHandle);
    os_d_map_next(mapHandle);
  }
  os_d_write_to_map(mapHandle, handle->data, element);
}

void os_d_close(HANDLE handle){
  int result;
  MAP_HANDLE mapHandle = handle->mapHandle;
  int totalByteSize = mapHandle->blocksRead*os_d_block_bytes() + mapHandle->offset;

  if(os_d_buffer_not_empty(mapHandle)) {
    os_d_write_block(mapHandle);
  }
  truncate_to(mapHandle->handle, totalByteSize);

  result = close(mapHandle->handle);

  if(result == -1) {
    raise_handle_error_with_number(ERROR_OUTPUT_CLOSE_FAILED, "ERROR during close in os_d_close");
  }

  free_map_handle(mapHandle);
  free_data(handle->data);
  free_handle(handle);
}

#endif //OUTPUT_STREAM_D_C
