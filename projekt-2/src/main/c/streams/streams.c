#ifndef STREAMS_C
#define STREAMS_C

#include "streams.h"
#include "osStub.h"

#ifndef O_BINARY
#define O_BINARY 0
#endif

void remove_elements_from_end(char* filename, int total) {
  long fileSize = file_size(filename);
  long bytesToRemove = total * element_byte_size();
  long desiredSize = fileSize - bytesToRemove;
  int handle;
  int closeResult;

  if(desiredSize < 0L) {
    desiredSize = 0;
  }

  handle = open(filename, O_RDWR | O_BINARY | O_CREAT, S_IWRITE | S_IREAD);

  if(handle == -1) {
    raise_handle_error_with_number(ERROR_CREATE_FAILED, "ERROR during open in remove_elements_from_end");
  }
  else {
    truncate_to(handle, desiredSize);

    closeResult = close(handle);

    if (closeResult == -1) {
      raise_handle_error_with_number(ERROR_OUTPUT_CLOSE_FAILED, "ERROR during close in remove_elements_from_end");
    }
  }
}

void init_streams() {
  is_a.name = "InputStream A";
  is_a.code = 'A';
  is_a.open = &is_a_open;
  is_a.read_next = &is_a_read_next;
  is_a.end_of_stream = &is_a_end_of_stream;
  is_a.close = &is_a_close;

  is_b.name = "InputStream B";
  is_b.code = 'B';
  is_b.open = &is_b_open;
  is_b.read_next = &is_b_read_next;
  is_b.end_of_stream = &is_b_end_of_stream;
  is_b.close = &is_b_close;

  is_c.name = "InputStream C";
  is_c.code = 'C';
  is_c.open = &is_c_open;
  is_c.read_next = &is_c_read_next;
  is_c.end_of_stream = &is_c_end_of_stream;
  is_c.close = &is_c_close;

  is_d.name = "InputStream D";
  is_d.code = 'D';
  is_d.open = &is_d_open;
  is_d.read_next = &is_d_read_next;
  is_d.end_of_stream = &is_d_end_of_stream;
  is_d.close = &is_d_close;

  os_a.name = "OutputStream A";
  os_a.code = 'A';
  os_a.create = &os_a_create;
  os_a.write = &os_a_write;
  os_a.close = &os_a_close;

  os_b.name = "OutputStream B";
  os_b.code = 'B';
  os_b.create = &os_b_create;
  os_b.write = &os_b_write;
  os_b.close = &os_b_close;

  os_c.name = "OutputStream C";
  os_c.code = 'C';
  os_c.create = &os_c_create;
  os_c.write = &os_c_write;
  os_c.close = &os_c_close;

  os_d.name = "OutputStream D";
  os_d.code = 'D';
  os_d.create = &os_d_create;
  os_d.write = &os_d_write;
  os_d.close = &os_d_close;

  ris_a.name = "ReverseInputStream A";
  ris_a.code = 'a';
  ris_a.open = &ris_a_open;
  ris_a.read_next = &ris_a_read_next;
  ris_a.end_of_stream = &ris_a_end_of_stream;
  ris_a.close = &ris_a_close;

  ris_b.name = "ReverseInputStream B";
  ris_b.code = 'b';
  ris_b.open = &ris_b_open;
  ris_b.read_next = &ris_b_read_next;
  ris_b.end_of_stream = &ris_b_end_of_stream;
  ris_b.close = &ris_b_close;

  ris_c.name = "ReverseInputStream C";
  ris_c.code = 'c';
  ris_c.open = &ris_c_open;
  ris_c.read_next = &ris_c_read_next;
  ris_c.end_of_stream = &ris_c_end_of_stream;
  ris_c.close = &ris_c_close;

  ris_d.name = "ReverseInputStream D";
  ris_d.code = 'd';
  ris_d.open = &ris_d_open;
  ris_d.read_next = &ris_d_read_next;
  ris_d.end_of_stream = &ris_d_end_of_stream;
  ris_d.close = &ris_d_close;

}

#endif /* STREAMS_C */
