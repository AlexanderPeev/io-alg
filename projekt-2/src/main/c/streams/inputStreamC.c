#ifndef INPUT_STREAM_C_C
#define INPUT_STREAM_C_C

#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>

#include "inputStream.h"
#include "inputStreamC.h"

#define IS_C_BLOCK_SIZE block_size()

#ifndef O_BINARY
#define O_BINARY 0
#endif

int is_c_block_size() {
  return IS_C_BLOCK_SIZE;
}

int is_c_block_bytes() {
  return is_c_block_size() * element_byte_size();
}

void* is_c_buffer_start(SYSTEM_HANDLE systemHandle) {
  return (void*)(systemHandle->buffer + systemHandle->offset);
}

BOOL is_c_buffer_empty(SYSTEM_HANDLE systemHandle) {
  return systemHandle->offset == systemHandle->loaded;
}

void is_c_buffer_fill(SYSTEM_HANDLE systemHandle, int bytesReadTotal) {
  systemHandle->offset = 0;
  systemHandle->loaded = bytesReadTotal;
  systemHandle->bytesRead += bytesReadTotal;
}

void is_c_buffer_poll(SYSTEM_HANDLE systemHandle, int bytesUsedTotal) {
  systemHandle->offset = util_min(systemHandle->offset + bytesUsedTotal, systemHandle->loaded);
}

int is_c_buffer_remaining(SYSTEM_HANDLE systemHandle) {
  return systemHandle->loaded - systemHandle->offset;
}

int is_c_bytes_to_read(int totalBytesToRead, int bytesReadTotal) {
  return totalBytesToRead - bytesReadTotal;
}

void is_c_read_block(HANDLE handle) {
  int bytesReadTotal = 0;
  int bytesReadLast = 0;
  int totalBytesToRead = is_c_block_bytes();
  SYSTEM_HANDLE systemHandle = handle->handle;

  do {
    bytesReadLast = read(systemHandle->handle,
                         systemHandle->buffer + bytesReadTotal,
                         is_c_bytes_to_read(totalBytesToRead, bytesReadTotal));

    if(bytesReadLast > 0) {
      bytesReadTotal += bytesReadLast;
    }
  }
  while(bytesReadLast > 0 && bytesReadTotal < totalBytesToRead);

  is_c_buffer_fill(systemHandle, bytesReadTotal);

  if(bytesReadLast == 0) {
    systemHandle->eof = TRUE;
  }
  else if(bytesReadLast == -1) {
    raise_handle_error_with_number(ERROR_READ_FAILED, "ERROR during read in is_c_read_block");
  }
}

HANDLE is_c_open(char* filename) {
  HANDLE handle;
  SYSTEM_HANDLE systemHandle;

  handle = create_blank_handle();
  systemHandle = create_system_handle();

  handle->handle = systemHandle;
  handle->data = create_blank_data();
  systemHandle->eof = FALSE;
  systemHandle->offset = 0;
  systemHandle->loaded = 0;
  systemHandle->buffer = create_buffer(is_c_block_bytes());
  systemHandle->fileSize = file_size(filename);
  systemHandle->bytesRead = 0L;
  systemHandle->handle = open(filename, O_RDONLY | O_BINARY, S_IREAD);

  if(systemHandle->handle == -1) {
    raise_handle_error_with_number(ERROR_OPEN_FAILED, "ERROR during open in is_c_open");
    systemHandle->eof = TRUE;
  }

  return handle;
}

ELEMENT* is_c_read_next(HANDLE handle) {
  ELEMENT* element;
  int elementSize = element_byte_size();
  DATA data = handle->data;
  SYSTEM_HANDLE systemHandle = handle->handle;

  if(is_c_buffer_empty(systemHandle)) {
    is_c_read_block(handle);
  }

  data->byteSize = util_min(is_c_buffer_remaining(systemHandle), elementSize);
  data->data = is_c_buffer_start(systemHandle);

  is_c_buffer_poll(systemHandle, data->byteSize);

  element = create_blank_element();

  if(data->byteSize > 0) {
    unmarshal(element, data);
  }

  return element;
}

BOOL is_c_end_of_stream(HANDLE handle) {
  SYSTEM_HANDLE systemHandle = handle->handle;
  return is_c_buffer_empty(systemHandle) && (systemHandle->eof || systemHandle->fileSize <= systemHandle->bytesRead);
}

void is_c_close(HANDLE handle) {
  SYSTEM_HANDLE systemHandle = handle->handle;
  int result = close(systemHandle->handle);

  if(result == -1) {
    raise_handle_error_with_number(ERROR_INPUT_CLOSE_FAILED, "ERROR during close in is_c_close");
  }

  free_buffer(systemHandle->buffer);
  free_system_handle(systemHandle);
  free_data(handle->data);
  free_handle(handle);
}

#endif /* INPUT_STREAM_C_C */
