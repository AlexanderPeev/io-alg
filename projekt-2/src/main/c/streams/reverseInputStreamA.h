#ifndef REVERSE_INPUT_STREAM_A_H
#define REVERSE_INPUT_STREAM_A_H

#include "inputStream.h"

HANDLE ris_a_open(char* filename);

ELEMENT* ris_a_read_next(HANDLE handle);

BOOL ris_a_end_of_stream(HANDLE handle);

void ris_a_close(HANDLE handle);

#endif /* REVERSE_INPUT_STREAM_A_H */
