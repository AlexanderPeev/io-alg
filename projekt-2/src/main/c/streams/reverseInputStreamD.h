#ifndef REVERSE_INPUT_STREAM_D_H
#define REVERSE_INPUT_STREAM_D_H

#include "inputStream.h"

long ris_d_block_bytes();
void *ris_d_buffer_start(MAP_HANDLE mapHandle);
void ris_d_buffer_poll(MAP_HANDLE mapHandle);
void ris_d_buffer_fill(MAP_HANDLE mapHandle, long bytesReadTotal);
long ris_d_first_block_offset(MAP_HANDLE mapHandle);
long ris_d_bytes_to_map(MAP_HANDLE mapHandle);
void ris_d_read_block(MAP_HANDLE mapHandle);
void ris_d_unmap(MAP_HANDLE mapHandle);
long ris_d_block_offsets(MAP_HANDLE mapHandle);
BOOL ris_d_is_initial_write(MAP_HANDLE mapHandle);

HANDLE ris_d_open(char *filename);
ELEMENT *ris_d_read_next(HANDLE handle);
BOOL ris_d_end_of_stream(HANDLE handle);
void ris_d_close(HANDLE handle);

#endif /* REVERSE_INPUT_STREAM_D_H */
