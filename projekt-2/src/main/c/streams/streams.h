#ifndef STREAMS_H
#define STREAMS_H

#include "../dataTypes/dataTypes.h"

#include "inputStream.h"
#include "inputStreamA.h"
#include "inputStreamB.h"
#include "inputStreamC.h"
#include "inputStreamD.h"

#include "outputStream.h"
#include "outputStreamA.h"
#include "outputStreamB.h"
#include "outputStreamC.h"
#include "outputStreamD.h"

#include "reverseInputStreamA.h"
#include "reverseInputStreamB.h"
#include "reverseInputStreamC.h"
#include "reverseInputStreamD.h"

InputStream is_a;
InputStream is_b;
InputStream is_c;
InputStream is_d;

OutputStream os_a;
OutputStream os_b;
OutputStream os_c;
OutputStream os_d;

InputStream ris_a;
InputStream ris_b;
InputStream ris_c;
InputStream ris_d;

void init_streams();

void remove_elements_from_end(char* filename, int total);

#endif /* STREAMS_H */
