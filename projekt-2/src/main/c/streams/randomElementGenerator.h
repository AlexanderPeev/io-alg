#ifndef RANDOM_ELEMENT_GENERATOR_H
#define RANDOM_ELEMENT_GENERATOR_H

#include "outputStream.h"

void write_random_elements_to_file(char* file, OutputStream os, long total);
void write_random_elements_to_handle(HANDLE handle, OutputStream os, long total);

#endif /* RANDOM_ELEMENT_GENERATOR_H */
