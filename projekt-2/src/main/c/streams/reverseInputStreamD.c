#ifndef REVERSE_INPUT_STREAM_D_C
#define REVERSE_INPUT_STREAM_D_C

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>

#ifndef O_BINARY
#define O_BINARY 0
#endif

#ifndef S_IREAD
#define S_IREAD 0
#endif

#include "../util/util.h"
#include "inputStream.h"
#include "osStub.h"
#include "reverseInputStreamD.h"

//returns the number of bytes in a block
long ris_d_block_bytes() {
  return pagesize() * BLOCK_SIZE_FACTOR;
}

long ris_d_block_size() {
  return ris_d_block_bytes() / element_byte_size();
}

//returns the next element in the buffer
void *ris_d_buffer_start(MAP_HANDLE mapHandle) {
  return (void *) (mapHandle->map + mapHandle->offset);
}

void ris_d_buffer_poll(MAP_HANDLE mapHandle) {
  mapHandle->offset = mapHandle->offset - element_byte_size();
}

//resets the buffer offset
void ris_d_buffer_fill(MAP_HANDLE mapHandle, long bytesReadTotal) {
  mapHandle->offset = bytesReadTotal - element_byte_size();
  mapHandle->loaded = bytesReadTotal;
  mapHandle->blocksRead ++;
}

long ris_d_first_block_offset(MAP_HANDLE mapHandle) {
  long firstBlockOffset;
  long bytesInABlock = ris_d_block_bytes();

  firstBlockOffset = mapHandle->fileSize % bytesInABlock;

  if(firstBlockOffset == 0 && mapHandle->fileSize > 0) {
    firstBlockOffset = bytesInABlock;
  }

  return firstBlockOffset;
}

long ris_d_bytes_to_map(MAP_HANDLE mapHandle) {
  long bytesToMap;

  if(mapHandle->blocksRead == 0) {
    bytesToMap = ris_d_first_block_offset(mapHandle);
  }
  else {
    bytesToMap = ris_d_block_bytes();
  }

  return bytesToMap;
}

long ris_d_block_offsets(MAP_HANDLE mapHandle) {
  long firstBlockOffset = ris_d_first_block_offset(mapHandle);
  long remainingBlockOffsets = (mapHandle->blocksRead > 1) ? (mapHandle->blocksRead - 1) * ris_d_block_bytes() : 0;

  return firstBlockOffset + remainingBlockOffsets;
}

BOOL ris_d_is_initial_write(MAP_HANDLE mapHandle){
  return mapHandle->blocksRead == 0;
}

//returns if the buffer/map is empty
BOOL ris_d_map_empty(MAP_HANDLE mapHandle) {
  return mapHandle->offset < 0;
}

void ris_d_unmap(MAP_HANDLE mapHandle){
  if (munmap(mapHandle->map, ris_d_block_bytes()) == -1) {
    raise_handle_error_with_number(ERROR_UNMAP_FAILED, "ERROR during munmap in ris_d_unmap");
  }
}

void ris_d_read_block(MAP_HANDLE mapHandle){
  long totalBytesToMap = ris_d_bytes_to_map(mapHandle);
  long seekOffset = ris_d_block_offsets(mapHandle);
  
  if(mapHandle->blocksRead > 0){
    seekOffset += totalBytesToMap;
  }
  
  seek_from_end(mapHandle->handle, -seekOffset);

  mapHandle->map = mmap(NULL, ris_d_block_bytes(), PROT_READ, MAP_SHARED, mapHandle->handle, mapHandle->fileSize - seekOffset);
  if (mapHandle->map == MAP_FAILED)
  {
    logInfoLine("Frederik was here!");
    logInfoLine(fI("start: %d", mapHandle->fileSize - seekOffset));
    raise_handle_error_with_number(ERROR_MAP_FAILED, "ERROR during mmap in ris_d_read_block");
    logErrorLine(fL("Failed to map region of size: %ld", totalBytesToMap));
  }

  ris_d_buffer_fill(mapHandle, totalBytesToMap);
}

/**
 * Interface implementations
 * */

HANDLE ris_d_open(char *filename) {
  HANDLE handle;
  MAP_HANDLE mapHandle;

  handle = create_blank_handle();
  mapHandle = create_map_handle();

  handle->mapHandle = mapHandle;
  handle->data = create_blank_data();
  mapHandle->eof = FALSE;
  mapHandle->offset = -1;
  mapHandle->loaded = 0;
  mapHandle->blocksRead = 0;
  mapHandle->fileSize = file_size(filename);
  mapHandle->handle = open(filename, O_RDONLY | O_BINARY, S_IREAD);

  if (mapHandle->handle == -1) {
    raise_handle_error_with_number(ERROR_OPEN_FAILED, "ERROR during open in ris_d_open");
    mapHandle->eof = TRUE;
  }

  return handle;
}

ELEMENT *ris_d_read_next(HANDLE handle) {
  ELEMENT *element;
  int elementSize = element_byte_size();
  DATA data = handle->data;
  MAP_HANDLE mapHandle = handle->mapHandle;
  //read a new block if necessary
  if(ris_d_is_initial_write(mapHandle)){
    ris_d_read_block(mapHandle);
  }
  else if (ris_d_map_empty(mapHandle)) {
    ris_d_unmap(mapHandle);
    ris_d_read_block(mapHandle);
  }

  data->byteSize = elementSize;
  data->data = ris_d_buffer_start(mapHandle);

  ris_d_buffer_poll(mapHandle);
  element = create_blank_element();

  if (data->byteSize > 0) {
    unmarshal(element, data);
  }

  return element;
}

BOOL ris_d_end_of_stream(HANDLE handle) {
  MAP_HANDLE mapHandle = handle->mapHandle;
  return ris_d_map_empty(mapHandle) &&
        (
            mapHandle->eof ||
            mapHandle->fileSize <= 0 ||
           (
              mapHandle->blocksRead > 0 &&
              mapHandle->fileSize <= ris_d_block_offsets(mapHandle)
           )
        );
}

void ris_d_close(HANDLE handle) {
  MAP_HANDLE mapHandle = handle->mapHandle;
  int result = close(mapHandle->handle);

  if(!ris_d_is_initial_write(mapHandle)){
    ris_d_unmap(mapHandle);
  }

  if (result == -1) {
    raise_handle_error_with_number(ERROR_INPUT_CLOSE_FAILED, "ERROR during close in ris_d_close");
  }

  free_map_handle(mapHandle);
  free_data(handle->data);
  free_handle(handle);
}

#endif /* REVERSE_INPUT_STREAM_D_C */
