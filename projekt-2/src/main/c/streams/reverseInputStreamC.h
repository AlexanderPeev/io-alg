#ifndef REVERSE_INPUT_STREAM_C_H
#define REVERSE_INPUT_STREAM_C_H

#include "inputStream.h"

int ris_c_block_size();

int ris_c_block_bytes();

void* ris_c_buffer_start(SYSTEM_HANDLE systemHandle);

BOOL ris_c_buffer_empty(SYSTEM_HANDLE systemHandle);

void ris_c_buffer_fill(SYSTEM_HANDLE systemHandle, int bytesReadTotal);

void ris_c_buffer_poll(SYSTEM_HANDLE systemHandle, int bytesUsedTotal);

int ris_c_buffer_remaining(SYSTEM_HANDLE systemHandle);

int ris_c_bytes_to_read(int totalBytesToRead, int bytesReadTotal);

void ris_c_read_block(HANDLE handle);

HANDLE ris_c_open(char* filename);

ELEMENT* ris_c_read_next(HANDLE handle);

BOOL ris_c_end_of_stream(HANDLE handle);

void ris_c_close(HANDLE handle);

#endif /* REVERSE_INPUT_STREAM_C_H */
