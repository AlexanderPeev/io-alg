#ifndef OUTPUTSTREAM_H
#define OUTPUTSTREAM_H

#include "../dataTypes/dataTypes.h"

typedef HANDLE (*os_create)(char*);
typedef void (*os_write)(HANDLE, ELEMENT);
typedef void (*os_close)(HANDLE);

typedef struct {
  char* name;
  char code;
  os_create create;
  os_write write;
  os_close close;
} OutputStream;

#endif /* OUTPUTSTREAM_H */
