#ifndef REVERSE_INPUT_STREAM_C_C
#define REVERSE_INPUT_STREAM_C_C

#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>

#include "inputStream.h"
#include "osStub.h"
#include "reverseInputStreamC.h"

#define RIS_C_BLOCK_SIZE block_size()

#ifndef O_BINARY
#define O_BINARY 0
#endif

int ris_c_block_size() {
  return RIS_C_BLOCK_SIZE;
}

int ris_c_block_bytes() {
  return ris_c_block_size() * element_byte_size();
}

void* ris_c_buffer_start(SYSTEM_HANDLE systemHandle) {
  return (void*)(systemHandle->buffer + systemHandle->offset);
}

BOOL ris_c_buffer_empty(SYSTEM_HANDLE systemHandle) {
  return systemHandle->offset < 0;
}

void ris_c_buffer_fill(SYSTEM_HANDLE systemHandle, int bytesReadTotal) {
  systemHandle->offset = bytesReadTotal - element_byte_size();
  systemHandle->loaded = bytesReadTotal;
  systemHandle->bytesRead += bytesReadTotal;
}

void ris_c_buffer_poll(SYSTEM_HANDLE systemHandle, int bytesUsedTotal) {
  systemHandle->offset = systemHandle->offset - bytesUsedTotal;
}

int ris_c_buffer_remaining(SYSTEM_HANDLE systemHandle) {
  return systemHandle->offset + element_byte_size();
}

int ris_c_bytes_to_read(int totalBytesToRead, int bytesReadTotal) {
  return totalBytesToRead - bytesReadTotal;
}

void ris_c_read_block(HANDLE handle) {
  int bytesReadTotal = 0;
  int bytesReadLast = 0;
  int totalBytesToRead = ris_c_block_bytes();
  int bytesToRead;
  SYSTEM_HANDLE systemHandle = handle->handle;

  do {
    bytesToRead = ris_c_bytes_to_read(totalBytesToRead, bytesReadTotal);
    if(systemHandle->bytesRead + totalBytesToRead > systemHandle->fileSize) {
      // do this only once in case of a half-block remaining at the final block read
      totalBytesToRead = systemHandle->fileSize - systemHandle->bytesRead;
      bytesToRead = ris_c_bytes_to_read(totalBytesToRead, bytesReadTotal);
    }
    seek_from_end(systemHandle->handle, -(systemHandle->bytesRead + bytesToRead));

    bytesReadLast = read(systemHandle->handle,
                         systemHandle->buffer + bytesReadTotal,
                         bytesToRead);

    if(bytesReadLast > 0) {
      bytesReadTotal += bytesReadLast;
    }
  }
  while(bytesReadLast > 0 && bytesReadTotal < totalBytesToRead);

  ris_c_buffer_fill(systemHandle, bytesReadTotal);

  if(bytesReadLast == 0) {
    systemHandle->eof = TRUE;
  }
  else if(bytesReadLast == -1) {
    raise_handle_error_with_number(ERROR_READ_FAILED, "ERROR during read in ris_c_read_block");
  }
}

HANDLE ris_c_open(char* filename) {
  HANDLE handle;
  SYSTEM_HANDLE systemHandle;
  char* buf = create_buffer(ris_c_block_bytes());

  handle = create_blank_handle();
  systemHandle = create_system_handle();

  handle->handle = systemHandle;
  handle->data = create_blank_data();
  systemHandle->eof = FALSE;
  systemHandle->offset = -1;
  systemHandle->loaded = 0;
  systemHandle->buffer = buf;
  systemHandle->fileSize = file_size(filename);
  systemHandle->bytesRead = 0L;
  systemHandle->handle = open(filename, O_RDONLY | O_BINARY, S_IREAD);

  if(systemHandle->handle == -1) {
    raise_handle_error_with_number(ERROR_OPEN_FAILED, "ERROR during open in ris_c_open");
    systemHandle->eof = TRUE;
  }

  return handle;
}

ELEMENT* ris_c_read_next(HANDLE handle) {
  ELEMENT* element;
  int elementSize = element_byte_size();
  DATA data = handle->data;
  SYSTEM_HANDLE systemHandle = handle->handle;

  if(ris_c_buffer_empty(systemHandle)) {
    ris_c_read_block(handle);
  }

  data->byteSize = elementSize;
  data->data = ris_c_buffer_start(systemHandle);

  ris_c_buffer_poll(systemHandle, data->byteSize);

  element = create_blank_element();

  if(data->byteSize > 0) {
    unmarshal(element, data);
  }

  return element;
}

BOOL ris_c_end_of_stream(HANDLE handle) {
  SYSTEM_HANDLE systemHandle = handle->handle;
  return ris_c_buffer_empty(systemHandle) && (systemHandle->eof || systemHandle->fileSize <= systemHandle->bytesRead);
}

void ris_c_close(HANDLE handle) {
  SYSTEM_HANDLE systemHandle = handle->handle;
  int result = close(systemHandle->handle);

  if(result == -1) {
    raise_handle_error_with_number(ERROR_INPUT_CLOSE_FAILED, "ERROR during close in ris_c_close");
  }

  free_buffer(systemHandle->buffer);
  free_system_handle(systemHandle);
  free_data(handle->data);
  free_handle(handle);
}

#endif /* REVERSE_INPUT_STREAM_C_C */
