//
// Created by Karen on 21/11/2016.
// code from http://www.cs.princeton.edu/~rs/Algs3.c1-4/code.txt
//

#include "buffer2.h"
#include <stdlib.h>

#define less(A, B) (A > B)
#define exch(A, B) { ELEMENT t = A; A = B; B = t; }

bufferheap2* bh2_new(int maxN){
  bufferheap2* pq = malloc(sizeof(bufferheap2));
  pq->array = calloc(maxN, sizeof(ELEMENT));
  pq->maxSize = maxN;
  pq ->n = 0;
}

void bh2_free(bufferheap2* bh){
  free(bh->array);
  free(bh);
}

void fixUp(bufferheap2* pq, int k) {
  ELEMENT* a = pq->array;
  while (k > 1 && less(a[k/2], a[k])){
    exch(a[k], a[k/2]); k = k/2;
  }
}

void fixDown(bufferheap2* pq, int k, int N){
  int j;
  ELEMENT* a = pq->array;
  while (2*k <= N){
    j = 2*k;
    if (j < N && less(a[j], a[j+1])) j++;
    if (!less(a[k], a[j])) break;
    exch(a[k], a[j]); k = j;
  }
}

int PQempty(bufferheap2* pq){
  return pq->n == 0;
}

void bh2_push(bufferheap2* pq, ELEMENT v){
  pq->n++;
  pq->array[pq->n] = v;
  fixUp(pq, pq->n);
}

ELEMENT bh2_pop(bufferheap2* bh){
  ELEMENT* pq = bh->array;
  int N = bh->n;
  exch(pq[1], pq[N]);
  fixDown(bh, 1, N-1);
  bh->n--;
  return pq[N];
}

ELEMENT bh2_root(bufferheap2* bh){
  return bh->array[0];
}

/*void PQsort(Item a[], int l, int r)
{ int k;
PQinit();
for (k = l; k <= r; k++) PQinsert(a[k]);
for (k = r; k >= l; k--) a[k] = PQdelmax();
}*/
