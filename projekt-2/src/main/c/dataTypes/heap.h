//
// Created by Karen on 25/10/2016.
//

#ifndef IO_ALG_HEAP_H
#define IO_ALG_HEAP_H

#include "element.h"
#include "../streams/inputStream.h"
#include "../streams/outputStream.h"
#include "buffer.h"
#include "node.h"
#include "buffer2.h"

typedef struct{
    int M; //(M/2),..,M elements in one node
    int fanout; //the fan out
    int bufferSize; //the size of the buffer

    node* lastnode;

//    int eltInRoot; //number of elements in the root //TODO: is this ever used, instead we must use n of the root itself
    bufferheap2* root; //the root of the heap, stored in memory

//    int eltInBuffer; //the number of elements in the buffer //TODO: use n of the buffer itself rather than this one
    bufferheap2* buffer; //internal buffer

    InputStream is;
    OutputStream os;
} heap;

typedef struct {
    int type; // 0 for buffer 1 for stream
    HANDLE handle;
    bufferheap2* buffer;
    long totalElements;
} element_source;

int buffer_min(heap* h);

void add(heap* h, ELEMENT e); //adds an element to the heap

BOOL heap_is_empty(heap* h);

ELEMENT removeMin(heap* h); //returns the smallest element of the heap

heap* createEmptyHeap(int bufferSize, int M, int fanout, InputStream is, OutputStream os); //create an empty heap

void freeHeap(heap* h); //free the heap
void freeElementSourceIS(heap* h, element_source* source);
void freeElementSourceOS(heap* h, element_source* source);

int numberOfChilds(node* nde, heap* h);

BOOL isLeaf(node* n, node* lastLeaf);

void removeLastNode(heap* h);

node* getPreviousNode(node* n, heap* h);

#endif //IO_ALG_HEAP_H
