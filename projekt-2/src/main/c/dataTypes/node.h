#ifndef IO_ALG_NODE_H
#define IO_ALG_NODE_H

#include "bool.h"

typedef struct{
    int levels;
    int* indexes;
} node;

node* getNodeFromString(char* str);
node* getNodeFromSizeAndArray(int size, int* arr);
void getStringFromNode(node* n, char* str, int sizeOfString);
void freeNode(node* n);

BOOL isNodeRoot(node* n);
node* getParentNode(node* n);
BOOL equalNode(node* n1, node* n2);

#endif //IO_ALG_NODE_H