#include <stdlib.h>
#include <memory.h>
#include <stdio.h>
#include <string.h>

#include "node.h"
#include "../util/util.h"
#include "heap.h"

//  TODO: implement node functionality

int intFromString(char* str){
  char** end = NULL;
  return (int) strtol(str, end, 10);
}

int countChars( char* s, char c )
{
  return *s == '\0'
         ? 0
         : countChars( s + 1, c ) + (*s == c);
}

int* arrFromString(char* str, int levels){
  int* arr;
  int i = 0;
  const char* s = "-";
  char * token;
  char * tmp = strdup(str);
  arr = malloc(sizeof(int) * levels);

  token = strtok (tmp,s);

  while (token != NULL)
  {
    arr[i] = intFromString(token);
    token = strtok (NULL, s);
    i++;
  }

  free(tmp);
  return arr;
}

node* getNodeFromString(char* str){
  node* n = malloc(sizeof(node));

  n->levels = *str == '\0' ? 0 :countChars(str, '-') + 1;
  n->indexes = arrFromString(str, n->levels);
  return n;
}

node* getNodeFromSizeAndArray(int size, int* arr){
  node* n = (node*) malloc(sizeof(node));

  n->levels = size;
  n->indexes = arr;
  return n;
}

void getStringFromNode(node* n, char* str, int sizeOfString){
  int i;
  int index = 0;

  if(n->levels == 0){
    str[0] = '\0';
  } else{
      for (i=0; i < n->levels; i++) {
          if (i + 1 == n->levels)
              index += snprintf(&str[index], sizeOfString-index, "%d", n->indexes[i]);
          else
              index += snprintf(&str[index], sizeOfString-index, "%d-", n->indexes[i]);
      }
  }
}

void freeNode(node* n){
  if(n->indexes != NULL){
    free(n->indexes);
  }
  free(n);
}

BOOL isNodeRoot(node* n){
  return n->levels == 0;
}


node* getParentNode(node* n){
  int i;

  if(!isNodeRoot(n)){
    node* parent = (node*) malloc(sizeof(node));
    parent->levels = n->levels - 1;
    if(parent->levels != 0){
      parent->indexes = (int*) malloc(sizeof(int) * parent->levels);
      for(i = 0; i < parent->levels; i++){
        parent->indexes[i] = n->indexes[i];
      }
    } else {
      parent->indexes = NULL;
    }
    return parent;
  } else {
    logWarningLine("Node has no parrent");
    return NULL;
  }
}

BOOL equalNode(node* n1, node* n2){
    int i;

    if(n1->levels != n2->levels) return FALSE;
//    if(n1->indexes == n2-> indexes) return FALSE;

    for(i = 0; i < n1->levels; i++){
        if(n1->indexes[i] != n2->indexes[i]) return FALSE;
    }

    return TRUE;
}

