//
// Created by Karen on 25/10/2016.
//

#ifndef IO_ALG_PROJEKT_2_BUFFER_H
#define IO_ALG_PROJEKT_2_BUFFER_H

//
// Created by Karen on 27/09/2016.
//

#ifndef IO_ALG_PRIORITYQUEUE_H
#define IO_ALG_PRIORITYQUEUE_H

#include "element.h"
#include "../../../../../projekt-1/src/main/c/inputStream.h"


typedef struct{
    ELEMENT* array; //array of pq_element-s
    int n;     // number of elements in bufferheap
    int maxSize; //number of max elements in the bufferheap
} bufferheap;

bufferheap* bh_new(int size);

void free_bh(bufferheap* bh);

void bh_push(bufferheap* bh, ELEMENT e);

void bh_adjusttree(bufferheap* bh);

ELEMENT bh_root(bufferheap* bh);

ELEMENT bh_pop(bufferheap* bh);

#endif //IO_ALG_PRIORITYQUEUE_H


#endif //IO_ALG_PROJEKT_2_BUFFER_H
