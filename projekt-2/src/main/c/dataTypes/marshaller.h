#ifndef MARSHALLER_H
#define MARSHALLER_H

#include "dataTypes.h"

void marshal(DATA data, ELEMENT* element);

void unmarshal(ELEMENT* element, DATA data);

int element_byte_size();

#endif /* MARSHALLER_H */