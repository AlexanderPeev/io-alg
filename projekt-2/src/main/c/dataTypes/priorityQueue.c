//
// Created by Karen on 25/10/2016.
// code from http://www.cs.princeton.edu/~rs/Algs3.c1-4/code.txt
//

#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>

#include "priorityQueue.h"

#define less(A, B) (A > B)
#define exch(A, B) { pq_element t = A; A = B; B = t; }

pq_ptr pq_new(int size){
    pq_ptr new_pq;

    if (size < 4)  // allocate at least for 4 elements
        size = 4;
    new_pq = (pq_ptr) malloc(sizeof(pq));
    new_pq->pq_array = (pq_element *)malloc(sizeof(pq_element) * size);
    new_pq->alloc = size;
    new_pq->n = 0;

    return new_pq;
}

void free_pq(pq_ptr pq){
    free(pq->pq_array);
    free(pq);
}

void fixUp_pq(pq_ptr pq, int k) {
  pq_element* a = pq->pq_array;
  while (k > 1 && less(a[k/2].data, a[k].data)){
    exch(a[k], a[k/2]); k = k/2;
  }
}

void fixDown_pq(pq_ptr pq, int k, int N){
  int j;

  pq_element* a = pq->pq_array;
  while (2*k <= N){
    j = 2*k;
    if (j < N && less(a[j].data, a[j+1].data)) j++;
    if (!less(a[k].data, a[j].data)) break;
    exch(a[k], a[j]); k = j;
  }
}

void pq_push(pq_ptr q, pq_element* data){
  q->n++;
  q->pq_array[q->n] = *data;
  fixUp_pq(q, q->n);
}

void pq_display(pq_ptr pq){
    int i;
    printf("Size of priority queue: %d\n", pq->n);
    printf("Space allocated for queue: %d\n", pq->alloc);
    printf("Data in priority queue:\n");
    for (i = 0; i < pq->n; i++)
        printf("%d ", pq->pq_array[i].data);
    printf("\n");
    /*  printf("Priority of the data:\n");
      for (i = 0; i < pq->n; i++)
          printf("%d ", pq->pq_array[i].data);*/
    printf("\n");
}


void pq_pop(pq_ptr q, pq_element* element){
  pq_element* pq = q->pq_array;
  int N = q->n;
  exch(pq[1], pq[N]);

  fixDown_pq(q, 1, N-1);
  element->data = pq[N].data;
  element->inputStream = pq[N].inputStream;

  q->n--;
}
