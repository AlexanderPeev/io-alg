//
// Created by Karen on 21/11/2016.
//

#include "element.h"

#ifndef IO_ALG_PROJEKT_2_BUFFER2_H
#define IO_ALG_PROJEKT_2_BUFFER2_H

typedef struct{
    ELEMENT* array; //array of pq_element-s
    int n;     // number of elements in bufferheap
    int maxSize; //number of max elements in the bufferheap
} bufferheap2;

bufferheap2* bh2_new(int size);

void bh2_free(bufferheap2* bh);

void bh2_push(bufferheap2* bh, ELEMENT e);

void bh2_adjusttree(bufferheap2* bh);

ELEMENT bh2_root(bufferheap2* bh);

ELEMENT bh2_pop(bufferheap2* bh);

#endif //IO_ALG_PROJEKT_2_BUFFER2_H
