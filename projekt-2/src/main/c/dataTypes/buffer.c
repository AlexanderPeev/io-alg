//
// Created by Karen on 25/10/2016.
//

//
// Created by Karen on 27/09/2016.
// reference: https://jlmedina123.wordpress.com/2013/04/08/priority-queue-implemented-with-heap-implemented-with-array-in-c/
//
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>

#include "buffer.h"
#include "../util/util.h"

/* Priority queue implementations:
 *   Unsorted list: O(1) insertion time, O(n) deqeue time (takes O(n) to search)
 *                  we can do this with a linked list
 *   Heap: tree data structure such as parent node always greater than child node.
 *         Unlike BST, there is no left subtree greater than right subtree
 *         Most implementation of heap use an array, where root is at index 0, left
 *         child at index 1, right child inde 2, left child of left child at index 3...
 *         Array can be dynamically resized. Linked list can also be used.
 *         Descending heap or max heap has greatest priority element on top (root). It
 *         supports getMax
 *         Ascending heap or min heap has lowest priority element on top. It supports getMin,
 *         which is called pq_pop here
 */

//create a new priority queue of size size (at least 4)
bufferheap* bh_new(int size){
    bufferheap* new_bh;

    new_bh = (bufferheap*) malloc(sizeof(bufferheap));
    new_bh->array = (ELEMENT*) malloc(sizeof(ELEMENT) * size);
    new_bh->n = 0;
    new_bh->maxSize = size;

    return new_bh;
}

void free_bh(bufferheap* bh){
    free(bh->array);
    free(bh);
}

void bh_push(bufferheap* bh, ELEMENT elt){
    int s, f; // indices to traverse tree (s son, f father)
    ELEMENT priority = elt;

    if(bh->n < bh->maxSize){
        bh->n++; // one more element in PQ
        s = bh->n - 1; // new element is placed at bottom of tree/array
        f = (s-1)/2; //pq_array[f] is father of pq_array[s]
        while ((s > 0) && (priority < bh->array[f]))
        {
            bh->array[s] = bh->array[f]; // shift father down
            s = f; // son takes place of father
            f = (s-1)/2; // father at this new position of son
        }
        //place element here
        //printf("Inserting data %d and priority %d in heap at index %d\n", data, priority, s);
        bh->array[s] = elt;
    } else{
        logErrorLine("ERROR: tried to add element to full heap.");
    }
}

/* adjust tree that lost its root */
void bh_adjusttree(bufferheap* q){
    int p;  // index to parent
    int s1; // index for son 1
    int s2; // index for son 2
    int i;
    int size;
    int current;

    p = 0;
    s1 = 1;
    s2 = 2;

    // readjusting tree since root (pq_array[0]) will be deleted
    do
    {
        // if only one son, or son 1 lower priority
        if ((s2 > q->n-1) || (q->array[s1] < q->array[s2]))
        {
            q->array[p]=q->array[s1]; // son 1 takes place of parent
            p = s1;
        }
        else
        {
            q->array[p]=q->array[s2]; // son 2 takes place of parent
            p = s2;
        }
        s1 = 2*p + 1;
        s2 = 2*p + 2;
    } while (s1 <= q->n-1); // parent doesn't have any children. we are done
    //printf("priority last parent: %d\n", p);

    // rellocate nodes in array after last parent that was moved up
    size = q->n;
    current = p+1;
    while (current != size)
    {
        q->n = current - 1;
        bh_push(q, q->array[current]);
        current++;
    }

    q->n = size-1; // one fewer element in heap
}


ELEMENT bh_pop(bufferheap* q){
    ELEMENT out;

    if (q->n == 0)
    {
        logErrorLine("Underflow error: trying to pop empty queue\n");
        return -1;
    }

    out = q->array[0];

    if (q->n == 1)
        q->n = 0;
    else
        bh_adjusttree(q);

    return out;
}

ELEMENT bh_root(bufferheap* bh){
    if (bh->n == 0)
    {
        logErrorLine("Underflow error: trying to read in empty queue\n");
        return -1;
    }
    return bh->array[0];
}

