#ifndef DATA_H
#define DATA_H

#define DATA Data*

typedef struct {
  int byteSize;
  void* data;
} Data;

DATA create_blank_data();

DATA create_element_data();

void free_element_data(DATA data);

void free_data(DATA data);

#endif /* DATA_H */