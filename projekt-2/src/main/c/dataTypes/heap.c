//
// Created by Karen on 25/10/2016.
//

#include <stdbool.h>
#include <memory.h>
#include "heap.h"
#include "priorityQueue.h"
#include "handle.h"
#include "../streams/inputStream.h"
#include "../streams/osStub.h"
#include "../streams/inputStream.h"
#include "../streams/outputStream.h"
#include "node.h"
#include "../streams/streams.h"
#include "../util/util.h"
#include "buffer2.h"

BOOL heap_is_empty(heap* h){
  return (h->buffer->n == 0) && (h->root->n == 0);
}

int buffer_min(heap* h) {
  return (h->M+1)/2;
}

void createEmptyBuffer(heap *h);

heap* createEmptyHeap(int bufferSize, int M, int fanout, InputStream is, OutputStream os){
  heap* h = calloc(1, sizeof(heap));

  h->bufferSize = bufferSize;
  h->fanout = fanout;
  h->M = M;

  createEmptyBuffer(h);

  h->root = bh2_new(M);

  h->lastnode = getNodeFromString("");
  h->is = is;
  h->os = os;

  return h;
}

void createEmptyBuffer(heap *h) {
  h->buffer = bh2_new(h->bufferSize);
}

void freeHeap(heap* h){
  bh2_free(h->buffer);
  bh2_free(h->root);
  freeNode(h->lastnode);
  free(h);
}

void freeElementSourceIS(heap* h, element_source* source){
  if(source != NULL){
    if(source->type){ //stream
      if(source->handle != NULL){
        (*(h->is.close))(source->handle);
        source->handle = NULL;
      }
    }
    free(source);
  }
}

void freeElementSourceOS(heap* h, element_source* source){
  if(source != NULL){
    if(source->type){ //stream
      if(source->handle != NULL){
        (*(h->os.close))(source->handle);
        source->handle = NULL;
      }
    }
    free(source);
  }
}


// #######################################
//  INSERT FUNCTIONS
// #######################################
BOOL heap_has_buffer_for_node(node *n);
HANDLE heap_create_handle_from_node(heap* h, node* n);

void heap_insert_into_buffer(heap *h, ELEMENT e) {
  bh2_push(h->buffer, e);
}

BOOL heap_buffer_full(heap* h){
  return h->bufferSize == h->buffer->n;
}

void empty_heap_buffer(heap *h) {
  bh2_free(h->buffer);
  createEmptyBuffer(h);
}

//Returns the node width updated indexes from former latest node
void heap_increment_last_node(heap *h){
  node* last = h->lastnode;
  BOOL heap_needs_new_level = TRUE;
  int i = 0;
  int size;
  int* arr = NULL;

  for(i; i < last->levels; i++){
    if(last->indexes[i] < h->fanout-1) {
      heap_needs_new_level = FALSE;
      break;
    }
  }

  if(heap_needs_new_level){
    size = last->levels + 1;
    arr = (int*) malloc(sizeof(int) * size);
    for(i = 0; i < size; i++){
      arr[i] = 0;
    }
  } else {
    size = last->levels;
    arr = (int*) malloc(sizeof(int) * size);

    for(i = 0; i < size; i++){
      arr[i] = last->indexes[i];
    }

    for(i = size-1; i >= 0; i--){
      if(last->indexes[i] == h->fanout-1){
        arr[i] = 0;
      } else {
        arr[i] = last->indexes[i] + 1;
        break;
      }
    }
  }

  freeNode(h->lastnode);
  h->lastnode = getNodeFromSizeAndArray(size, arr);
}

BOOL heap_has_buffer_for_node(node *n){
  if(isNodeRoot(n)) return TRUE;
  return FALSE;
}

bufferheap2* heap_get_buffer_for_node(heap *h, node *n){
  if(isNodeRoot(n)) return h->root;
  return NULL;
}

element_source* heap_get_element_source_from_buffer(heap* h){
  element_source* source = malloc(sizeof(element_source));
  source->type = 0;
  source->buffer = h->buffer;
  source->totalElements = h->buffer->n;
  return source;
}

BOOL element_source_has_next(heap* h, element_source* element){
  if(element->type) { //stream
    return !(*(h->is.end_of_stream))(element->handle);
  } else { //buffer
    return element->buffer->n > 0;
  }
}

ELEMENT element_source_get_next(heap* h, element_source* element){
  ELEMENT* pointer;
  ELEMENT e;

  if(element->type){ //stream
    pointer = (*(h->is.read_next))(element->handle);
    e = *pointer;
    free_element(pointer);
    return e;
  } else { //buffer
    return bh2_pop(element->buffer);
  }
}

//returns the size of the destination
void heap_fill_destination_from_index_with_buffer(heap* h, ELEMENT* destination, int index, element_source* b, long items_left){
//  long before = 0;
//  long after = 0;
//  double time1;

//  before = clock();
  int i = index;
  while(i-index < items_left) {
    destination[i] = element_source_get_next(h, b);
    i++;
  }
//  after = clock();
//  time1 = (double)(after-before)/CLOCKS_PER_SEC;
//  logInfoLine(fD("Fill rest: %lf", time1));

}

void heap_merge_source_and_array_to_array(heap* h, ELEMENT* array, int array_size, element_source* source, ELEMENT* destination){
  int i;
  ELEMENT array_element, source_element;
  int index = 0;
  int size = (h->M)*2;
  long source_elements_used = 0;


  if(source->totalElements == 0){
    for(i = 0; i < array_size; i++){
      destination[i] = array[i];
    }
  } else { //my_fun_merge sources
    array_element = array[index];
    index++;
    source_element = element_source_get_next(h, source);
    source_elements_used ++;
    for(i = 0; i < size; i++){
      if(array_element < source_element){
        destination[i] = array_element;
        if(index < array_size){
          array_element = array[index];
          index++;
        } else {
          destination[i+1] = source_element;
          heap_fill_destination_from_index_with_buffer(h, destination, i+2, source, source->totalElements - source_elements_used);
          return;
        }
      } else {
        destination[i] = source_element;
        if(source->totalElements - source_elements_used > 0){
          source_element = element_source_get_next(h, source);
          source_elements_used ++;
        } else {
          i++;
          destination[i] = array_element;
          for(; index < array_size; index++){
            i++;
            destination[i] = array[index];
          }
          return;
        }
      }
    }

  }
}

void heap_merge_sources_to_array(heap* h, element_source* parent_source, element_source* child_source, ELEMENT* destination, ELEMENT* max_in_parent){
  ELEMENT parent_element, child_element;
  long parent_size = parent_source->totalElements;
  long child_size = child_source->totalElements;
  long size = parent_size + child_size;
  long parent_used = 0;
  long child_used = 0;
  int i;
//
//  long before = 0;
//  long after = 0;
//  double time;

  if(parent_size == 0){
    heap_fill_destination_from_index_with_buffer(h, destination, 0, child_source, child_size);
    return;
  } else if(child_size == 0){
    heap_fill_destination_from_index_with_buffer(h, destination, 0, parent_source, parent_size);
    *max_in_parent = destination[size-1];
    return;
  } else { //my_fun_merge sources
    parent_element = element_source_get_next(h, parent_source);
    parent_used ++;
    child_element = element_source_get_next(h, child_source);
    child_used ++;
    *max_in_parent = parent_element;
//    logInfoLine(fI("Number of elements in array %d", size));

//    before = clock();

    for(i = 0; i < size; i++){
      if(parent_element < child_element){
        destination[i] = parent_element;
        if(parent_size - parent_used > 0){
          parent_element = element_source_get_next(h, parent_source);
          parent_used ++;
        } else {
          *max_in_parent = parent_element;
          i++;
          destination[i] = child_element;
          heap_fill_destination_from_index_with_buffer(h, destination, i+1, child_source, child_size - child_used);
          break;
        }
      } else {
        destination[i] = child_element;
        if(child_size - child_used > 0){
          child_element = element_source_get_next(h, child_source);
          child_used ++;
        } else {
          i++;
          destination[i] = parent_element;
          heap_fill_destination_from_index_with_buffer(h, destination, i+1, parent_source, parent_size - parent_used);
          *max_in_parent = destination[size-1];
          break;
        }
      }
    }
//
//    after = clock();
//    time = (double)(after-before)/CLOCKS_PER_SEC;
//    logInfoLine(fD("forloop took: %lf", time));

  }
}

void heap_merge_sources_to_destination(heap* h, node* parent_node, node* child_node, element_source* parent_source, element_source* child_source, ELEMENT* destination, long size){
  BOOL parentIsRoot = isNodeRoot(parent_node);
  long i = 0;
  long h_in_buffer = -1;
  long k;
  ELEMENT max_in_parent;
  HANDLE parent_handle;
  HANDLE child_handle;


  heap_merge_sources_to_array(h, parent_source, child_source, destination, &max_in_parent);

  //Find size of h
  for(i = 0; i < size; i++){
    if(destination[i] > max_in_parent) {
      h_in_buffer = i;
      break;
    }
  }
  if(h_in_buffer == -1){
    h_in_buffer = size;
  }

  //write the destination to the two nodes
  //Max(ceil(M/2), r-h)
  k = buffer_min(h) > size - h_in_buffer ? buffer_min(h) : size - h_in_buffer;
  k = k > size-h->M ? k : size - h->M;

  child_handle = heap_create_handle_from_node(h, child_node);
  if(!parentIsRoot){
    parent_handle = heap_create_handle_from_node(h, parent_node);
  }

  //TODO: KEEP parent in memory for next iteration instead of writing it to disc.
  for(i = size-1; i >= 0; i--){
    if(i >= size - k){
      (*(h->os.write))(child_handle, destination[i]);
    } else{
      if(parentIsRoot){
        bh2_push(h->root, destination[i]);
      } else{
        (*(h->os.write))(parent_handle, destination[i]);
      }
    }

  }

  if(!parentIsRoot) (*(h->os.close))(parent_handle);
  (*(h->os.close))(child_handle);
}

element_source* heap_get_source_element_from_node(heap* h, node* n){
  char* file_name = create_null_terminated_string(256);
  element_source* source = malloc(sizeof(element_source));
  getStringFromNode(n, file_name, 256);

  if(heap_has_buffer_for_node(n)){
    source->type = 0;
    source->buffer = heap_get_buffer_for_node(h, n);
    source->totalElements = h->root->n;
  } else {
    source->type = 1;

    strcat(file_name, ".txt");

    source->totalElements = file_size(file_name) / element_byte_size();
    source->handle = (*(h->is.open))(file_name);
  }
  free(file_name);
  return source;
}

HANDLE heap_open_file_from_node(heap* h, node* n){
  char* file_name = create_null_terminated_string(256);
  HANDLE handle;

  getStringFromNode(n, file_name, 256);

  strcat(file_name, ".txt");

  handle = (*(h->is.open))(file_name);

  free(file_name);
  return handle;
}

element_source* heap_get_source_element_from_handle(HANDLE handle, node* n){
  char file_name[256];
  file_name[0] = '\0';
  element_source* source = malloc(sizeof(element_source));

  getStringFromNode(n, file_name, 256);

  strcat(file_name, ".txt");

  source->type = 1;
  source->handle = handle;
  source->totalElements = file_size(file_name) / element_byte_size();
  return source;
}

void heap_shift_up(heap* h, node* parent, node* child){
  ELEMENT* merge;
  element_source *sParent, *sChild;
  sParent = heap_get_source_element_from_node(h, parent);
  sChild = heap_get_source_element_from_node(h, child);

  long size = sParent->totalElements + sChild->totalElements;
  merge = malloc(sizeof(ELEMENT)*size);

  heap_merge_sources_to_destination(h, parent, child, sParent, sChild, merge, size);

  freeElementSourceIS(h, sParent);
  freeElementSourceIS(h, sChild);
  free(merge);
}

void heap_heapify(heap* h, node* currentNode){
  node* current = currentNode;
  node* tmp;
  node* parent = NULL;

  while(!isNodeRoot(current)){
    parent = getParentNode(current);
    heap_shift_up(h, parent, current);

    tmp = current;
    current = parent;
    if(h->lastnode!=tmp && tmp != currentNode){
      freeNode(tmp);
    }
  }
  if(current != h->lastnode && current != currentNode){
    freeNode(current);
  }
}

void heap_heapify_from_buffer(heap* h){
//  long before, after;
//  double time1;
  heap_increment_last_node(h);
  node* parent = getParentNode(h->lastnode);
  ELEMENT* merge;
  element_source *sParent, *sChild;

  sParent = heap_get_source_element_from_node(h, parent);
  sChild = heap_get_element_source_from_buffer(h);

  long size = sParent->totalElements + sChild->totalElements;
  merge = malloc(sizeof(ELEMENT) * size);

//  before = clock();
  heap_merge_sources_to_destination(h, parent, h->lastnode, sParent, sChild, merge, size);
//  after = clock();
//  time1 = (double)(after-before)/CLOCKS_PER_SEC;
//  logInfoLine(fD("Merge sources to destination took: %lf", time1));

//  before = clock();
  heap_heapify(h, parent);
//  after = clock();
//  time1 = (double)(after-before)/CLOCKS_PER_SEC;
//  logInfoLine(fD("heapify took: %lf", time1));

  freeElementSourceIS(h, sParent);
  freeElementSourceIS(h, sChild);
  free(merge);
  freeNode(parent);

}

void heap_fill_root_from_buffer(heap *h);

void add(heap *h, ELEMENT e) {
  heap_insert_into_buffer(h, e);

  if(heap_buffer_full(h)){
    if(isNodeRoot(h->lastnode) && h->root->n == 0){
      heap_fill_root_from_buffer(h);
    } else {
      heap_heapify_from_buffer(h);
    }
  }
}

void heap_fill_root_from_buffer(heap *h) {
  int i = 0;
  for(i; i < h->bufferSize; i++){
    bh2_push(h->root, bh2_pop(h->buffer));
  }
}

// #######################################
//  REMOVE MIN FUNCTIONS
// #######################################
ELEMENT removeMinFromRoot(heap* h);
ELEMENT removeMinFromBuffer(heap* h);
int getSmallestFromKids(node* nde, heap* h, ELEMENT* smallest, int n, BOOL* all_kids_are_empty);
bool endOfLevel(node* n);
int numberOfChilds(node* nde, heap* h);
void repairNode(node* n, heap* h);
HANDLE heap_create_handle_from_node(heap* h, node* n);
HANDLE heap_open_handle_from_node(heap* h, node* n);
void repair_internal_node(node* n, element_source* brokenNode, heap* h);
BOOL repairLeaf(node* n, element_source* n_source_element, heap* h);

HANDLE heap_open_handle_from_node(heap* h, node* n){
  char file_name[256];
  HANDLE handle;

  getStringFromNode(n, file_name, 256);
  strcat(file_name, ".txt");

  handle = (*(h->is.open))(file_name);

  return handle;
}

HANDLE heap_create_handle_from_node(heap* h, node* n){
  char file_name[256];
  HANDLE handle;

  getStringFromNode(n, file_name, 256);
  strcat(file_name, ".txt");

  handle = (*(h->os.create))(file_name);

  return handle;
}

ELEMENT removeMin(heap* h){
  ELEMENT rootMin;
  ELEMENT bufferMin;

  if (h->buffer->n == 0){
    if(h->root->n == 0){
      logErrorLine("Underflow error: trying to pop empty heap\n");
      return -1;
    } else{
      return removeMinFromRoot(h);
    }
  } else{
    if(h->root->n == 0) return removeMinFromBuffer(h);
    rootMin = bh2_root(h->root);
    bufferMin = bh2_root(h->buffer);

    if(rootMin > bufferMin){
      return removeMinFromBuffer(h);
    } else{
      return removeMinFromRoot(h);
    }
  }
}

ELEMENT removeMinFromRoot(heap* h){
  int i, n;
  ELEMENT* smallest;
  ELEMENT out = bh2_pop(h->root);
  node* root;
  node* child;
  char* nodename = create_null_terminated_string(256);
  char* filenameFormat = "%d.txt";
  BOOL all_kids_are_empty = FALSE;

  if(h->root->n < buffer_min(h)){
    smallest = calloc(buffer_min(h), element_byte_size());

    root = getNodeFromString("");
    //collect M/2 elements from the kids
    n = getSmallestFromKids(root, h, smallest, buffer_min(h), &all_kids_are_empty);

    //logInfoLine("Got smallest from kids");

    //put them in the root
    for(i = 0; i < n; i++){
      bh2_push(h->root, smallest[i]);
    }
    //if there are not enough elements, the root must become the last because all others are empty
    if(n != buffer_min(h) || all_kids_are_empty){
      if(!equalNode(h->lastnode, root)) {
        freeNode(h->lastnode);
        h->lastnode = root;
      }else{
        freeNode(root);
      }
    } else{
      //fix kids recursively
      for(i = 0; i < numberOfChilds(root, h); i++){
        sprintf(nodename, filenameFormat, i);
        child = getNodeFromString(nodename);

        // recursion down the tree
        repairNode(child, h);

        //logInfoLine(fI("Repaired child with index: %d", i));

        free(child);
      }
      freeNode(root);
    }
    free(smallest);
  }
  free(nodename);
  return out;
}

//repairs a node and returns true if a heapify is done
void repairNode(node* n, heap* h){
  long nodeSize;
  HANDLE brokenNode;
  element_source* brokenNode_source;

  brokenNode = heap_open_file_from_node(h, n);
  brokenNode_source = heap_get_source_element_from_handle(brokenNode, n);
  nodeSize = brokenNode_source->totalElements;

  if(nodeSize < buffer_min(h)){
    if(isLeaf(n, h->lastnode)){
      repairLeaf(n, brokenNode_source, h);
    } else{
      repair_internal_node(n, brokenNode_source, h);
    }
  }

  freeElementSourceIS(h, brokenNode_source);

}

void repair_internal_node(node* n, element_source* brokenNode, heap* h){
  int i, s, elt_in_node;
  int smallest_size;
  ELEMENT* smallest;
  ELEMENT* destination;
  HANDLE node_handle;
  BOOL all_kids_are_empty = FALSE;

  node* child;
  char* nodename = create_null_terminated_string(256);
  char* childname = create_null_terminated_string(256);
  char* filenameFormat = "%s-%d";

  getStringFromNode(n, nodename, 256);

  elt_in_node = brokenNode->totalElements;

  if(elt_in_node < buffer_min(h)){
    smallest = calloc(buffer_min(h), element_byte_size());

    //collect M/2 elements from the kids
    smallest_size = getSmallestFromKids(n, h, smallest, buffer_min(h), &all_kids_are_empty);
    s = smallest_size + elt_in_node;


    //put them in the node
    destination = calloc(s, element_byte_size());

    heap_merge_source_and_array_to_array(h, smallest, smallest_size, brokenNode, destination);

    node_handle = heap_create_handle_from_node(h, n);
    for(i = s-1; i >= 0; i--){
      (*(h->os.write))(node_handle, destination[i]);
    }
    (*(h->os.close))(node_handle);

    //if there are not enough elements, then the only child of the node must have been the last node, and it is empty now
    if(smallest_size < buffer_min(h) || all_kids_are_empty){
      if(!equalNode(n, h->lastnode)) {
        removeLastNode(h);
      }
    } else{
      //fix kids recursively
      for(i = 0; i < numberOfChilds(n, h); i++){
        sprintf(childname, filenameFormat, nodename, i);
        child = getNodeFromString(childname);

        // recursion down the tree
        repairNode(child, h);

        freeNode(child);
      }
    }
    free(smallest);
    free(destination);
  }
  free(childname);
  free(nodename);
}

//the leaf in the array MUST NOT be the last leaf
void repairLeaf_from_array(node* n, ELEMENT* array, int size_array, heap* h){
  long s;
  int i;
  long lastLeafSize;

  ELEMENT* destination;
  HANDLE lastLeaf_handle;
  HANDLE node_handle;
  element_source* lastLeaf;

  if(equalNode(h->lastnode, n)){
    node_handle = heap_create_handle_from_node(h, n);
    for(i = size_array-1; i >= 0; i--){
      (*(h->os.write))(node_handle, array[i]);
    }
    (*(h->os.close))(node_handle);
    return;
  }

  lastLeaf_handle = heap_open_handle_from_node(h, h->lastnode);
  lastLeaf = heap_get_source_element_from_handle(lastLeaf_handle, h->lastnode);
  lastLeafSize = lastLeaf->totalElements;
  s = size_array + lastLeafSize;

  destination = calloc(s, element_byte_size());

  heap_merge_source_and_array_to_array(h, array, size_array, lastLeaf, destination);
  freeElementSourceIS(h, lastLeaf);


  if(s > h->M){
    node_handle = heap_create_handle_from_node(h, n);
    lastLeaf_handle = heap_create_handle_from_node(h, h->lastnode);
    //put M elements in the broken leaf and the others in the last leaf
    for(i = h->M-1; i >= 0; i--){
      (*(h->os.write))(node_handle, destination[i]);
    }
    for(i = s-1; i >= h->M; i--){
      (*(h->os.write))(lastLeaf_handle, destination[i]);
    }
    (*(h->os.close))(node_handle);
    (*(h->os.close))(lastLeaf_handle);

    //heapify with the new leaf
    heap_heapify(h, n);

  } else{
    node_handle = heap_create_handle_from_node(h, n);
    for(i = s-1; i >=0; i--){
      (*(h->os.write))(node_handle, destination[i]);
    }
    (*(h->os.close))(node_handle);

    // remove lastnode
    removeLastNode(h);

    heap_heapify(h, n);
  }

  free(destination);

}

BOOL repairLeaf(node* n, element_source* n_source_element, heap* h){
  long s, i;
  long nodeSize, lastLeafSize;
  element_source* lastLeaf = NULL;
  ELEMENT* destination;
  ELEMENT dummy;
  HANDLE node_handle;
  HANDLE lastLeaf_handle;



  if(!equalNode(n, h->lastnode)){
    lastLeaf_handle = heap_open_handle_from_node(h, h->lastnode);
    lastLeaf = heap_get_source_element_from_handle(lastLeaf_handle, h->lastnode);

    nodeSize = n_source_element->totalElements;
    lastLeafSize = lastLeaf->totalElements;
    s = nodeSize + lastLeafSize;

    destination = calloc(s, element_byte_size());
    //my_fun_merge the leaf with the last leaf
    heap_merge_sources_to_array(h, n_source_element, lastLeaf, destination, &dummy);

    freeElementSourceIS(h, lastLeaf);

    if(s > h->M){
      node_handle = heap_create_handle_from_node(h, n);
      lastLeaf_handle = heap_create_handle_from_node(h, h->lastnode);

      //put M elements in the broken leaf and the others in the last leaf
      for(i = h->M-1; i >= 0; i--){
        (*(h->os.write))(node_handle, destination[i]);
      }
      for(i = s-1; i >= h->M; i--){
        (*(h->os.write))(lastLeaf_handle, destination[i]);
      }
      (*(h->os.close))(node_handle);
      (*(h->os.close))(lastLeaf_handle);

      //heapify with the new leaf
      heap_heapify(h, n);

    } else if(s < buffer_min(h)){
      // remove lastnode
      removeLastNode(h);
      //repeat the repair process
      repairLeaf_from_array(n, destination, s, h);

    } else{
      node_handle = heap_create_handle_from_node(h, n);
      for(i = s-1; i >=0; i--){
        (*(h->os.write))(node_handle, destination[i]);
      }
      (*(h->os.close))(node_handle);

      // remove lastnode
      removeLastNode(h);

      heap_heapify(h, n);
    }

    free(destination);
  } else {
    if(n_source_element->totalElements == 0){
      //the last node is empty, make a new node the last node
      removeLastNode(h);
    }
  }
}

//removes the last node and makes the previous one the last
void removeLastNode(heap* h){
  node* newLastNode = getPreviousNode(h->lastnode, h);
  freeNode(h->lastnode);
  h->lastnode = newLastNode;
}

node* getPreviousNode(node* n, heap* h){
  node* previous = malloc(sizeof(node));
  bool goBack = true;
  int i;

  previous->indexes = malloc(sizeof(int) * n->levels);

  if(isNodeRoot(n)){
    previous->levels = 0;
    return previous;
  }

  if(endOfLevel(n)){
    previous->levels = n->levels -1;
    for(i = 0; i < previous->levels; i++){
      previous->indexes[i] = h->fanout-1;
    }
  } else{
    previous->levels = n->levels;
    for(i = n->levels-1; i >= 0; i--){
      if(goBack){
        if(n->indexes[i] > 0){
          previous->indexes[i] = n->indexes[i] -1;
          goBack = false;
        } else{
          previous->indexes[i] = h->fanout-1;
        }
      } else {
        previous->indexes[i] = n->indexes[i];
      }
    }
  }

  return previous;
}

bool endOfLevel(node* n){
  int i;
  for(i = 0; i < n->levels; i++){
    if(n->indexes[i] > 0) return false;
  }
  return true;
}

//tries to return n smallest element in the children and returns how many are returned
int getSmallestFromKids(node* nde, heap* h, ELEMENT* smallest, int n, BOOL* all_kids_are_empty){
  int i;
  int size;
  ELEMENT* el;
  int childs = numberOfChilds(nde, h);


  HANDLES handles;
  char* filename = create_null_terminated_string(256);
  char* filenameFormat = create_null_terminated_string(256);
  char* nodeName= create_null_terminated_string(256);


  pq_element* element = (pq_element*)calloc(1, sizeof(pq_element));
  is_open open = h->is.open;
  is_read_next next = h->is.read_next;
  is_close iclose = h->is.close;
  is_end_of_stream eof = h->is.end_of_stream;

  pq_ptr pq = pq_new(h->fanout);

  int* toTruncate = (int*) calloc(childs, sizeof(int));
  for(i = 0; i < childs; i++){
    toTruncate[i] = 0;
  }

  handles = create_handles(childs);

  if(isNodeRoot(nde)){
    sprintf(filenameFormat, "%s.txt", "%d");
  } else{
    getStringFromNode(nde, nodeName, 256);
    sprintf(filenameFormat, "%s-%s.txt", nodeName, "%d");
  }

  for(i = 0; i < childs; i++){
    sprintf(filename, filenameFormat, i);
    handles->handles[i] = (*open)(filename);
  }
  handles->total = childs;

  for(i = 0; i < childs; i++){
    el = (*next)(handles->handles[i]);
    element->data = *el;
    element->inputStream = i;
    pq_push(pq, element);
    free_element(el);
  }

  size = 0;

  while(pq->n > 0 && size < n){
    pq_pop(pq, element);
    //remove the element
    toTruncate[element->inputStream] = toTruncate[element->inputStream] + 1;

    smallest[size] = element->data;

    if((*eof)(handles->handles[element->inputStream]) == 0){
      el = (*next)(handles->handles[element->inputStream]);

      element->data = *el;
      pq_push(pq, element);
      free_element(el);
    }
    size++;
  }

  *all_kids_are_empty = TRUE;
  if(pq->n == 0){
    for(i = 0; i < childs; i++){
      if(!(*eof)(handles->handles[i])) *all_kids_are_empty = FALSE;
    }
  } else{
    *all_kids_are_empty = FALSE;
  }

  for(i = 0; i < childs; i++){
    (*iclose)(handles->handles[i]);
  }

  for(i = 0; i < childs; i++){
    if(toTruncate[i] > 0){
      sprintf(filename, filenameFormat, i);
      remove_elements_from_end(filename, toTruncate[i]);
    }
  }

  free(element);
  free_handles(handles);
  free_pq(pq);
  free(toTruncate);
  free(filename);
  free(filenameFormat);
  free(nodeName);

  return size;
}


//returns the number of childs, given the node is NOT a leaf
int numberOfChilds(node* nde, heap* h){
  if(isLeaf(nde, h->lastnode)) return 0;

  node* lastnode = h->lastnode;
  node* lastParent = getParentNode(h->lastnode);

  if(equalNode(lastParent, nde)){
    freeNode(lastParent);
    return lastnode->indexes[lastnode->levels-1]+1;
  }

  freeNode(lastParent);
  return h->fanout;
}

ELEMENT removeMinFromBuffer(heap* h){
  return bh2_pop(h->buffer);
}

BOOL isLeaf(node* n, node* lastLeaf){
  int i, j;

  if(n->levels < lastLeaf->levels-1){
    return FALSE;
  } else if(n->levels == lastLeaf->levels){
    return TRUE;
  } else if(n->levels > 0){
    i = n->levels-1;
    for(j = 0; j < i; j++) {
      if (n->indexes[j] > lastLeaf->indexes[j]) return TRUE;
      if (n->indexes[j] < lastLeaf->indexes[j]) return FALSE;
    }
    if(n->indexes[i] <= lastLeaf->indexes[i]) return FALSE;

    return TRUE;
  } else {
    return FALSE;
  }
}
