Starting a Gradle Daemon, 1 stopped Daemon could not be reused, use --status for details
:projekt-1:compileMainDebugExecutableMainC
:projekt-1:linkMainDebugExecutable
:projekt-1:mainDebugExecutable
:projekt-1:compileMainReleaseExecutableMainC
:projekt-1:linkMainReleaseExecutable
:projekt-1:mainReleaseExecutable
:projekt-1:buildAllExecutables
:projekt-1:runLinux
[34mStarting test run. [0m
[34mTesting marshaller... [0m
[34mStarting tests for marshaller... [0m
[34mFinished with tests for marshaller... [0m
[34mTesting implementations... [0m
[34mStarting tests for implementation (InputStream A, OutputStream A)[0m
[34mStarting ROUNDTRIP test for implementation (InputStream A, OutputStream A)[0m
[34mFinished with ROUNDTRIP test for implementation (InputStream A, OutputStream A)[0m
[34mStarting DUAL ROUNDTRIP test for implementation (InputStream A, OutputStream A)[0m
[34mFinished with DUAL ROUNDTRIP test for implementation (InputStream A, OutputStream A)[0m
[34mStarting MULTIPLE STREAMS test for implementation (InputStream A, OutputStream A)[0m
[34mWriting is done, switching to reading: MULTIPLE STREAMS test for implementation (InputStream A, OutputStream A)[0m
[34mFinished with MULTIPLE STREAMS test for implementation (InputStream A, OutputStream A)[0m
[34mStarting MERGE-ONE test for implementation (InputStream A, OutputStream A)[0m
[34mFinished with MERGE-ONE test for implementation (InputStream A, OutputStream A)[0m
[34mStarting MERGE test for implementation (InputStream A, OutputStream A)[0m
[34mFinished with MERGE test for implementation (InputStream A, OutputStream A)[0m
[34mStarting MERGESORT test for implementation (InputStream A, OutputStream A)[0m
[34mFinished with MERGESORT test for implementation (InputStream A, OutputStream A)[0m
[34mFinished with tests for implementation (InputStream A, OutputStream A)[0m

[34mStarting tests for implementation (InputStream B, OutputStream B)[0m
[34mStarting ROUNDTRIP test for implementation (InputStream B, OutputStream B)[0m
[34mFinished with ROUNDTRIP test for implementation (InputStream B, OutputStream B)[0m
[34mStarting DUAL ROUNDTRIP test for implementation (InputStream B, OutputStream B)[0m
[34mFinished with DUAL ROUNDTRIP test for implementation (InputStream B, OutputStream B)[0m
[34mStarting MULTIPLE STREAMS test for implementation (InputStream B, OutputStream B)[0m
[34mWriting is done, switching to reading: MULTIPLE STREAMS test for implementation (InputStream B, OutputStream B)[0m
[34mFinished with MULTIPLE STREAMS test for implementation (InputStream B, OutputStream B)[0m
[34mStarting MERGE-ONE test for implementation (InputStream B, OutputStream B)[0m
[34mFinished with MERGE-ONE test for implementation (InputStream B, OutputStream B)[0m
[34mStarting MERGE test for implementation (InputStream B, OutputStream B)[0m
[34mFinished with MERGE test for implementation (InputStream B, OutputStream B)[0m
[34mStarting MERGESORT test for implementation (InputStream B, OutputStream B)[0m
[34mFinished with MERGESORT test for implementation (InputStream B, OutputStream B)[0m
[34mFinished with tests for implementation (InputStream B, OutputStream B)[0m

[34mStarting tests for implementation (InputStream C, OutputStream C)[0m
[34mStarting ROUNDTRIP test for implementation (InputStream C, OutputStream C)[0m
[34mFinished with ROUNDTRIP test for implementation (InputStream C, OutputStream C)[0m
[34mStarting DUAL ROUNDTRIP test for implementation (InputStream C, OutputStream C)[0m
[34mFinished with DUAL ROUNDTRIP test for implementation (InputStream C, OutputStream C)[0m
[34mStarting MULTIPLE STREAMS test for implementation (InputStream C, OutputStream C)[0m
[34mWriting is done, switching to reading: MULTIPLE STREAMS test for implementation (InputStream C, OutputStream C)[0m
[34mFinished with MULTIPLE STREAMS test for implementation (InputStream C, OutputStream C)[0m
[34mStarting MERGE-ONE test for implementation (InputStream C, OutputStream C)[0m
[34mFinished with MERGE-ONE test for implementation (InputStream C, OutputStream C)[0m
[34mStarting MERGE test for implementation (InputStream C, OutputStream C)[0m
[34mFinished with MERGE test for implementation (InputStream C, OutputStream C)[0m
[34mStarting MERGESORT test for implementation (InputStream C, OutputStream C)[0m
[34mFinished with MERGESORT test for implementation (InputStream C, OutputStream C)[0m
[34mFinished with tests for implementation (InputStream C, OutputStream C)[0m

[34mStarting tests for implementation (InputStream D, OutputStream D)[0m
[34mStarting ROUNDTRIP test for implementation (InputStream D, OutputStream D)[0m
[34mFinished with ROUNDTRIP test for implementation (InputStream D, OutputStream D)[0m
[34mStarting DUAL ROUNDTRIP test for implementation (InputStream D, OutputStream D)[0m
[34mFinished with DUAL ROUNDTRIP test for implementation (InputStream D, OutputStream D)[0m
[34mStarting MULTIPLE STREAMS test for implementation (InputStream D, OutputStream D)[0m
[34mWriting is done, switching to reading: MULTIPLE STREAMS test for implementation (InputStream D, OutputStream D)[0m
[34mFinished with MULTIPLE STREAMS test for implementation (InputStream D, OutputStream D)[0m
[34mStarting MERGE-ONE test for implementation (InputStream D, OutputStream D)[0m
[34mFinished with MERGE-ONE test for implementation (InputStream D, OutputStream D)[0m
[34mStarting MERGE test for implementation (InputStream D, OutputStream D)[0m
[34mFinished with MERGE test for implementation (InputStream D, OutputStream D)[0m
[34mStarting MERGESORT test for implementation (InputStream D, OutputStream D)[0m
[34mFinished with MERGESORT test for implementation (InputStream D, OutputStream D)[0m
[34mFinished with tests for implementation (InputStream D, OutputStream D)[0m

[34mTesting Priority Queue... [0m
[34mStarting Priority Queue test[0m
[34mFinished with Priority Queue test[0m
[34mTesting Quick Sort... [0m
[34mTesting Heap Sort... [0m
[34mTesting Stream Implementation Benchmark... [0m
[34mRunning Benchmark for block size factor: 1[0m
[34mCalculating for stream count: 1[0m
[34mCalculating for stream count: 2[0m
[34mCalculating for stream count: 4[0m
[34mCalculating for stream count: 8[0m
[34mCalculating for stream count: 16[0m
[34mCalculating for stream count: 32[0m
[34mCalculating for stream count: 64[0m
[34mCalculating for stream count: 128[0m
[34mCalculating for stream count: 256[0m
[34mCalculating for stream count: 512[0m
[34mCalculating for stream count: 1024[0m
[34mCalculating for stream count: 2048[0m
[34mBenchmark results for block size factor: 1[0m
Write results: 
 & A & B & C & D\\
1 & 0.0113s & 0.0006s & 0.0007s & 0.0006s\\
2 & 0.0214s & 0.0013s & 0.0012s & 0.0013s\\
4 & 0.0423s & 0.0025s & 0.0024s & 0.0026s\\
8 & 0.0837s & 0.0048s & 0.0049s & 0.0053s\\
16 & 0.1692s & 0.0098s & 0.0096s & 0.0111s\\
32 & 0.4878s & 0.0210s & 0.0214s & 0.0218s\\
64 & 0.6882s & 0.0550s & 0.0420s & 0.0431s\\
128 & 1.3573s & 0.0849s & 0.0756s & 0.0832s\\
256 & 2.6835s & 0.1541s & 0.1520s & 0.1669s\\
512 & 5.3827s & 0.3120s & 0.3102s & 0.3342s\\
1024 & 10.8066s & 0.6324s & 0.6053s & 0.6786s\\
2048 & 21.4851s & 1.2669s & 1.2287s & 1.3380s\\
Read results: 
 & A & B & C & D\\
1 & 0.0035s & 0.0007s & 0.0008s & 0.0008s\\
2 & 0.0069s & 0.0016s & 0.0015s & 0.0015s\\
4 & 0.0129s & 0.0029s & 0.0030s & 0.0031s\\
8 & 0.0257s & 0.0057s & 0.0060s & 0.0061s\\
16 & 0.0521s & 0.0116s & 0.0121s & 0.0128s\\
32 & 0.1612s & 0.0253s & 0.0245s & 0.0249s\\
64 & 0.2065s & 0.0722s & 0.0543s & 0.0487s\\
128 & 0.4189s & 0.1016s & 0.0932s & 0.0955s\\
256 & 0.8217s & 0.1830s & 0.1868s & 0.1915s\\
512 & 1.6345s & 0.3700s & 0.3812s & 0.3834s\\
1024 & 3.2830s & 0.7564s & 0.7489s & 0.7679s\\
2048 & 6.5891s & 1.4941s & 1.5108s & 1.5383s\\
[34mRunning Benchmark for block size factor: 2[0m
[33mResults for A will not be calculated! [0m
[34mCalculating for stream count: 1[0m
[34mCalculating for stream count: 2[0m
[34mCalculating for stream count: 4[0m
[34mCalculating for stream count: 8[0m
[34mCalculating for stream count: 16[0m
[34mCalculating for stream count: 32[0m
[34mCalculating for stream count: 64[0m
[34mCalculating for stream count: 128[0m
[34mCalculating for stream count: 256[0m
[34mCalculating for stream count: 512[0m
[34mCalculating for stream count: 1024[0m
[34mCalculating for stream count: 2048[0m
[34mBenchmark results for block size factor: 2[0m
[33mResults for A were not calculated! [0m
Write results: 
 & A & B & C & D\\
1 & 0.0000s & 0.0007s & 0.0006s & 0.0007s\\
2 & 0.0000s & 0.0013s & 0.0012s & 0.0013s\\
4 & 0.0000s & 0.0024s & 0.0024s & 0.0026s\\
8 & 0.0000s & 0.0048s & 0.0047s & 0.0052s\\
16 & 0.0000s & 0.0097s & 0.0095s & 0.0104s\\
32 & 0.0000s & 0.0210s & 0.0196s & 0.0254s\\
64 & 0.0000s & 0.0390s & 0.0398s & 0.0419s\\
128 & 0.0000s & 0.0789s & 0.0780s & 0.0822s\\
256 & 0.0000s & 0.1553s & 0.1505s & 0.1655s\\
512 & 0.0000s & 0.3081s & 0.3005s & 0.3346s\\
1024 & 0.0000s & 0.6239s & 0.6043s & 0.6750s\\
2048 & 0.0000s & 1.3202s & 1.2096s & 1.3270s\\
Read results: 
 & A & B & C & D\\
1 & 0.0000s & 0.0007s & 0.0007s & 0.0007s\\
2 & 0.0000s & 0.0014s & 0.0015s & 0.0015s\\
4 & 0.0000s & 0.0029s & 0.0029s & 0.0030s\\
8 & 0.0000s & 0.0058s & 0.0059s & 0.0060s\\
16 & 0.0000s & 0.0114s & 0.0118s & 0.0119s\\
32 & 0.0000s & 0.0256s & 0.0255s & 0.0280s\\
64 & 0.0000s & 0.0462s & 0.0490s & 0.0493s\\
128 & 0.0000s & 0.0931s & 0.0985s & 0.0963s\\
256 & 0.0000s & 0.1833s & 0.1864s & 0.1909s\\
512 & 0.0000s & 0.3646s & 0.3745s & 0.3793s\\
1024 & 0.0000s & 0.7332s & 0.7467s & 0.7730s\\
2048 & 0.0000s & 1.5025s & 1.5030s & 1.5186s\\
[34mRunning Benchmark for block size factor: 3[0m
[33mResults for A will not be calculated! [0m
[34mCalculating for stream count: 1[0m
[34mCalculating for stream count: 2[0m
[34mCalculating for stream count: 4[0m
[34mCalculating for stream count: 8[0m
[34mCalculating for stream count: 16[0m
[34mCalculating for stream count: 32[0m
[34mCalculating for stream count: 64[0m
[34mCalculating for stream count: 128[0m
[34mCalculating for stream count: 256[0m
[34mCalculating for stream count: 512[0m
[34mCalculating for stream count: 1024[0m
[34mCalculating for stream count: 2048[0m
[34mBenchmark results for block size factor: 3[0m
[33mResults for A were not calculated! [0m
Write results: 
 & A & B & C & D\\
1 & 0.0000s & 0.0007s & 0.0006s & 0.0006s\\
2 & 0.0000s & 0.0012s & 0.0012s & 0.0013s\\
4 & 0.0000s & 0.0024s & 0.0023s & 0.0025s\\
8 & 0.0000s & 0.0048s & 0.0047s & 0.0051s\\
16 & 0.0000s & 0.0095s & 0.0093s & 0.0103s\\
32 & 0.0000s & 0.0191s & 0.0187s & 0.0206s\\
64 & 0.0000s & 0.0385s & 0.0380s & 0.0412s\\
128 & 0.0000s & 0.0771s & 0.0758s & 0.0824s\\
256 & 0.0000s & 0.1538s & 0.1495s & 0.1654s\\
512 & 0.0000s & 0.3082s & 0.3049s & 0.3297s\\
1024 & 0.0000s & 0.6454s & 0.6153s & 0.6648s\\
2048 & 0.0000s & 1.2903s & 1.2193s & 1.3376s\\
Read results: 
 & A & B & C & D\\
1 & 0.0000s & 0.0008s & 0.0007s & 0.0007s\\
2 & 0.0000s & 0.0014s & 0.0014s & 0.0015s\\
4 & 0.0000s & 0.0028s & 0.0029s & 0.0029s\\
8 & 0.0000s & 0.0057s & 0.0058s & 0.0059s\\
16 & 0.0000s & 0.0114s & 0.0117s & 0.0119s\\
32 & 0.0000s & 0.0227s & 0.0233s & 0.0236s\\
64 & 0.0000s & 0.0457s & 0.0471s & 0.0476s\\
128 & 0.0000s & 0.0915s & 0.0934s & 0.0954s\\
256 & 0.0000s & 0.1826s & 0.1871s & 0.1888s\\
512 & 0.0000s & 0.3649s & 0.3754s & 0.3778s\\
1024 & 0.0000s & 0.7687s & 0.7596s & 0.7570s\\
2048 & 0.0000s & 1.5549s & 1.4946s & 1.5299s\\
[34mRunning Benchmark for block size factor: 4[0m
[33mResults for A will not be calculated! [0m
[34mCalculating for stream count: 1[0m
[34mCalculating for stream count: 2[0m
[34mCalculating for stream count: 4[0m
[34mCalculating for stream count: 8[0m
[34mCalculating for stream count: 16[0m
[34mCalculating for stream count: 32[0m
[34mCalculating for stream count: 64[0m
[34mCalculating for stream count: 128[0m
[34mCalculating for stream count: 256[0m
[34mCalculating for stream count: 512[0m
[34mCalculating for stream count: 1024[0m
[34mCalculating for stream count: 2048[0m
[34mBenchmark results for block size factor: 4[0m
[33mResults for A were not calculated! [0m
Write results: 
 & A & B & C & D\\
1 & 0.0000s & 0.0006s & 0.0006s & 0.0006s\\
2 & 0.0000s & 0.0012s & 0.0012s & 0.0013s\\
4 & 0.0000s & 0.0024s & 0.0023s & 0.0025s\\
8 & 0.0000s & 0.0048s & 0.0046s & 0.0051s\\
16 & 0.0000s & 0.0095s & 0.0093s & 0.0102s\\
32 & 0.0000s & 0.0195s & 0.0187s & 0.0204s\\
64 & 0.0000s & 0.0383s & 0.0377s & 0.0407s\\
128 & 0.0000s & 0.0779s & 0.0758s & 0.0817s\\
256 & 0.0000s & 0.1565s & 0.1505s & 0.1642s\\
512 & 0.0000s & 0.3099s & 0.3010s & 0.3359s\\
1024 & 0.0000s & 0.6207s & 0.6201s & 0.6601s\\
2048 & 0.0000s & 1.2837s & 1.2169s & 1.3310s\\
Read results: 
 & A & B & C & D\\
1 & 0.0000s & 0.0007s & 0.0007s & 0.0007s\\
2 & 0.0000s & 0.0014s & 0.0015s & 0.0015s\\
4 & 0.0000s & 0.0028s & 0.0029s & 0.0029s\\
8 & 0.0000s & 0.0057s & 0.0058s & 0.0059s\\
16 & 0.0000s & 0.0113s & 0.0116s & 0.0118s\\
32 & 0.0000s & 0.0228s & 0.0232s & 0.0236s\\
64 & 0.0000s & 0.0455s & 0.0468s & 0.0470s\\
128 & 0.0000s & 0.0914s & 0.0940s & 0.0941s\\
256 & 0.0000s & 0.1831s & 0.1863s & 0.1885s\\
512 & 0.0000s & 0.3674s & 0.3737s & 0.3878s\\
1024 & 0.0000s & 0.7365s & 0.7634s & 0.7598s\\
2048 & 0.0000s & 1.5211s & 1.4963s & 1.5203s\\
[34mTesting Experiment Merge Sort... [0m
[34mTime spent on merge sort: 5.432420 seconds[0m
[34mTime spent on merge sort: 2.725106 seconds[0m
[34mTime spent on merge sort: 2.739128 seconds[0m
[34mTime spent on merge sort: 2.745546 seconds[0m
[34mTesting Experiment Merge Sort... [0m
[34mTesting a million elements[0m
[34mRunning time (avg  3) for my_fun_merge sort: 2.744506[0m
[34mRunning time (avg  3) for quick sort: 2.264050[0m
[34mRunning time (avg  3) for heap sort:  0.439164[0m
[34mFinished with test run. [0m
[34mFinished with test run. [0m

BUILD SUCCESSFUL

Total time: 42 mins 6.63 secs
