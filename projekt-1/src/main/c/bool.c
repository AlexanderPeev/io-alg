#ifndef BOOL_C
#define BOOL_C

#include "bool.h"

char* visualize_bool(BOOL value) {
  return value ? "TRUE" : "FALSE";
}

#endif /* BOOL_C */
