#ifndef TEST_CASE_PQ_C
#define TEST_CASE_PQ_C

#include "priorityQueue.h"
#include "inputStream.h"
#include "testDeclarations.h"

void test_pq() {
    pq_ptr pq;
    pq_element* element1;
    pq_element* element2;
    pq_element* element3;
    pq_element* element4;
    pq_element* element5;
    pq_element* popped;

    ELEMENT el4 = element_for_number(4);
    ELEMENT el10 = element_for_number(10);
    ELEMENT el7 = element_for_number(7);
    ELEMENT el5 = element_for_number(5);
    ELEMENT el17 = element_for_number(17);

    logInfoLine("Starting Priority Queue test");

    pq = pq_new(8);

    element1 = (pq_element*)calloc(1, sizeof(pq_element));
    element1->data = el4;
    element1->inputStream = 1;

    element2 = (pq_element*)calloc(1, sizeof(pq_element));
    element2->data = el10;
    element2->inputStream = 1;

    element3 = (pq_element*)calloc(1, sizeof(pq_element));
    element3->data = el7;
    element3->inputStream = 1;

    element4 = (pq_element*)calloc(1, sizeof(pq_element));
    element4->data = el5;
    element4->inputStream = 1;

    element5 = (pq_element*)calloc(1, sizeof(pq_element));
    element5->data = el17;
    element5->inputStream = 1;

    popped = (pq_element*)calloc(1, sizeof(pq_element));

    pq_push(pq, element1);
    pq_push(pq, element2);
    pq_push(pq, element3);
    pq_push(pq, element4);
    pq_push(pq, element5);

    pq_pop(pq, popped);
    assert_elements_equal_with_message("Priority Queue - element  4 must be 1st: ", el4, popped->data);
    pq_pop(pq, popped);
    assert_elements_equal_with_message("Priority Queue - element  5 must be 2nd: ", el5, popped->data);
    pq_pop(pq, popped);
    assert_elements_equal_with_message("Priority Queue - element  7 must be 3rd: ", el7, popped->data);
    pq_pop(pq, popped);
    assert_elements_equal_with_message("Priority Queue - element 10 must be 4th: ", el10, popped->data);
    pq_pop(pq, popped);
    assert_elements_equal_with_message("Priority Queue - element 17 must be 5th: ", el17, popped->data);

    // pq_display(pq);

    free(pq->pq_array);
    free(pq);
    free(element1);
    free(element2);
    free(element3);
    free(element4);
    free(element5);

    logInfoLine("Finished with Priority Queue test");
}

#endif /* TEST_CASE_PQ_C */

