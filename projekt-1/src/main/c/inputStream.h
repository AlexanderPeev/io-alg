#ifndef INPUTSTREAM_H
#define INPUTSTREAM_H

#include "dataTypes.h"

typedef HANDLE (*is_open)(char*);
typedef ELEMENT* (*is_read_next)(HANDLE);
typedef BOOL (*is_end_of_stream)(HANDLE);
typedef void (*is_close)(HANDLE);

typedef struct {
  char* name;
  char code;
  is_open open;
  is_read_next read_next;
  is_end_of_stream end_of_stream;
  is_close close;
} InputStream;

#endif /* INPUTSTREAM_H */
