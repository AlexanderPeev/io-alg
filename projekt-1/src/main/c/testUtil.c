#include <string.h>

#include "streams.h"
#include "testDeclarations.h"


BOOL are_equal(ELEMENT a, ELEMENT b) {
  if(memcmp(&a, &b, element_byte_size()) == 0) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

void assert_elements_equal(ELEMENT expected, ELEMENT actual) {
  assert_elements_equal_with_message("", expected, actual);
}

void assert_elements_equal_with_message(char* message, ELEMENT expected, ELEMENT actual) {
  if(!are_equal(expected, actual)) {
    char* sExpected = visualize_element(expected);
    char* sActual = visualize_element(actual);

    logErrorLine(fS("ASSERT FAILED! Elements are not equal: %s", message));
    logErrorLine(fS("\t\tExpected: %s", sExpected));
    logErrorLine(fS("\t\tActual:   %s", sActual));

    free(sExpected);
    free(sActual);
  }
}

void assert_bool_equal(BOOL expected, BOOL actual) {
  assert_bool_equal_with_message("", expected, actual);
}

void assert_bool_equal_with_message(char* message, BOOL expected, BOOL actual) {
  if(expected != actual) {
    char* sExpected = visualize_bool(expected);
    char* sActual = visualize_bool(actual);

    logErrorLine(fS("ASSERT FAILED! Boolean values are not equal: %s", message));
    logErrorLine(fS("\t\tExpected: %s", sExpected));
    logErrorLine(fS("\t\tActual:   %s", sActual));

  }
}
