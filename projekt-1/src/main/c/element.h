#ifndef ELEMENT_H
#define ELEMENT_H

#include <stdint.h>
#include <stdlib.h>
#include <time.h>

#define ELEMENT int32_t
#define SOME_ELEMENT (ELEMENT)42
#define SOME_OTHER_ELEMENT (ELEMENT)1337
#define SETUP_RANDOM_ELEMENT srand((unsigned int)time(NULL));
#define RANDOM_ELEMENT ((ELEMENT)rand())
#define NUMBER_TO_ELEMENT(number) ((ELEMENT)number)

#define BLOCK_SIZE_FACTOR block_size_factor()
#define BLOCK_SIZE (1024 * BLOCK_SIZE_FACTOR)

ELEMENT* create_blank_element();
ELEMENT random_element();
ELEMENT element_for_number(int number);

char* visualize_element(ELEMENT element);

void dump_element(char* message, ELEMENT element);

void free_element(ELEMENT* element);

ELEMENT* create_element_array(int total);

void free_element_array(ELEMENT* elementArray);

char* create_buffer(int byteSize);

void free_buffer(char* buffer);

int block_size();

int block_size_factor();
void set_block_size_factor(int factor);

#endif /* ELEMENT_H */
