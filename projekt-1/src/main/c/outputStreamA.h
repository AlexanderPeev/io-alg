#ifndef OUTPUT_STREAM_A_H
#define OUTPUT_STREAM_A_H

#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>

#include "outputStream.h"
#include "handle.h"

BOOL os_a_non_fatal_errno();

HANDLE os_a_create(char* filename);

void os_a_write(HANDLE handle, ELEMENT element);

void os_a_close(HANDLE handle);

#endif /* OUTPUT_STREAM_A_H */
