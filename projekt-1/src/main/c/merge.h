#ifndef MERGE_H
#define MERGE_H

#include "inputStream.h"
#include "outputStream.h"

void merge(InputStream is, OutputStream os, HANDLES input, HANDLE output);

void my_fun_mergesort(HANDLE input, HANDLE output, int N, int M, int d, InputStream is, OutputStream os);

#endif /* MERGE_H */
