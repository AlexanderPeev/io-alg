#ifndef TEST_CASE_DECLARATIONS_H
#define TEST_CASE_DECLARATIONS_H

#include "streams.h"

void test_marshaller_roundtrip(ELEMENT element);
void test_roundtrip(InputStream is, OutputStream os);
void test_dual_roundtrip(InputStream is, OutputStream os);
void test_multiple_streams(InputStream is, OutputStream os);
void test_pq();
void test_quicksort();
void test_heapsort();
void test_merge_one(InputStream is, OutputStream os);
void test_merge(InputStream is, OutputStream os);
void test_eof(InputStream is, OutputStream os);
void test_mergesort(InputStream is, OutputStream os);
void testParameters(InputStream is, OutputStream os);

#endif /* TEST_CASE_DECLARATIONS_H */

