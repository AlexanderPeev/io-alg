#ifndef UTIL_H
#define UTIL_H

#include <math.h>
#include <stdlib.h>

int total_decimal_digits (int number);

int util_min (int a, int b);
long util_long_min (long a, long b);
int file_size_from_file_descriptor (int fd);

char* create_null_terminated_string(int length);

void free_string(char* nullTerminatedString);

int fallback_errno();
long file_size(char* filename);

char* fC(char* format, char arg);
char* fD(char* format, double arg);
char* fS(char* format, char* arg);
char* fS2(char* format, char* arg1, char* arg2);
char* fI(char* format, int arg);
char* fL(char* format, long arg);

void logNewLine();
void logLine(char* message);
void logErrorLine(char* message);
void logWarningLine(char* message);
void logSuccessLine(char* message);
void logInfoLine(char* message);

#endif /* UTIL_H */
