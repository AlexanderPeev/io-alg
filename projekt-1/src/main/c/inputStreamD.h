#ifndef IO_ALG_INPUTSTREAM_D_H
#define IO_ALG_INPUTSTREAM_D_H

#include "inputStream.h"

long is_d_block_size();
long is_d_block_bytes();
void *is_d_buffer_start(MAP_HANDLE mapHandle);
void is_d_buffer_poll(MAP_HANDLE mapHandle);
void is_d_buffer_fill(MAP_HANDLE mapHandle, long bytesReadTotal);
long is_d_bytes_to_map(MAP_HANDLE mapHandle);
void is_d_read_block(MAP_HANDLE mapHandle);
void is_d_unmap(MAP_HANDLE mapHandle);
BOOL is_d_is_initial_write(MAP_HANDLE mapHandle);

HANDLE is_d_open(char *filename);
ELEMENT *is_d_read_next(HANDLE handle);
BOOL is_d_end_of_stream(HANDLE handle);
void is_d_close(HANDLE handle);

#endif //IO_ALG_INPUTSTREAM_D_H
