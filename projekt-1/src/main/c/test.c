#include "testCaseDeclarations.h"
#include "streams.h"
#include "experiment.h"
#include "randomElementGenerator.h"
#include "sort.h"
#include "benchmarkSortings.h"

void test_marshaller() {
  logInfoLine("Starting tests for marshaller... ");

  test_marshaller_roundtrip(SOME_ELEMENT);
  test_marshaller_roundtrip(SOME_OTHER_ELEMENT);

  logInfoLine("Finished with tests for marshaller... ");
}

void test_implementation(InputStream is, OutputStream os) {
  logInfoLine(fS2("Starting tests for implementation (%s, %s)", is.name, os.name));

  test_roundtrip(is, os);
  test_dual_roundtrip(is, os);
  test_multiple_streams(is, os);
  test_merge_one(is, os);
  test_merge(is, os);
  test_mergesort(is, os);
  // TODO

  logInfoLine(fS2("Finished with tests for implementation (%s, %s)", is.name, os.name));
  logNewLine();
}

void run_tests() {
  int totalElements;
  int blockSize;
  char* mergeTestFileName;
  double runningTime;

  logInfoLine(fI("Size of int: %d", sizeof(int)));
  logInfoLine(fI("Size of long: %d", sizeof(long)));

  logInfoLine("Starting test run. ");

  init_streams();

//  logInfoLine("Testing marshaller... ");
//
//  test_marshaller();

//  logInfoLine("Testing implementations... ");

//  test_implementation(is_a, os_a);
//  test_implementation(is_b, os_b);
//  test_implementation(is_c, os_c);
//  if(is_stream_d_valid()) {
//    test_implementation(is_d, os_d);
//  }
//  else {
//    logWarningLine(fS2("Skipping tests for implementation (%s, %s) as it is not valid", is_d.name, os_d.name));
//  }

//  logInfoLine("Testing Priority Queue... ");
//
//  test_pq();
//
//  logInfoLine("Testing Quick Sort... ");
//
//  test_quicksort();
//
//  logInfoLine("Testing Heap Sort... ");
//
//  test_heapsort();

  /*

  logInfoLine("Testing Stream Implementation Benchmark... ");

  set_block_size_factor(1);
  do_read_and_write_benchmark(TRUE);
  set_block_size_factor(2);
  do_read_and_write_benchmark(FALSE);
  set_block_size_factor(3);
  do_read_and_write_benchmark(FALSE);
  set_block_size_factor(4);
  do_read_and_write_benchmark(FALSE);
  set_block_size_factor(8);
  do_read_and_write_benchmark(FALSE);
  set_block_size_factor(16);
  do_read_and_write_benchmark(FALSE);

  set_block_size_factor(32);
  do_read_and_write_benchmark(FALSE);
  set_block_size_factor(64);
  do_read_and_write_benchmark(FALSE);
  set_block_size_factor(128);
  do_read_and_write_benchmark(FALSE);
  set_block_size_factor(256);
  do_read_and_write_benchmark(FALSE);
  set_block_size_factor(512);
  do_read_and_write_benchmark(FALSE);

  */

  /*logInfoLine("Testing Experiment Merge Sort... ");

  experiment_merge_sort(is_a, os_a, 1000000, 10000, 100);
  experiment_merge_sort(is_b, os_b, 1000000, 10000, 100);
  experiment_merge_sort(is_c, os_c, 1000000, 10000, 100);
  if(is_stream_d_valid()) {
    experiment_merge_sort(is_d, os_d, 1000000, 10000, 100);
  }*/

  /*

  logInfoLine("Run test parameters for block size 128. ");
  set_block_size_factor(128);
  testParameters(is_d, os_d);

  /*/

//  logInfoLine("Final Testing Benchmark for Sorting Algorithms... ");
//  set_block_size_factor(128);
//  benchmark_sorting(is_d, os_d, 64 * 1024 * 1024, 31);

  // */

  set_block_size_factor(32);
  do_read_and_write_benchmark(FALSE, 1000000000);

  logInfoLine("Finished with test run. ");

}



