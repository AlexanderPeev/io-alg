#ifndef ELEMENT_C
#define ELEMENT_C

#include <stdlib.h>
#include <stdio.h>

#include "bool.h"
#include "element.h"
#include "util.h"
#include "marshaller.h"

BOOL shouldSetUpRandomElement = TRUE;
int blockSizeFactor = 1;

ELEMENT* create_blank_element() {
  return (ELEMENT*)calloc(1, element_byte_size());
}

ELEMENT random_element() {
  if(shouldSetUpRandomElement) {
    SETUP_RANDOM_ELEMENT
    shouldSetUpRandomElement = FALSE;
  }
  return RANDOM_ELEMENT;
}

ELEMENT element_for_number(int number) {
  return NUMBER_TO_ELEMENT(number);
}

char* visualize_element(ELEMENT element) {
  int i;
  int elementSize = element_byte_size();
  char* data = (char*)(&element);
  char* string = (char*)malloc(elementSize * 2 + 1);
  string[elementSize * 2] = 0;

  for (i = 0; i < elementSize; i++) {
    sprintf(string + i * 2, "%02X", data[i]);
  }

  return string;
}

void dump_element(char* message, ELEMENT element) {
  char* visualization = visualize_element(element);
  logInfoLine(fS2("%s: %s", message, visualization));
  free(visualization);
}

void free_element(ELEMENT* element) {
  if(element != NULL) {
    free(element);
  }
  else {
    logErrorLine("ELEMENT was null in free_element");
  }
}

ELEMENT* create_element_array(int total) {
  return (ELEMENT*)calloc(total, element_byte_size());
}

void free_element_array(ELEMENT* elementArray) {
  if(elementArray != NULL) {
    free(elementArray);
  }
  else {
    logErrorLine("ELEMENT ARRAY was null in free_element_array");
  }
}

char* create_buffer(int byteSize) {
  return (char*)malloc(byteSize);
}

void free_buffer(char* buffer) {
  if(buffer != NULL) {
    free(buffer);
  }
  else {
    logErrorLine("Buffer was null in free_buffer");
  }
}

int block_size() {
  return BLOCK_SIZE;
}

int block_size_factor() {
  return blockSizeFactor;
}

void set_block_size_factor(int factor) {
  blockSizeFactor = factor;
}

#endif /* ELEMENT_C */
