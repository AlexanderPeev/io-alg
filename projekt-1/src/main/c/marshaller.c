#ifndef MARSHALLER_C
#define MARSHALLER_C

#include <stdio.h>
#include <string.h>

#include "dataTypes.h"
#include "marshaller.h"
#include "util.h"

void marshal(DATA data, ELEMENT* element) {
  memcpy(data->data, element, util_min(element_byte_size(), data->byteSize));
}

void unmarshal(ELEMENT* element, DATA data) {
  memcpy(element, data->data, util_min(element_byte_size(), data->byteSize));
}

int element_byte_size() {
  return sizeof(ELEMENT);
}

#endif /* MARSHALLER_C */