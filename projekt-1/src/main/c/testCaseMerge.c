#ifndef TEST_CASE_MERGE_C
#define TEST_CASE_MERGE_C

#include "dataTypes.h"
#include "merge.h"
#include "testDeclarations.h"

void test_merge(InputStream is, OutputStream os) {
  char filenameInputFormat1[] = "./mergeInput1-%c.txt";
  char filenameInputFormat2[] = "./mergeInput2-%c.txt";
  char filenameInputFormat3[] = "./mergeInput3-%c.txt";
  char filenameInputFormat4[] = "./mergeInput4-%c.txt";
  char filenameOutputFormat[] = "./mergeOutput-%c.txt";
  ELEMENT element3 = element_for_number(3);
  ELEMENT element7 = element_for_number(7);
  ELEMENT element5 = element_for_number(5);
  ELEMENT element31 = element_for_number(31);
  ELEMENT element0 = element_for_number(0);
  ELEMENT elementNeg20 = element_for_number(-20);
  ELEMENT element23452 = element_for_number(23452);
  ELEMENT* actual;
  HANDLE handleIn1;
  HANDLE handleIn2;
  HANDLE handleIn3;
  HANDLE handleIn4;
  HANDLE handleOut;
  HANDLE handleCheck;
  HANDLES handles;

  os_create create = os.create;
  os_write write = os.write;
  os_close oclose = os.close;

  is_open open = is.open;
  is_read_next read_next = is.read_next;
  is_close iclose = is.close;

  logInfoLine(fS2("Starting MERGE test for implementation (%s, %s)", is.name, os.name));

  handleIn1 = (*create)(fC(filenameInputFormat1, os.code));
  handleIn2 = (*create)(fC(filenameInputFormat2, os.code));
  handleIn3 = (*create)(fC(filenameInputFormat3, os.code));
  handleIn4 = (*create)(fC(filenameInputFormat4, os.code));

  (*write)(handleIn1, element3);
  (*write)(handleIn2, elementNeg20);
  (*write)(handleIn3, element31);
  (*write)(handleIn4, element0);

  (*write)(handleIn1, element7);
  (*write)(handleIn2, element5);
  (*write)(handleIn3, element23452);

  (*oclose)(handleIn1);
  (*oclose)(handleIn2);
  (*oclose)(handleIn3);
  (*oclose)(handleIn4);

  handleIn1 = (*open)(fC(filenameInputFormat1, os.code));
  handleIn2 = (*open)(fC(filenameInputFormat2, os.code));
  handleIn3 = (*open)(fC(filenameInputFormat3, os.code));
  handleIn4 = (*open)(fC(filenameInputFormat4, os.code));
  handleOut = (*create)(fC(filenameOutputFormat, os.code));

  handles = create_handles(4);
  handles->handles[0] = handleIn1;
  handles->handles[1] = handleIn2;
  handles->handles[2] = handleIn3;
  handles->handles[3] = handleIn4;

  merge(is, os, handles, handleOut);

  (*iclose)(handleIn1);
  (*iclose)(handleIn2);
  (*iclose)(handleIn3);
  (*iclose)(handleIn4);
  (*oclose)(handleOut);

  free_handles(handles);

  handleCheck = (*open)(fC(filenameOutputFormat, os.code));

  actual = (*read_next)(handleCheck);
  assert_elements_equal_with_message("Merge output (-20)", elementNeg20, *actual);
  free_element(actual);

  actual = (*read_next)(handleCheck);
  assert_elements_equal_with_message("Merge output (0)", element0, *actual);
  free_element(actual);

  actual = (*read_next)(handleCheck);
  assert_elements_equal_with_message("Merge output (3)", element3, *actual);
  free_element(actual);

  actual = (*read_next)(handleCheck);
  assert_elements_equal_with_message("Merge output (5)", element5, *actual);
  free_element(actual);

  actual = (*read_next)(handleCheck);
  assert_elements_equal_with_message("Merge output (7)", element7, *actual);
  free_element(actual);

  actual = (*read_next)(handleCheck);
  assert_elements_equal_with_message("Merge output (31)", element31, *actual);
  free_element(actual);

  actual = (*read_next)(handleCheck);
  assert_elements_equal_with_message("Merge output (23452)", element23452, *actual);
  free_element(actual);

  (*iclose)(handleCheck);

  logInfoLine(fS2("Finished with MERGE test for implementation (%s, %s)", is.name, os.name));
}

#endif /* TEST_CASE_MERGE_C */
