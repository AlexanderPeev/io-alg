#ifndef INPUT_STREAM_C_H
#define INPUT_STREAM_C_H

#include "inputStream.h"

int is_c_block_size();

int is_c_block_bytes();

void* is_c_buffer_start(SYSTEM_HANDLE systemHandle);

BOOL is_c_buffer_empty(SYSTEM_HANDLE systemHandle);

void is_c_buffer_fill(SYSTEM_HANDLE systemHandle, int bytesReadTotal);

void is_c_buffer_poll(SYSTEM_HANDLE systemHandle, int bytesUsedTotal);

int is_c_buffer_remaining(SYSTEM_HANDLE systemHandle);

int is_c_bytes_to_read(int totalBytesToRead, int bytesReadTotal);

void is_c_read_block(HANDLE handle);

HANDLE is_c_open(char* filename);

ELEMENT* is_c_read_next(HANDLE handle);

BOOL is_c_end_of_stream(HANDLE handle);

void is_c_close(HANDLE handle);

#endif /* INPUT_STREAM_C_H */
