#ifndef OUTPUT_STREAM_C_H
#define OUTPUT_STREAM_C_H

#include "outputStream.h"

int os_c_block_size();

int os_c_block_bytes();

BOOL os_c_buffer_full(SYSTEM_HANDLE systemHandle);

BOOL os_c_buffer_not_empty(SYSTEM_HANDLE systemHandle);

void* os_c_buffer_write_start(SYSTEM_HANDLE systemHandle);

void os_c_buffer_added(SYSTEM_HANDLE systemHandle);

void os_c_buffer_processed(SYSTEM_HANDLE systemHandle);

BOOL os_c_non_fatal_errno();

void os_c_write_block(HANDLE handle);

HANDLE os_c_create(char* filename);

void os_c_write(HANDLE handle, ELEMENT element);

void os_c_close(HANDLE handle);

#endif /* OUTPUT_STREAM_C_H */
