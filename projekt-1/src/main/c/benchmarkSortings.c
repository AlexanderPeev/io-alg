#include <stdio.h>
#include "experiment.h"
#include "merge.h"
#include "sort.h"
#include "randomElementGenerator.h"

void benchmark_sorting(InputStream is, OutputStream os, int M, int d){
  int N[] = {1000000, 100000000, 250000000, 500000000, 1000000000};
  int countN = 5;
  int i;
  int runs = 3;
  char* fileNameFormat = "./final-my_fun_merge-%d.txt";
  char mergeTestFileName[50];
  double runningTime = -1.0;

  logInfoLine("Creating files");
  for(i = 0; i < countN; i++) {
    logInfoLine(fI("Creating %d elements", N[i]));
    sprintf(mergeTestFileName, fileNameFormat, N[i]);
    write_random_elements_to_file(mergeTestFileName, os, N[i]);
    logInfoLine(fI("Wrote %d elements", N[i]));
  }

  for(i = 4; i < countN; i++) {
    logInfoLine(fI("Testing %d elements", N[i]));
/*    sprintf(mergeTestFileName, fileNameFormat, N[i]);

    write_random_elements_to_file(mergeTestFileName, os, N[i]);
    logInfoLine("Wrote random elements to file... ");
*/
    runningTime = benchmark_merge_sorts(is, os, mergeTestFileName, runs, N[i], M, d);
    logInfoLine(fD("Running time (avg  3) for my_fun_merge sort: %lf", runningTime));
  }

  for(i = 0; i < countN; i++) {
    logInfoLine(fI("Testing %d elements", N[i]));
    sprintf(mergeTestFileName, fileNameFormat, N[i]);

    runningTime = benchmark_sorting_algorithms(mergeTestFileName, is, runs, N[i], (&quicksort));
    logInfoLine(fD("Running time (avg  3) for quick sort: %lf", runningTime));

    runningTime = benchmark_sorting_algorithms(mergeTestFileName, is, runs, N[i], (&my_fun_heapsort));
    logInfoLine(fD("Running time (avg  3) for heap sort:  %lf", runningTime));

    remove(mergeTestFileName);
  }

  logInfoLine("Finished with benchmark. ");

}

double benchmark_merge_sorts(InputStream is, OutputStream os, char* fileName, int runs, int N, int M, int d){
  double result = 0;
  int i;

  for(i = 0; i < runs; i++){
    result += benchmark_merge_sort(is, os, fileName, N, M, d);
  }

  return result / runs;
}

double benchmark_merge_sort(InputStream is, OutputStream os, char* fileName, int N, int M, int d){
  clock_t begin;
  clock_t end;
  double time_spent;

  HANDLE input;
  HANDLE output;

  input = (*is.open)(fileName);
  output = (*os.create)(fC("./benchmark-output-%c.txt", os.code));

#ifdef _WIN32
  //for stream B, we have to set the maximum files that can be opened by fopen (normally only 512, can be increased up to 2048)
    _setmaxstdio(1100);
#endif

  begin = clock();

  my_fun_mergesort(input, output, N, M, d, is, os);

  end = clock();
  time_spent = (double)(end - begin) / CLOCKS_PER_SEC;

  (*is.close)(input);
  (*os.close)(output);

  return time_spent;
}

double benchmark_sorting_algorithms(char* fileName, InputStream is, int runs, int N, void (*sort)(ELEMENT*, int)) {
  double result = 0;
  int i;

  for(i = 0; i < runs; i++){
    result += benchmark_sorting_algorithm(fileName, is, N, sort);
  }

  return result / runs;
}

double benchmark_sorting_algorithm(char* fileName, InputStream is, int N, void (*sort)(ELEMENT*, int)) {
  int i;
  ELEMENT* elt;
  clock_t begin;
  clock_t end;
  ELEMENT* elements;

  HANDLE input;

  input = (*is.open)(fileName);
  elements = calloc(N, element_byte_size());

  for(i = 0; i < N; i++){
    elt = (*is.read_next)(input);
    elements[i] = *elt;
    free_element(elt);
  }

  begin = clock();
  (*sort)(elements, N);
  end = clock();

  (*is.close)(input);
  free(elements);

  return (double)(end-begin)/CLOCKS_PER_SEC;
}
