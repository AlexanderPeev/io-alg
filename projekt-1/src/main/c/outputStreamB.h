#ifndef OUTPUT_STREAM_B_H
#define OUTPUT_STREAM_B_H

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include "outputStream.h"

int os_b_block_size();

int os_b_block_bytes();

BOOL os_b_buffer_full(FILE_HANDLE fileHandle);

BOOL os_b_buffer_not_empty(FILE_HANDLE fileHandle);

void* os_b_buffer_write_start(FILE_HANDLE fileHandle);

void os_b_buffer_added(FILE_HANDLE fileHandle);

void os_b_buffer_processed(FILE_HANDLE fileHandle);

void os_b_write_block(HANDLE handle);

void os_b_write_to_buffer(FILE_HANDLE fileHandle, DATA data, ELEMENT element);

HANDLE os_b_create(char* filename);

void os_b_write(HANDLE handle, ELEMENT element);

void os_b_close(HANDLE handle);

#endif /* OUTPUT_STREAM_B_H */
