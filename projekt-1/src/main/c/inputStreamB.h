#ifndef INPUT_STREAM_B_H
#define INPUT_STREAM_B_H

#include "inputStream.h"

int is_b_block_size();

int is_b_block_bytes();

void* is_b_buffer_start(FILE_HANDLE fileHandle);

BOOL is_b_buffer_empty(FILE_HANDLE fileHandle);

void is_b_buffer_fill(FILE_HANDLE fileHandle, int bytesReadTotal);

void is_b_buffer_poll(FILE_HANDLE fileHandle, int bytesUsedTotal);

int is_b_buffer_remaining(FILE_HANDLE fileHandle);

void is_b_read_block(FILE_HANDLE fileHandle);

HANDLE is_b_open(char* filename);

ELEMENT* is_b_read_next(HANDLE handle);

BOOL is_b_end_of_stream(HANDLE handle);

void is_b_close(HANDLE handle);

#endif /* INPUT_STREAM_B_H */
