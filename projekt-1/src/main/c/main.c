#include <stdio.h>

#include "streams.h"

int main(int argc, char const *argv[]) {
  run_tests();

  return 0;
}
