#include <stdio.h>
#include "inputStream.h"
#include "outputStream.h"
#include "randomElementGenerator.h"
#include "merge.h"

//
// Created by Karen on 6/10/2016.
//
void testParameters(InputStream is, OutputStream os){
    int i,j,k;
    clock_t begin;
    clock_t end;
    HANDLE input;
    HANDLE output;
    double time_spent;

    //how many iterations do we want?
    //double iterations = 1.0;
    int it;
    int megaByte = 1024 * 1024;

    //input size files
    /*int sizeN = 4;
    int N[] = {100000000,250000000,500000000,1000000000};*/
    int sizeN = 1;
    int N[] = {100000000};

    //my_fun_merge that many streams at the same time
    int sized = 5;
    int d[] = {15, 31, 63, 127, 255};
    int maxIdxD = 2;

    //put this many elements in one stream
    int sizeM = 3;
    int M[] = {8 * megaByte, 16 * megaByte, 32 * megaByte}; // measured in elements - multiply by 4 for bytes

    for(i = 0; i < sizeN; i++){
        input = (*os.create)(fI("./testParameters-input-%d.txt", N[i]));
        write_random_elements_to_handle(input, os, N[i]);
        (*os.close)(input);


        printf("Result input size %d\n", N[i]);
        for(k = 0; k < sized; k++){
            printf("& %d ", d[k]);
        }
        printf("\\\\");
        logNewLine();

        for(j = 0; j < sizeM; j++){
            printf("%d ",M[j]);
            for(k = 0; k <= maxIdxD; k++){
                time_spent = 0;

                //for(it = 0; it < iterations; it++){
                    input = (*is.open)(fI("./testParameters-input-%d.txt", N[i]));
                    output = (*os.create)(fI("./testParameters-output-%d.txt", N[i]));

                    begin = clock();
              my_fun_mergesort(input, output, N[i], M[j], d[k], is, os);
                    end = clock();

                    time_spent += (double)(end - begin) / CLOCKS_PER_SEC;

                    (*is.close)(input);
                    (*os.close)(output);

                    remove(fI("./testParameters-output-%d.txt", N[i]));
                //}


                printf("& %lf ", time_spent); // /iterations

            }

            for(; k <= sized; k++){
                printf("& -- ");
            }

            printf("\\\\");
            logNewLine();

            ++maxIdxD;
        }

        logNewLine();

        // Remove this random bit-scrub from our HDD...
        remove(fI("./testParameters-input-%d.txt", N[i]));
    }
}
