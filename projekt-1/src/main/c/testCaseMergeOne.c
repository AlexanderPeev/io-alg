#ifndef TEST_CASE_MERGE_C
#define TEST_CASE_MERGE_C

#include "dataTypes.h"
#include "merge.h"
#include "testDeclarations.h"

void test_merge_one(InputStream is, OutputStream os) {
  char filenameInputFormat[] = "./mergeOneInput-%c.txt";
  char filenameOutputFormat[] = "./mergeOneOutput-%c.txt";
  ELEMENT element = SOME_ELEMENT;
  ELEMENT* actual;
  HANDLE handleIn;
  HANDLE handleOut;
  HANDLE handleCheck;
  HANDLES handles;

  os_create create = os.create;
  os_write write = os.write;
  os_close oclose = os.close;

  is_open open = is.open;
  is_read_next read_next = is.read_next;
  is_close iclose = is.close;

  logInfoLine(fS2("Starting MERGE-ONE test for implementation (%s, %s)", is.name, os.name));

  handleIn = (*create)(fC(filenameInputFormat, os.code));
  (*write)(handleIn, element);
  (*oclose)(handleIn);

  handleIn = (*open)(fC(filenameInputFormat, os.code));
  handleOut = (*create)(fC(filenameOutputFormat, os.code));

  handles = create_handles(1);
  handles->handles[0] = handleIn;

  merge(is, os, handles, handleOut);

  (*iclose)(handleIn);
  (*oclose)(handleOut);

  free_handles(handles);

  handleCheck = (*open)(fC(filenameOutputFormat, os.code));
  actual = (*read_next)(handleCheck);
  (*iclose)(handleCheck);

  assert_elements_equal_with_message("Merge-one output", element, *actual);

  free_element(actual);

  logInfoLine(fS2("Finished with MERGE-ONE test for implementation (%s, %s)", is.name, os.name));
}

#endif /* TEST_CASE_MERGE_C */
