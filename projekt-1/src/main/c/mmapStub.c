#ifndef MMAP_STUB_C
#define MMAP_STUB_C

#ifndef _WIN32
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>


int is_mmap_valid() {
  return 1;
}

int pagesize() {
  return getpagesize();
}

long long seek_to(int handle, long long offset) {
  return lseek(handle, offset, SEEK_SET);
}

void truncate_to(int handle, int size) {
  ftruncate(handle, size);
}

#endif

#ifdef _WIN32
#include <fcntl.h>
#include <stdlib.h>
#define off_t long long

void *mmap(void *addr,
           size_t length,
           int prot,
           int flags,
           int fd,
           off_t offset) {
  return calloc(length, 1);
}

int munmap(void *addr, size_t length) {
  free(addr);
  return 0;
}

int is_mmap_valid() {
  return 0;
}

int pagesize() {
  return 0; // IDGAF
}

long long seek_to(int handle, long long offset) {
  return offset; // IDGAF
}

void truncate_to(int handle, int size) {
  // IDGAF
}

#endif /* WIN32 */


#endif /* MMAP_STUB_C */
