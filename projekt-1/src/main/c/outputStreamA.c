#ifndef OUTPUT_STREAM_A_C
#define OUTPUT_STREAM_A_C

#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>

#include "outputStream.h"
#include "handle.h"
#include "outputStreamA.h"

#ifndef O_BINARY
#define O_BINARY 0
#endif

#ifndef EWOULDBLOCK
#define EWOULDBLOCK 0
#endif

BOOL os_a_non_fatal_errno() {
  if (errno == EINTR || errno == EAGAIN || errno == EWOULDBLOCK) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

HANDLE os_a_create(char* filename) {
  HANDLE handle;
  SYSTEM_HANDLE systemHandle;

  handle = create_blank_handle();
  systemHandle = create_system_handle();

  handle->handle = systemHandle;
  handle->data = create_element_data();
  systemHandle->eof = FALSE;
  systemHandle->handle = open(filename, O_WRONLY | O_BINARY | O_CREAT, S_IWRITE | S_IREAD);

  if(systemHandle->handle == -1) {
    raise_handle_error_with_number(ERROR_CREATE_FAILED, "ERROR during open in os_a_create");
    systemHandle->eof = TRUE;
  }

  return handle;
}

void os_a_write(HANDLE handle, ELEMENT element) {
  DATA data = handle->data;
  int bytesWrittenTotal = 0;
  int bytesWrittenLast = 0;
  SYSTEM_HANDLE systemHandle = handle->handle;

  marshal(data, &element);

  do {
    bytesWrittenLast = write(systemHandle->handle,
                             ((char*)data->data) + bytesWrittenTotal,
                             data->byteSize - bytesWrittenTotal);

    if(bytesWrittenLast > 0) {
      bytesWrittenTotal += bytesWrittenLast;
    }
  }
  while((bytesWrittenLast > 0
              || (bytesWrittenLast == -1 && os_a_non_fatal_errno()))
          && bytesWrittenTotal < data->byteSize);

  if(bytesWrittenLast == -1) {
    raise_handle_error_with_number(ERROR_WRITE_FAILED, "ERROR during write in os_a_write");
    systemHandle->eof = TRUE;
  }
}

void os_a_close(HANDLE handle) {
  int result = close(handle->handle->handle);

  if(result == -1) {
    raise_handle_error_with_number(ERROR_OUTPUT_CLOSE_FAILED, "ERROR during close in os_a_close");
  }

  free_system_handle(handle->handle);
  free_element_data(handle->data);
  free_handle(handle);
}

#endif /* OUTPUT_STREAM_A_C */
