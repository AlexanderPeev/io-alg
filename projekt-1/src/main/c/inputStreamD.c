#ifndef INPUT_STREAM_D_C
#define INPUT_STREAM_D_C

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>

#ifndef O_BINARY
#define O_BINARY 0
#endif

#ifndef S_IREAD
#define S_IREAD 0
#endif

#include "mmapStub.h"
#include "inputStream.h"
#include "handle.h"


//returns the numbber of bytes in a block
long is_d_block_bytes() {
  return pagesize() * BLOCK_SIZE_FACTOR;
}

long is_d_block_size() {
  return is_d_block_bytes() / element_byte_size();
}

//returns the next element in the buffer
void *is_d_buffer_start(MAP_HANDLE mapHandle) {
  return (void *) (mapHandle->map + mapHandle->offset);
}

void is_d_buffer_poll(MAP_HANDLE mapHandle) {
  mapHandle->offset = util_min(mapHandle->offset + element_byte_size(), mapHandle->loaded);
}

//resets the buffer offset
void is_d_buffer_fill(MAP_HANDLE mapHandle, long bytesReadTotal) {
  mapHandle->offset = 0;
  mapHandle->loaded = bytesReadTotal;
  mapHandle->blocksRead ++;
  if (bytesReadTotal < is_d_block_bytes()) {
    mapHandle->eof = TRUE;
  }
}

long is_d_bytes_to_map(MAP_HANDLE mapHandle){
  return util_long_min(
      is_d_block_bytes(),
      (mapHandle->fileSize - mapHandle->blocksRead * is_d_block_bytes())
  );
}

BOOL is_d_is_initial_write(MAP_HANDLE mapHandle){
  return mapHandle->offset == -1;
}

//returns if the buffer/map is empty
BOOL is_d_map_empty(MAP_HANDLE mapHandle) {
  return mapHandle->offset == mapHandle->loaded;
}

void is_d_unmap(MAP_HANDLE mapHandle){
  if (munmap(mapHandle->map, mapHandle->loaded) == -1) {
    raise_handle_error_with_number(ERROR_UNMAP_FAILED, "ERROR during munmap in is_d_unmap");
  }
}

void is_d_read_block(MAP_HANDLE mapHandle){
  long totalBytesToMap = is_d_bytes_to_map(mapHandle);

  seek_to(mapHandle->handle, mapHandle->blocksRead * is_d_block_bytes());

  mapHandle->map = mmap(NULL, totalBytesToMap, PROT_READ, MAP_SHARED, mapHandle->handle, is_d_block_bytes() * mapHandle->blocksRead);
  if (mapHandle->map == MAP_FAILED)
  {
    raise_handle_error_with_number(ERROR_MAP_FAILED, "ERROR during mmap in is_d_read_block");
    logErrorLine(fL("Failed to map region of size: %ld", totalBytesToMap));
  }

  is_d_buffer_fill(mapHandle, totalBytesToMap);
}

/**
 * Interface implementations
 * */

HANDLE is_d_open(char *filename) {
  HANDLE handle;
  MAP_HANDLE mapHandle;

  handle = create_blank_handle();
  mapHandle = create_map_handle();

  handle->mapHandle = mapHandle;
  handle->data = create_blank_data();
  mapHandle->eof = FALSE;
  mapHandle->offset = -1;
  mapHandle->loaded = 0;
  mapHandle->blocksRead = 0;
  mapHandle->fileSize = file_size(filename);
  mapHandle->handle = open(filename, O_RDONLY | O_BINARY, S_IREAD);

  seek_to(mapHandle->handle, 0);

  if (mapHandle->handle == -1) {
    raise_handle_error_with_number(ERROR_OPEN_FAILED, "ERROR during open in is_d_open");
    mapHandle->eof = TRUE;
  }

  return handle;
}

ELEMENT *is_d_read_next(HANDLE handle) {
  ELEMENT *element;
  int elementSize = element_byte_size();
  DATA data = handle->data;
  MAP_HANDLE mapHandle = handle->mapHandle;
  //read a new block if necessary
  if(is_d_is_initial_write(mapHandle)){
    is_d_read_block(mapHandle);
  }
  else if (is_d_map_empty(mapHandle)) {
    is_d_unmap(mapHandle);
    is_d_read_block(mapHandle);
  }

  data->byteSize = elementSize;
  data->data = is_d_buffer_start(mapHandle);

  is_d_buffer_poll(mapHandle);
  element = create_blank_element();

  if (data->byteSize > 0) {
    unmarshal(element, data);
  }

  return element;
}

BOOL is_d_end_of_stream(HANDLE handle) {
  MAP_HANDLE mapHandle = handle->mapHandle;
  return is_d_map_empty(mapHandle) && (mapHandle->eof || mapHandle->fileSize <= mapHandle->blocksRead*is_d_block_bytes());
}

void is_d_close(HANDLE handle) {
  MAP_HANDLE mapHandle = handle->mapHandle;
  int result = close(mapHandle->handle);

  if(!is_d_is_initial_write(mapHandle)){
    is_d_unmap(mapHandle);
  }

  if (result == -1) {
    raise_handle_error_with_number(ERROR_INPUT_CLOSE_FAILED, "ERROR during close in is_d_close");
  }

  free_map_handle(mapHandle);
  free_data(handle->data);
  free_handle(handle);
}

#endif /* INPUT_STREAM_D_C */
