#ifndef STREAMS_H
#define STREAMS_H

#include "dataTypes.h"

#include "inputStream.h"
#include "inputStreamA.h"
#include "inputStreamB.h"
#include "inputStreamC.h"
#include "inputStreamD.h"

#include "outputStream.h"
#include "outputStreamA.h"
#include "outputStreamB.h"
#include "outputStreamC.h"
#include "outputStreamD.h"

InputStream is_a;
InputStream is_b;
InputStream is_c;
InputStream is_d;

OutputStream os_a;
OutputStream os_b;
OutputStream os_c;
OutputStream os_d;

void init_streams();
void run_tests();

#endif /* STREAMS_H */
