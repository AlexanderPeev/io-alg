#ifndef HANDLE_C
#define HANDLE_C

#include <errno.h>

#include "dataTypes.h"

HANDLE_ERROR lastError = NULL;

void raise_handle_error(int errorNumber, int code, char* text) {
  HANDLE_ERROR handleError = create_blank_handle_error();
  handleError->errorNumber = errorNumber;
  handleError->code = code;
  handleError->text = text;

  lastError = handleError;

  logErrorLine(text);
  logErrorLine(fI("\tError number: %d", errorNumber));
}

void raise_handle_error_with_number(int code, char* text) {
  raise_handle_error(errno, code, text);
}

BOOL has_handle_error() {
  return lastError != NULL;
}

HANDLE_ERROR get_handle_error() {
  return lastError;
}

void reset_handle_error() {
  if(has_handle_error()) {
    free_handle_error(lastError);
    lastError = NULL;
  }
}

HANDLE_ERROR create_blank_handle_error() {
  return (HANDLE_ERROR)calloc(1, sizeof(HandleError));
}

void free_handle_error(HANDLE_ERROR handleError) {
  if(handleError != NULL) {
    free(handleError);
  }
  else {
    logErrorLine("HANDLE_ERROR was null in free_handle_error");
  }
}

HANDLES create_blank_handles() {
  return (HANDLES)calloc(1, sizeof(Handles));
}

HANDLES create_handles(int total) {
  HANDLES handles = create_blank_handles();

  handles->total = total;
  handles->handles = create_handle_array(total);

  return handles;
}

void free_blank_handles(HANDLES handles) {
  if(handles != NULL) {
    free(handles);
  }
  else {
    logErrorLine("HANDLES were null in free_blank_handles");
  }
}

void free_handles(HANDLES handles) {
  if(handles != NULL) {
    if(handles->handles != NULL) {
      free_handle_array(handles->handles);
    }

    free_blank_handles(handles);
  }
  else {
    logErrorLine("HANDLES were null in free_handles");
  }
}

HANDLE* create_handle_array(int total) {
  return (HANDLE*)malloc(sizeof(HANDLE) * total);
}

HANDLE* extend_handle_array(HANDLE* handleArray, int newTotal) {
  return realloc(handleArray, sizeof(HANDLE) * newTotal);
}

void free_handle_array(HANDLE* handleArray) {
  if(handleArray != NULL) {
    free(handleArray);
  }
  else {
    logErrorLine("HANDLE ARRAY was null in free_handle_array");
  }
}

HANDLE create_blank_handle() {
  return (HANDLE)malloc(sizeof(Handle));
}

void free_handle(HANDLE handle) {
  if(handle != NULL) {
    free(handle);
  }
  else {
    logErrorLine("HANDLE was null in free_handle");
  }
}

SYSTEM_HANDLE create_system_handle() {
  return (SYSTEM_HANDLE)malloc(sizeof(SystemHandle));
}

void free_system_handle(SYSTEM_HANDLE handle) {
  if(handle != NULL) {
    free(handle);
  }
  else {
    logErrorLine("SYSTEM_HANDLE was null in free_system_handle");
  }
}

FILE_HANDLE create_file_handle() {
  return (FILE_HANDLE)malloc(sizeof(FileHandle));
}

void free_file_handle(FILE_HANDLE handle) {
  if(handle != NULL) {
    free(handle);
  }
  else {
    logErrorLine("FILE_HANDLE was null in free_system_handle");
  }
}

MAP_HANDLE create_map_handle() {
  return (MAP_HANDLE)malloc(sizeof(MapHandle));
}

void free_map_handle(MAP_HANDLE handle) {
  if(handle != NULL) {
    free(handle);
  }
  else {
    logErrorLine("MAP_HANDLE was null in free_system_handle");
  }
}

#endif /* HANDLE_C */
