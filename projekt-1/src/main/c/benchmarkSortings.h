#include "inputStream.h"
#include "outputStream.h"

#ifndef IO_BENCHMARK_SORTING_H
#define IO_BENCHMARK_SORTING_H

void benchmark_sorting(InputStream is, OutputStream os, int M, int d);

double benchmark_merge_sorts(InputStream is, OutputStream os, char* fileName, int runs, int N, int M, int d);
double benchmark_merge_sort(InputStream is, OutputStream os, char* fileName, int N, int M, int d);
double benchmark_sorting_algorithms(char* fileName, InputStream is, int runs, int N, void (*sort)(ELEMENT*, int));
double benchmark_sorting_algorithm(char* fileName, InputStream is, int N, void (*sort)(ELEMENT*, int));

#endif //IO_BENCHMARK_SORTING_H
