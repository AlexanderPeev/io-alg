//
// Created by Karen on 1/10/2016.
//
#include "testDeclarations.h"
#include "merge.h"
#include "outputStream.h"
#include "inputStream.h"
#include "experiment.h"

void test_mergesort(InputStream is, OutputStream os) {
    char filenameInputFormat[] = "./my_fun_mergesort-input-%c.txt";
    char filenameOutputFormat[] = "./my_fun_mergesort-output-%c.txt";

    int i, n = 20;
    ELEMENT sorted[20] = {
      element_for_number(1),
      element_for_number(3),
      element_for_number(6),
      element_for_number(9),
      element_for_number(12),
      element_for_number(15),
      element_for_number(25),
      element_for_number(29),
      element_for_number(31),
      element_for_number(39),
      element_for_number(43),
      element_for_number(43),
      element_for_number(47),
      element_for_number(59),
      element_for_number(62),
      element_for_number(84),
      element_for_number(90),
      element_for_number(344),
      element_for_number(674),
      element_for_number(1234)
    };
    ELEMENT* read;
    HANDLE output;
    HANDLE input;
    HANDLE check;

    logInfoLine(fS2("Starting MERGESORT test for implementation (%s, %s)", is.name, os.name));
    input = (*os.create)(fC(filenameInputFormat, os.code));

    (*os.write)(input, element_for_number(25));
    (*os.write)(input, element_for_number(43));
    (*os.write)(input, element_for_number(1));
    (*os.write)(input, element_for_number(39));
    (*os.write)(input, element_for_number(12));
    (*os.write)(input, element_for_number(6));
    (*os.write)(input, element_for_number(3));
    (*os.write)(input, element_for_number(9));
    (*os.write)(input, element_for_number(90));
    (*os.write)(input, element_for_number(674));
    (*os.write)(input, element_for_number(47));
    (*os.write)(input, element_for_number(15));
    (*os.write)(input, element_for_number(62));
    (*os.write)(input, element_for_number(43));
    (*os.write)(input, element_for_number(29));
    (*os.write)(input, element_for_number(59));
    (*os.write)(input, element_for_number(344));
    (*os.write)(input, element_for_number(84));
    (*os.write)(input, element_for_number(31));
    (*os.write)(input, element_for_number(1234));

    (*os.close)(input);

    input = (*is.open)(fC(filenameInputFormat, os.code));
    output = (*os.create)(fC(filenameOutputFormat, os.code));

  my_fun_mergesort(input, output, n, 5, 3, is, os);

    (*is.close)(input);
    (*os.close)(output);

    check = (*is.open)(fC(filenameOutputFormat, os.code));


    for(i = 0; i < n; i++) {
      read = (*is.read_next)(check);

      assert_elements_equal_with_message("Merge sort should sort in ascending order: ", sorted[i], *read);

      free_element(read);
    }

    (*is.close)(check);

    logInfoLine(fS2("Finished with MERGESORT test for implementation (%s, %s)", is.name, os.name));
}
