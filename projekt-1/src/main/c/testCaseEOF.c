#ifndef TEST_CASE_EOF_C
#define TEST_CASE_EOF_C

#include "streams.h"
#include "testDeclarations.h"

void test_eof(InputStream is, OutputStream os) {
  char filenameFormat[] = "./eof-%c.txt";
  ELEMENT element = SOME_ELEMENT;
  ELEMENT* read;
  BOOL actual;
  HANDLE handle;

  os_create create = os.create;
  os_write write = os.write;
  os_close oclose = os.close;

  is_open open = is.open;
  is_read_next read_next = is.read_next;
  is_end_of_stream end_of_stream = is.end_of_stream;
  is_close iclose = is.close;

  logInfoLine(fS2("Starting EOF test for implementation (%s, %s)", is.name, os.name));

  handle = (*create)(fC(filenameFormat, os.code));
  (*write)(handle, element);
  (*oclose)(handle);

  handle = (*open)(fC(filenameFormat, os.code));

  actual = (*end_of_stream)(handle);
  assert_bool_equal_with_message("EOF should NOT be reached: ", FALSE, actual);

  read = (*read_next)(handle);

  actual = (*end_of_stream)(handle);
  assert_bool_equal_with_message("EOF should be reached: ", TRUE, actual);

  (*iclose)(handle);

  free_element(read);

  logInfoLine(fS2("Finished with EOF test for implementation (%s, %s)", is.name, os.name));
}

#endif /* TEST_CASE_EOF_C */
