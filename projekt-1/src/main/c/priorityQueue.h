//
// Created by Karen on 27/09/2016.
//

#ifndef IO_ALG_PRIORITYQUEUE_H
#define IO_ALG_PRIORITYQUEUE_H

#include "element.h"

#define pq_ptr pq*

typedef struct {
    ELEMENT data; // this can be any data type
    int inputStream;
} pq_element;

typedef struct{
    pq_element *pq_array; //array of pq_element-s
    int n;     // number of elements in PQ
    int alloc; // number of elements memory was allocated for
} pq;


void pq_display(pq_ptr pq);

pq_ptr pq_new(int size);

void pq_push(pq_ptr q, pq_element* data);

void pq_adjusttree(pq_ptr q);

void pq_pop(pq_ptr q, pq_element* element);

#endif //IO_ALG_PRIORITYQUEUE_H
