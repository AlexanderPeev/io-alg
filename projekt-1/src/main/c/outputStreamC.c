#ifndef OUTPUT_STREAM_C_C
#define OUTPUT_STREAM_C_C

#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>

#include "outputStream.h"

#ifndef EWOULDBLOCK
#define EWOULDBLOCK 0
#endif

#define OS_C_BLOCK_SIZE block_size()

#ifndef O_BINARY
#define O_BINARY 0
#endif

int os_c_block_size() {
  return OS_C_BLOCK_SIZE;
}

int os_c_block_bytes() {
  return os_c_block_size() * element_byte_size();
}

BOOL os_c_buffer_full(SYSTEM_HANDLE systemHandle) {
  return systemHandle->offset == os_c_block_bytes();
}

BOOL os_c_buffer_not_empty(SYSTEM_HANDLE systemHandle) {
  return systemHandle->offset > 0;
}

void* os_c_buffer_write_start(SYSTEM_HANDLE systemHandle) {
  return (void*)(systemHandle->buffer + systemHandle->offset);
}

void os_c_buffer_added(SYSTEM_HANDLE systemHandle) {
  systemHandle->offset += element_byte_size();
}

void os_c_buffer_processed(SYSTEM_HANDLE systemHandle) {
  systemHandle->offset = 0;
}

void os_c_write_to_buffer(SYSTEM_HANDLE systemHandle, DATA data, ELEMENT element) {
  data->byteSize = element_byte_size();
  data->data = os_c_buffer_write_start(systemHandle);

  marshal(data, &element);

  os_c_buffer_added(systemHandle);
}

BOOL os_c_non_fatal_errno() {
  if (errno == EINTR || errno == EAGAIN || errno == EWOULDBLOCK) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

void os_c_write_block(HANDLE handle) {
  int bytesWrittenTotal = 0;
  int bytesWrittenLast = 0;
  int elementSize = element_byte_size();
  SYSTEM_HANDLE systemHandle = handle->handle;
  int totalBytesToWrite = systemHandle->offset;

  do {
    bytesWrittenLast = write(systemHandle->handle,
                             systemHandle->buffer + bytesWrittenTotal,
                             totalBytesToWrite - bytesWrittenTotal);

    if(bytesWrittenLast > 0) {
      bytesWrittenTotal += bytesWrittenLast;
    }
  }
  while((bytesWrittenLast > 0
              || (bytesWrittenLast == -1 && os_c_non_fatal_errno()))
          && bytesWrittenTotal < totalBytesToWrite);

  if(bytesWrittenLast == -1) {
    raise_handle_error_with_number(ERROR_WRITE_FAILED, "ERROR during write in os_c_write_block");
    systemHandle->eof = TRUE;
  }

  os_c_buffer_processed(systemHandle);
}

HANDLE os_c_create(char* filename) {
  HANDLE handle;
  SYSTEM_HANDLE systemHandle;

  handle = create_blank_handle();
  systemHandle = create_system_handle();

  handle->handle = systemHandle;
  handle->data = create_blank_data();
  systemHandle->eof = FALSE;
  systemHandle->offset = 0;
  systemHandle->loaded = 0;
  systemHandle->buffer = create_buffer(os_c_block_bytes());
  systemHandle->handle = open(filename, O_WRONLY | O_BINARY | O_CREAT, S_IWRITE | S_IREAD);

  if(systemHandle->handle == -1) {
    raise_handle_error_with_number(ERROR_CREATE_FAILED, "ERROR during open in os_c_create");
    systemHandle->eof = TRUE;
  }

  return handle;
}

void os_c_write(HANDLE handle, ELEMENT element) {
  SYSTEM_HANDLE systemHandle = handle->handle;
  os_c_write_to_buffer(systemHandle, handle->data, element);

  if(os_c_buffer_full(systemHandle)) {
    os_c_write_block(handle);
  }
}

void os_c_close(HANDLE handle) {
  int result;
  SYSTEM_HANDLE systemHandle = handle->handle;

  if(os_c_buffer_not_empty(systemHandle)) {
    os_c_write_block(handle);
  }

  result = close(systemHandle->handle);

  if(result == -1) {
    raise_handle_error_with_number(ERROR_OUTPUT_CLOSE_FAILED, "ERROR during close in os_c_close");
  }

  free_buffer(systemHandle->buffer);
  free_system_handle(systemHandle);
  free_data(handle->data);
  free_handle(handle);
}

#endif /* OUTPUT_STREAM_C_C */
