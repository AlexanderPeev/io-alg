//
// Created by Karen on 1/10/2016.
//

#include <stdio.h>
#include "experiment.h"
#include "merge.h"
#include "sort.h"
#include "testDeclarations.h"
#include "handle.h"
#include "outputStream.h"


void cleanup_output_handles_up_to_index(OutputStream os, HANDLES handles, int lastIndex) {
  int i;
  logErrorLine(fI("Error encountered when creating output handle index: %d", lastIndex));

  for(i = 0; i <= lastIndex; i++) {
    (*os.close)(handles->handles[i]);
  }

  free_handles(handles);

  reset_handle_error();
}

void cleanup_output_handles_for_write(OutputStream os, HANDLES handles, unsigned long lastWriteIndex) {
  int i;
  logErrorLine(fL("Error encountered when writing to output handle at write index: %l. ", lastWriteIndex));

  for(i = 0; i <= handles->total; i++) {
    (*os.close)(handles->handles[i]);
  }

  free_handles(handles);

  reset_handle_error();
}

void cleanup_output_handles_for_closed(HANDLES handles) {
  logErrorLine("Error encountered when closing output handles. ");

  free_handles(handles);

  reset_handle_error();
}

void cleanup_input_handles_up_to_index(InputStream is, HANDLES handles, int lastIndex) {
  int i;
  logErrorLine(fI("Error encountered when creating input handle index: %d", lastIndex));

  for(i = 0; i <= lastIndex; i++) {
    (*is.close)(handles->handles[i]);
  }

  free_handles(handles);

  reset_handle_error();
}

void cleanup_input_handles_for_read(InputStream is, HANDLES handles, int lastReadIndex) {
  int i;
  logErrorLine(fI("Error encountered when reading from input handle at read index: %d. ", lastReadIndex));

  for(i = 0; i <= handles->total; i++) {
    (*is.close)(handles->handles[i]);
  }

  free_handles(handles);

  reset_handle_error();
}

void cleanup_input_handles_for_closed(HANDLES handles) {
  logErrorLine("Error encountered when closing input handles. ");

  free_handles(handles);

  reset_handle_error();
}

double time_spent(clock_t begin, clock_t end) {
  return (double)(end - begin) / CLOCKS_PER_SEC;
}

void write_and_read_timed(InputStream is, OutputStream os, unsigned long writes, int streams, double* writeTime, double* readTime, double* pureWriteTime, double* pureReadTime) {
    clock_t begin;
    clock_t end;
    clock_t pureBegin;
    clock_t pureEnd;
    int i;
    unsigned long j;
    ELEMENT* element;
    char* filenameFormat = "./stream-timed-output-%05d-%c.txt";
    HANDLES handles = create_handles(streams);
    char* filename = create_null_terminated_string(100);

    //logInfoLine(fC("Write and read timed for implementation %c:", os.code));
    //logInfoLine(fI("\tWrites: %d", writes));
    //logInfoLine(fI("\tStreams: %d", streams));

    *writeTime = -1;
    *readTime = -1;
    *pureWriteTime = -1;
    *pureReadTime = -1;

    for(i = 0; i < streams; i++) {
        sprintf(filename, filenameFormat, i, os.code);
        remove(filename);
    }

    begin = clock();
    for(i = 0; i < streams; i++) {
        sprintf(filename, filenameFormat, i, os.code);
        handles->handles[i] = (*os.create)(filename);

        if (has_handle_error()) {
          cleanup_output_handles_up_to_index(os, handles, i);
          return;
        }
    }

    pureBegin = clock();
    for(i = 0; i < streams; i++) {
        for(j = 0; j < writes; j++) {
          (*os.write)(handles->handles[i], random_element());

          if (has_handle_error()) {
            cleanup_output_handles_for_write(os, handles, j);
            return;
          }
        }
    }

    for(i = 0; i < streams; i++) {
        (*os.close)(handles->handles[i]);
    }
    pureEnd = clock();

    if (has_handle_error()) {
      cleanup_output_handles_for_closed(handles);
      return;
    }
    end = clock();

    *writeTime = time_spent(begin, end);
    *pureWriteTime = time_spent(pureBegin, pureEnd);

    begin = clock();
    for(i = 0; i < streams; i++) {
        sprintf(filename, filenameFormat, i, os.code);
        handles->handles[i] = (*is.open)(filename);

        if (has_handle_error()) {
          cleanup_input_handles_up_to_index(is, handles, i);
          return;
        }
    }

    pureBegin = clock();
    for(i = 0; i < streams; i++) {
        for(j = 0; j < writes; j++) {
            element = (*is.read_next)(handles->handles[i]);
            free_element(element);

            if (has_handle_error()) {
              cleanup_input_handles_for_read(is, handles, j);
              return;
            }
        }
    }
    pureEnd = clock();

    for(i = 0; i < streams; i++) {
        (*is.close)(handles->handles[i]);
    }

    if (has_handle_error()) {
      cleanup_input_handles_for_closed(handles);
      return;
    }
    else {
      free_handles(handles);
    }
    end = clock();

    *readTime = time_spent(begin, end);
    *pureReadTime = time_spent(pureBegin, pureEnd);

    free_string(filename);
}

void display_timing_results(double *results, unsigned int streamCount, int* streams, unsigned int implementationCount, OutputStream* oss) {
  int i;
  int j;
  int offset;

  for (j = 0; j < implementationCount; j++) {
    printf(" & %c", oss[j].code);
  }
  printf("\\\\");
  logNewLine();

  for(i = 0; i < streamCount; i++) {
    printf("%d", streams[i]);
    for (j = 0; j < implementationCount; j++) {
      offset = (implementationCount * i) + j;
      printf(" & %06.4lfs", *(results + offset));
    }
    printf("\\\\");
    logNewLine();
  }
}

void do_read_and_write_benchmark(BOOL includeA, unsigned long writes) {
  unsigned int implementationCount = 4;
  unsigned int streamCount = 1;
  unsigned int streamFactor = 1;
  unsigned int repetitions = 1;
  int i;
  int j;
  int r;
  int offset;
  int writeCount;
  int readCount;
  double writeValue;
  double readValue;
  int pureWriteCount;
  int pureReadCount;
  double pureWriteValue;
  double pureReadValue;
  InputStream iss[4];
  OutputStream oss[4];
  int* streams = calloc(streamCount, sizeof(int));
  double* writeResults = calloc(streamCount * implementationCount, sizeof(double));
  double* readResults = calloc(streamCount * implementationCount, sizeof(double));
  double* pureWriteResults = calloc(streamCount * implementationCount, sizeof(double));
  double* pureReadResults = calloc(streamCount * implementationCount, sizeof(double));

  iss[0] = is_a;
  iss[1] = is_b;
  iss[2] = is_c;
  iss[3] = is_d;

  oss[0] = os_a;
  oss[1] = os_b;
  oss[2] = os_c;
  oss[3] = os_d;

  for(i = 0; i < streamCount; i++) {
    streams[i] = streamFactor;
    streamFactor *= 4;
  }

  logInfoLine(fI("Running Benchmark for block size factor: %d", block_size_factor()));
  if(!includeA) {
    logWarningLine("Results for A will not be calculated! ");
  }

  for(i = 0; i < streamCount; i++) {
    logInfoLine(fI("Calculating for stream count: %d", streams[i]));
    for (j = 0; j < implementationCount; j++) {
      offset = (implementationCount * i) + j;

      *(writeResults + offset) = 0;
      *(readResults + offset) = 0;
      *(pureWriteResults + offset) = 0;
      *(pureReadResults + offset) = 0;

      if(j == 0 && !includeA) {
        continue;
      }
      else if(j == 3 && !is_stream_d_valid()) {
        continue;
      }

      writeCount = 0;
      readCount = 0;
      pureWriteCount = 0;
      pureReadCount = 0;
      for(r = 0; r < repetitions; r++) {
        writeValue = 0;
        readValue = 0;
        pureWriteValue = 0;
        pureReadValue = 0;
        write_and_read_timed(iss[j], oss[j], writes, streams[i], &writeValue, &readValue, &pureWriteValue, &pureReadValue);

        if(writeValue >= 0) {
          *(writeResults + offset) += writeValue;
          ++writeCount;
        }

        if(readValue >= 0) {
          *(readResults + offset) += readValue;
          ++readCount;
        }

        if(pureWriteValue >= 0) {
          *(pureWriteResults + offset) += pureWriteValue;
          ++pureWriteCount;
        }

        if(pureReadValue >= 0) {
          *(pureReadResults + offset) += pureReadValue;
          ++pureReadCount;
        }
      }

      if(writeCount > 0) {
        *(writeResults + offset) /= writeCount;
      }
      else {
        *(writeResults + offset) = -1;
      }
      if(readCount > 0) {
        *(readResults + offset) /= readCount;
      }
      else {
        *(readResults + offset) = -1;
      }
      if(pureWriteCount > 0) {
        *(pureWriteResults + offset) /= pureWriteCount;
      }
      else {
        *(pureWriteResults + offset) = -1;
      }
      if(pureReadCount > 0) {
        *(pureReadResults + offset) /= pureReadCount;
      }
      else {
        *(pureReadResults + offset) = -1;
      }
    }
  }

  logInfoLine(fI("Benchmark results for block size factor: %d", block_size_factor()));
  if(!includeA) {
    logWarningLine("Results for A were not calculated! ");
  }

  printf("Write results: ");
  logNewLine();
  display_timing_results(writeResults, streamCount, streams, implementationCount, oss);

  printf("Read results: ");
  logNewLine();
  display_timing_results(readResults, streamCount, streams, implementationCount, oss);

  printf("Pure Write results: ");
  logNewLine();
  display_timing_results(pureWriteResults, streamCount, streams, implementationCount, oss);

  printf("Pure Read results: ");
  logNewLine();
  display_timing_results(pureReadResults, streamCount, streams, implementationCount, oss);

  free(streams);
  free(writeResults);
  free(readResults);
  free(pureWriteResults);
  free(pureReadResults);
}

void experiment_merge_sort(InputStream is, OutputStream os, int N, int M, int d){
    int i;
    ELEMENT elt;
    ELEMENT* element;
    clock_t begin;
    clock_t end;
    double time_spent;
    ELEMENT* check;

    HANDLE input;
    HANDLE output;
    HANDLE result;

    check = (ELEMENT*) calloc(N, sizeof(ELEMENT));

    output = (*os.create)(fC("./experiment-output-%c.txt", os.code));
    input = (*os.create)(fC("./experiment-input-%c.txt", os.code));

    for(i = 0; i < N; i++){
        elt = random_element();
        check[i] = elt;
        (*os.write)(input, elt);
    }

    quicksort(check, N);

    (*os.close)(input);

    input = (*is.open)(fC("./experiment-input-%c.txt", os.code));

    #ifdef _WIN32
    //for stream B, we have to set the maximum files that can be opened by fopen (normally only 512, can be increased up to 2048)
    _setmaxstdio(1100);
    #endif

    begin = clock();

  my_fun_mergesort(input, output, N, M, d, is, os);

    end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;

    (*is.close)(input);
    (*os.close)(output);

    //check that it is sorted well
    result = (*is.open)(fC("./experiment-output-%c.txt", os.code));


    for(i = 0; i < N; i++) {
        element = (*is.read_next)(result);

        assert_elements_equal_with_message("Merge sort should sort in ascending order: ", check[i], *element);

        free_element(element);
    }

    (*is.close)(result);

    free(check);

    logInfoLine(fC("Tested my_fun_merge sort for stream %c", os.code));
    logInfoLine(fD("Time spent on my_fun_merge sort: %lf seconds", time_spent));
}
