//
// Created by Karen on 27/09/2016.
//

#include "element.h"
#include "inputStream.h"
#include "outputStream.h"

#ifndef IO_ALG_SORT_H
#define IO_ALG_SORT_H

void quicksort(ELEMENT* array, int n);
void my_fun_heapsort(ELEMENT *array, int n);

#endif //IO_ALG_SORT_H
