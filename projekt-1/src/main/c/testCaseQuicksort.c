//
// Created by Karen on 27/09/2016.
//
#include <stdio.h>
#include "sort.h"
#include "testDeclarations.h"

void test_quicksort() {
    int i, n = 10;
    ELEMENT array[10] = {element_for_number(8),
                         element_for_number(6),
                         element_for_number(4),
                         element_for_number(1),
                         element_for_number(9),
                         element_for_number(3),
                         element_for_number(7),
                         element_for_number(5),
                         element_for_number(10),
                         element_for_number(2)};

    quicksort(array, n);

    for(i = 0; i < n; i++) {
      assert_elements_equal_with_message("Quicksorted elements should be in ascending order: ", element_for_number(i + 1), array[i]);
    }
}
