#include <stdlib.h>

#include "data.h"
#include "util.h"
#include "dataTypes.h"

DATA create_blank_data() {
  return (DATA)malloc(sizeof(Data));
}

DATA create_element_data() {
  DATA data = create_blank_data();

  data->byteSize = element_byte_size();
  data->data = malloc(data->byteSize);

  return data;
}

void free_element_data(DATA data) {
  if(data != NULL) {
    if(data->data != NULL) {
      free(data->data);
      data->data = NULL;
    }
    else {
      logErrorLine("DATA->data was null in free_element_data");
    }

    free_data(data);
  }
  else {
    logErrorLine("DATA was null in free_element_data");
  }
}


void free_data(DATA data) {
  if(data != NULL) {
    free(data);
  }
  else {
    logErrorLine("DATA was null in free_data");
  }
}
