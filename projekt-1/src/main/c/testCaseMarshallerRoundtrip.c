#ifndef TEST_CASE_MARSHALLER_ROUNDTRIP_C
#define TEST_CASE_MARSHALLER_ROUNDTRIP_C

#include "dataTypes.h"
#include "testDeclarations.h"

void test_marshaller_roundtrip(ELEMENT expected) {
  ELEMENT* actual = create_blank_element();
  int elementSize = element_byte_size();
  DATA data = create_element_data();

  marshal(data, &expected);
  unmarshal(actual, data);

  assert_elements_equal(expected, *actual);

  free_element_data(data);
}

#endif /* TEST_CASE_MARSHALLER_ROUNDTRIP_C */
