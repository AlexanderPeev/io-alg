//
// Created by Karen on 27/09/2016.
//
#include <math.h>
#include <stdio.h>
#include <string.h>
#include "sort.h"
#include "dataTypes.h"
#include "outputStream.h"
#include "inputStream.h"

void shiftDown(ELEMENT* array, int start, int end);
void swapElements(ELEMENT* array, int i, int j);
int rightChild(int i);
int leftChild(int i);
int parent(int i);
void heapify(ELEMENT* array, int n);


//sorts an array using the quicksort algorithm
void quicksort(ELEMENT* array, int n){
    int i = 0;
    int j;
    int randomPivotIndex, end = n - 1;
    ELEMENT pivot;
    srand(time(NULL));

    if(n > 1) {
        //chose a random element, the pivot
        randomPivotIndex = rand()%n;

        pivot = array[randomPivotIndex];
        //place the pivot at the end
        swapElements(array, randomPivotIndex, end);

        //reposition the elements
        for (j = 0; j < n; j++) {
            if(array[j] < pivot) {
                //switch i and j
                swapElements(array, i, j);
                i++; // go to next partition slot
            }
        }

        //switch element i (the next partition slot) and the pivot, then the pivot is at position i
        swapElements(array, i, end);

        //sort the left and right parts
        quicksort(array, i); // exclude our pivot by giving its index as the length
        quicksort(array+i+1, n-i-1);
    }
}

void my_fun_heapsort(ELEMENT *array, int n){
    int end;

    //build a heap
    heapify(array, n);

    end = n-1;

    while(end > 0){
        swapElements(array, end, 0);
        end--;
        shiftDown(array, 0, end);
    }

}

//puts the elements of array in heap order
void heapify(ELEMENT* array, int n){
    int start = parent(n-1);

    while(start >= 0){
        shiftDown(array, start, n-1);
        start--;
    }
}

void shiftDown(ELEMENT* array, int start, int end){
    int root = start;
    int child, swap;

    while(leftChild(root) <= end){
        child = leftChild(root);
        swap = root;

        if(array[swap] < array[child]){
            swap = child;
        }
        if(child+1 <= end && array[swap] < array[child+1]){
            swap = child+1;
        }
        if(swap == root){
            return;
        } else{
            swapElements(array, root, swap);
            root = swap;
        }
    }
}

int parent(int i){
    return floor((i-1) / 2);
}

int leftChild(int i){
    return 2*i + 1;
}

int rightChild(int i){
    return 2*i + 2;
}

void swapElements(ELEMENT* array, int i, int j){
    ELEMENT tmp = array[i];
    array[i] = array[j];
    array[j] = tmp;
}


