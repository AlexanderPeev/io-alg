#ifndef HANDLE_H
#define HANDLE_H

#include <stdio.h>

#include "bool.h"
#include "data.h"

#define HANDLE Handle*
#define HANDLES Handles*
#define SYSTEM_HANDLE SystemHandle*
#define FILE_HANDLE FileHandle*
#define MAP_HANDLE MapHandle*
#define HANDLE_ERROR HandleError*

#define ERROR_CREATE_FAILED_BLANK_RESULT 1
#define ERROR_CREATE_FAILED 2
#define ERROR_WRITE_FAILED 3
#define ERROR_OUTPUT_CLOSE_FAILED 4
#define ERROR_OPEN_FAILED 5
#define ERROR_READ_FAILED 6
#define ERROR_INPUT_CLOSE_FAILED 7
#define ERROR_MAP_FAILED 8
#define ERROR_UNMAP_FAILED 9
#define ERROR_SEEK_FAILED 10
#define ERROR_STRETCH_FAILED 11

typedef struct {
  int handle;
  char* buffer;
  long offset;
  long loaded;
  BOOL eof;
  long blocksRead;
  long fileSize;
  long bytesRead;
} SystemHandle;

typedef struct {
  FILE* file;
  char* buffer;
  long offset;
  BOOL eof;
  long fileSize;
} FileHandle;

typedef struct {
  int handle;
  char* map;
  long blocksRead;
  long offset;
  long loaded;
  BOOL eof;
  long fileSize;
} MapHandle;

typedef struct {
  SYSTEM_HANDLE handle;
  FILE_HANDLE fileHandle;
  MAP_HANDLE mapHandle;
  DATA data;
} Handle;

typedef struct {
  int total;
  HANDLE* handles;
} Handles;

typedef struct {
    int errorNumber;
    int code;
    char* text;
} HandleError;

void raise_handle_error(int errorNumber, int code, char* text);
void raise_handle_error_with_number(int code, char* text);

BOOL has_handle_error();

HANDLE_ERROR get_handle_error();

void reset_handle_error();

HANDLE_ERROR create_blank_handle_error();

void free_handle_error(HANDLE_ERROR handleError);

HANDLES create_blank_handles();

HANDLES create_handles(int total);

void free_blank_handles(HANDLES handles);

void free_handles(HANDLES handles);

HANDLE* create_handle_array(int total);

HANDLE* extend_handle_array(HANDLE* handleArray, int newTotal);

void free_handle_array(HANDLE* handleArray);

HANDLE create_blank_handle();

void free_handle(HANDLE handle);

SYSTEM_HANDLE create_system_handle();

void free_system_handle(SYSTEM_HANDLE handle);

FILE_HANDLE create_file_handle();

void free_file_handle(FILE_HANDLE handle);

MAP_HANDLE create_map_handle();

void free_map_handle(MAP_HANDLE handle);

#endif /* HANDLE_H */
