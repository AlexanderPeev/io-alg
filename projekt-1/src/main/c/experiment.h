//
// Created by Karen on 1/10/2016.
//

#include "inputStream.h"
#include "outputStream.h"

#ifndef IO_ALG_EXPERIMENT_H
#define IO_ALG_EXPERIMENT_H

void do_read_and_write_benchmark(BOOL includeA, unsigned long writes);
void experiment_merge_sort(InputStream is, OutputStream os, int N, int M, int d);
double benchmark_merge_sorts(InputStream is, OutputStream os, char* fileName, int runs, int N, int M, int d);
double benchmark_merge_sort(InputStream is, OutputStream os, char* fileName, int N, int M, int d);
double benchmark_sorting_algorithms(char* fileName, InputStream is, int runs, int N, void (*sort)(ELEMENT*, int));
double benchmark_sorting_algorithm(char* fileName, InputStream is, int N, void (*sort)(ELEMENT*, int));

#endif //IO_ALG_EXPERIMENT_H
