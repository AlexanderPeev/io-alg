[34mStarting test run. [0m
[34mTesting marshaller... [0m
[34mStarting tests for marshaller... [0m
[34mFinished with tests for marshaller... [0m
[34mTesting implementations... [0m
[34mStarting tests for implementation (InputStream A, OutputStream A)[0m
[34mStarting ROUNDTRIP test for implementation (InputStream A, OutputStream A)[0m
[34mFinished with ROUNDTRIP test for implementation (InputStream A, OutputStream A)[0m
[34mStarting DUAL ROUNDTRIP test for implementation (InputStream A, OutputStream A)[0m
[34mFinished with DUAL ROUNDTRIP test for implementation (InputStream A, OutputStream A)[0m
[34mStarting MULTIPLE STREAMS test for implementation (InputStream A, OutputStream A)[0m
[34mWriting is done, switching to reading: MULTIPLE STREAMS test for implementation (InputStream A, OutputStream A)[0m
[34mFinished with MULTIPLE STREAMS test for implementation (InputStream A, OutputStream A)[0m
[34mStarting MERGE-ONE test for implementation (InputStream A, OutputStream A)[0m
[34mFinished with MERGE-ONE test for implementation (InputStream A, OutputStream A)[0m
[34mStarting MERGE test for implementation (InputStream A, OutputStream A)[0m
[34mFinished with MERGE test for implementation (InputStream A, OutputStream A)[0m
[34mStarting MERGESORT test for implementation (InputStream A, OutputStream A)[0m
[34mFinished with MERGESORT test for implementation (InputStream A, OutputStream A)[0m
[34mFinished with tests for implementation (InputStream A, OutputStream A)[0m

[34mStarting tests for implementation (InputStream B, OutputStream B)[0m
[34mStarting ROUNDTRIP test for implementation (InputStream B, OutputStream B)[0m
[34mFinished with ROUNDTRIP test for implementation (InputStream B, OutputStream B)[0m
[34mStarting DUAL ROUNDTRIP test for implementation (InputStream B, OutputStream B)[0m
[34mFinished with DUAL ROUNDTRIP test for implementation (InputStream B, OutputStream B)[0m
[34mStarting MULTIPLE STREAMS test for implementation (InputStream B, OutputStream B)[0m
[34mWriting is done, switching to reading: MULTIPLE STREAMS test for implementation (InputStream B, OutputStream B)[0m
[34mFinished with MULTIPLE STREAMS test for implementation (InputStream B, OutputStream B)[0m
[34mStarting MERGE-ONE test for implementation (InputStream B, OutputStream B)[0m
[34mFinished with MERGE-ONE test for implementation (InputStream B, OutputStream B)[0m
[34mStarting MERGE test for implementation (InputStream B, OutputStream B)[0m
[34mFinished with MERGE test for implementation (InputStream B, OutputStream B)[0m
[34mStarting MERGESORT test for implementation (InputStream B, OutputStream B)[0m
[34mFinished with MERGESORT test for implementation (InputStream B, OutputStream B)[0m
[34mFinished with tests for implementation (InputStream B, OutputStream B)[0m

[34mStarting tests for implementation (InputStream C, OutputStream C)[0m
[34mStarting ROUNDTRIP test for implementation (InputStream C, OutputStream C)[0m
[34mFinished with ROUNDTRIP test for implementation (InputStream C, OutputStream C)[0m
[34mStarting DUAL ROUNDTRIP test for implementation (InputStream C, OutputStream C)[0m
[34mFinished with DUAL ROUNDTRIP test for implementation (InputStream C, OutputStream C)[0m
[34mStarting MULTIPLE STREAMS test for implementation (InputStream C, OutputStream C)[0m
[34mWriting is done, switching to reading: MULTIPLE STREAMS test for implementation (InputStream C, OutputStream C)[0m
[34mFinished with MULTIPLE STREAMS test for implementation (InputStream C, OutputStream C)[0m
[34mStarting MERGE-ONE test for implementation (InputStream C, OutputStream C)[0m
[34mFinished with MERGE-ONE test for implementation (InputStream C, OutputStream C)[0m
[34mStarting MERGE test for implementation (InputStream C, OutputStream C)[0m
[34mFinished with MERGE test for implementation (InputStream C, OutputStream C)[0m
[34mStarting MERGESORT test for implementation (InputStream C, OutputStream C)[0m
[34mFinished with MERGESORT test for implementation (InputStream C, OutputStream C)[0m
[34mFinished with tests for implementation (InputStream C, OutputStream C)[0m

[34mStarting tests for implementation (InputStream D, OutputStream D)[0m
[34mStarting ROUNDTRIP test for implementation (InputStream D, OutputStream D)[0m
[34mFinished with ROUNDTRIP test for implementation (InputStream D, OutputStream D)[0m
[34mStarting DUAL ROUNDTRIP test for implementation (InputStream D, OutputStream D)[0m
[34mFinished with DUAL ROUNDTRIP test for implementation (InputStream D, OutputStream D)[0m
[34mStarting MULTIPLE STREAMS test for implementation (InputStream D, OutputStream D)[0m
[34mWriting is done, switching to reading: MULTIPLE STREAMS test for implementation (InputStream D, OutputStream D)[0m
[34mFinished with MULTIPLE STREAMS test for implementation (InputStream D, OutputStream D)[0m
[34mStarting MERGE-ONE test for implementation (InputStream D, OutputStream D)[0m
[34mFinished with MERGE-ONE test for implementation (InputStream D, OutputStream D)[0m
[34mStarting MERGE test for implementation (InputStream D, OutputStream D)[0m
[34mFinished with MERGE test for implementation (InputStream D, OutputStream D)[0m
[34mStarting MERGESORT test for implementation (InputStream D, OutputStream D)[0m
[34mFinished with MERGESORT test for implementation (InputStream D, OutputStream D)[0m
[34mFinished with tests for implementation (InputStream D, OutputStream D)[0m

[34mTesting Priority Queue... [0m
[34mStarting Priority Queue test[0m
[34mFinished with Priority Queue test[0m
[34mTesting Quick Sort... [0m
[34mTesting Heap Sort... [0m
[34mTesting Stream Implementation Benchmark... [0m
[34mRunning Benchmark for block size factor: 1[0m
[34mCalculating for stream count: 1[0m
[34mCalculating for stream count: 2[0m
[34mCalculating for stream count: 4[0m
[34mCalculating for stream count: 8[0m
[34mCalculating for stream count: 16[0m
[34mCalculating for stream count: 32[0m
[34mCalculating for stream count: 64[0m
[34mCalculating for stream count: 128[0m
[34mCalculating for stream count: 256[0m
[34mCalculating for stream count: 512[0m
[34mCalculating for stream count: 1024[0m

[34mBenchmark results for block size factor: 1[0m
Write results: 
 & A & B & C & D\\
1 & 0.0173s & 0.0006s & 0.0006s & 0.0006s\\
2 & 0.0348s & 0.0013s & 0.0011s & 0.0013s\\
4 & 0.0697s & 0.0025s & 0.0022s & 0.0025s\\
8 & 0.1407s & 0.0051s & 0.0044s & 0.0050s\\
16 & 0.2791s & 0.0101s & 0.0087s & 0.0099s\\
32 & 0.5572s & 0.0204s & 0.0175s & 0.0198s\\
64 & 1.1131s & 0.0407s & 0.0350s & 0.0397s\\
128 & 2.2262s & 0.0819s & 0.0706s & 0.0798s\\
256 & 4.4404s & 0.1650s & 0.1421s & 0.1607s\\
512 & 8.8941s & 0.3299s & 0.2852s & 0.3210s\\
1024 & -1.0000s & -1.0000s & -1.0000s & -1.0000s\\
2048 & -1.0000s & -1.0000s & -1.0000s & -1.0000s\\
Read results: 
 & A & B & C & D\\
1 & 0.0075s & 0.0008s & 0.0008s & 0.0008s\\
2 & 0.0151s & 0.0017s & 0.0016s & 0.0016s\\
4 & 0.0303s & 0.0033s & 0.0032s & 0.0031s\\
8 & 0.0604s & 0.0066s & 0.0064s & 0.0062s\\
16 & 0.1208s & 0.0132s & 0.0126s & 0.0123s\\
32 & 0.2416s & 0.0264s & 0.0253s & 0.0247s\\
64 & 0.4826s & 0.0528s & 0.0509s & 0.0500s\\
128 & 0.9636s & 0.1063s & 0.1022s & 0.0992s\\
256 & 1.9285s & 0.2129s & 0.2043s & 0.1989s\\
512 & 3.8468s & 0.4273s & 0.4093s & 0.4002s\\
1024 & -1.0000s & -1.0000s & -1.0000s & -1.0000s\\
2048 & -1.0000s & -1.0000s & -1.0000s & -1.0000s\\


Write results: 
 & A & B & C & D\\
1 & 0.0000s & 0.0006s & 0.0006s & 0.0006s\\
2 & 0.0000s & 0.0013s & 0.0011s & 0.0012s\\
4 & 0.0000s & 0.0025s & 0.0022s & 0.0024s\\
8 & 0.0000s & 0.0051s & 0.0044s & 0.0047s\\
16 & 0.0000s & 0.0103s & 0.0087s & 0.0095s\\
32 & 0.0000s & 0.0204s & 0.0173s & 0.0190s\\
64 & 0.0000s & 0.0409s & 0.0348s & 0.0381s\\
128 & 0.0000s & 0.0818s & 0.0700s & 0.0765s\\
256 & 0.0000s & 0.1643s & 0.1405s & 0.1532s\\
512 & 0.0000s & 0.3297s & 0.2815s & 0.3070s\\
1024 & 0.0000s & -1.0000s & -1.0000s & -1.0000s\\
2048 & 0.0000s & -1.0000s & -1.0000s & -1.0000s\\
Read results: 
 & A & B & C & D\\
1 & 0.0000s & 0.0008s & 0.0008s & 0.0008s\\
2 & 0.0000s & 0.0017s & 0.0016s & 0.0015s\\
4 & 0.0000s & 0.0033s & 0.0032s & 0.0031s\\
8 & 0.0000s & 0.0066s & 0.0067s & 0.0061s\\
16 & 0.0000s & 0.0133s & 0.0127s & 0.0122s\\
32 & 0.0000s & 0.0266s & 0.0254s & 0.0244s\\
64 & 0.0000s & 0.0532s & 0.0510s & 0.0487s\\
128 & 0.0000s & 0.1062s & 0.1018s & 0.0976s\\
256 & 0.0000s & 0.2127s & 0.2049s & 0.1953s\\
512 & 0.0000s & 0.4255s & 0.4089s & 0.3909s\\
1024 & 0.0000s & -1.0000s & -1.0000s & -1.0000s\\
2048 & 0.0000s & -1.0000s & -1.0000s & -1.0000s\\

Write results: 
 & A & B & C & D\\
1 & 0.0000s & 0.0006s & 0.0006s & 0.0006s\\
2 & 0.0000s & 0.0013s & 0.0011s & 0.0012s\\
4 & 0.0000s & 0.0025s & 0.0022s & 0.0024s\\
8 & 0.0000s & 0.0051s & 0.0043s & 0.0048s\\
16 & 0.0000s & 0.0102s & 0.0086s & 0.0094s\\
32 & 0.0000s & 0.0203s & 0.0172s & 0.0188s\\
64 & 0.0000s & 0.0409s & 0.0347s & 0.0378s\\
128 & 0.0000s & 0.0819s & 0.0697s & 0.0762s\\
256 & 0.0000s & 0.1643s & 0.1403s & 0.1525s\\
512 & 0.0000s & 0.3292s & 0.2801s & 0.3063s\\
1024 & 0.0000s & -1.0000s & -1.0000s & -1.0000s\\
2048 & 0.0000s & -1.0000s & -1.0000s & -1.0000s\\
Read results: 
 & A & B & C & D\\
1 & 0.0000s & 0.0008s & 0.0008s & 0.0008s\\
2 & 0.0000s & 0.0017s & 0.0016s & 0.0015s\\
4 & 0.0000s & 0.0033s & 0.0032s & 0.0031s\\
8 & 0.0000s & 0.0066s & 0.0063s & 0.0061s\\
16 & 0.0000s & 0.0132s & 0.0126s & 0.0121s\\
32 & 0.0000s & 0.0263s & 0.0254s & 0.0244s\\
64 & 0.0000s & 0.0531s & 0.0511s & 0.0487s\\
128 & 0.0000s & 0.1062s & 0.1019s & 0.0972s\\
256 & 0.0000s & 0.2129s & 0.2037s & 0.1945s\\
512 & 0.0000s & 0.4245s & 0.4069s & 0.3923s\\
1024 & 0.0000s & -1.0000s & -1.0000s & -1.0000s\\
2048 & 0.0000s & -1.0000s & -1.0000s & -1.0000s\\


Write results: 
 & A & B & C & D\\
1 & 0.0000s & 0.0006s & 0.0005s & 0.0006s\\
2 & 0.0000s & 0.0013s & 0.0011s & 0.0012s\\
4 & 0.0000s & 0.0025s & 0.0021s & 0.0023s\\
8 & 0.0000s & 0.0051s & 0.0043s & 0.0046s\\
16 & 0.0000s & 0.0103s & 0.0086s & 0.0093s\\
32 & 0.0000s & 0.0203s & 0.0172s & 0.0185s\\
64 & 0.0000s & 0.0408s & 0.0347s & 0.0372s\\
128 & 0.0000s & 0.0820s & 0.0698s & 0.0749s\\
256 & 0.0000s & 0.1642s & 0.1401s & 0.1504s\\
512 & 0.0000s & 0.3293s & 0.2809s & 0.3015s\\
1024 & 0.0000s & -1.0000s & -1.0000s & -1.0000s\\
2048 & 0.0000s & -1.0000s & -1.0000s & -1.0000s\\
Read results: 
 & A & B & C & D\\
1 & 0.0000s & 0.0008s & 0.0008s & 0.0008s\\
2 & 0.0000s & 0.0017s & 0.0016s & 0.0015s\\
4 & 0.0000s & 0.0033s & 0.0032s & 0.0030s\\
8 & 0.0000s & 0.0066s & 0.0063s & 0.0061s\\
16 & 0.0000s & 0.0136s & 0.0126s & 0.0121s\\
32 & 0.0000s & 0.0264s & 0.0253s & 0.0241s\\
64 & 0.0000s & 0.0532s & 0.0510s & 0.0485s\\
128 & 0.0000s & 0.1063s & 0.1020s & 0.0969s\\
256 & 0.0000s & 0.2122s & 0.2038s & 0.1939s\\
512 & 0.0000s & 0.4246s & 0.4078s & 0.3881s\\
1024 & 0.0000s & -1.0000s & -1.0000s & -1.0000s\\
2048 & 0.0000s & -1.0000s & -1.0000s & -1.0000s\\

Result input size 10000
& 100 & 1000 & 10000 & 10000 \\
100 & 0.006380 & 0.006381 & 0.006382 & 0.006383 \\
1000 & 0.006384 & 0.006385 & 0.006386 & 0.006387 \\
10000 & 0.006390 & 0.006393 & 0.006395 & 0.006397 \\

Result input size 100000
& 100 & 1000 & 10000 & 10000 \\
100 & 0.076253 & 0.076254 & 0.076255 & 0.076256 \\
1000 & 0.076257 & 0.076257 & 0.076258 & 0.076259 \\
10000 & 0.076264 & 0.076266 & 0.076268 & 0.076271 \\

Result input size 1000000
& 100 & 1000 & 10000 & 10000 \\
& 0.655687 & 0.655687 & 0.655688 & 0.655689 \\
1000 & 0.655693 & 0.655696 & 0.655699 & 0.655702 \\
10000 & 0.655707 & 0.655710 & 0.655712 & 0.655715 \\

[34mFinished with test run. [0m
