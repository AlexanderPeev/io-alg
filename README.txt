 --- Create IntelliJ IDEA Project Files ---
gradlew idea

 --- Projekt 1 ---

BUILD:
gradlew projekt-1:build

RUN (Windows):
gradlew projekt-1:runWindows
 -- OR --
gradlew projekt-1:runWindows > ./run-log.txt


RUN (Linux):
gradlew projekt-1:runLinux
 -- OR --
gradlew projekt-1:runLinux > ./run-log.txt


 --- Projekt 2 ---

BUILD:
gradlew projekt-2:build --stacktrace

RUN (Windows):
gradlew projekt-2:runWindows
 -- OR --
gradlew projekt-2:runWindows > ./run-log.txt


RUN (Linux):
gradlew projekt-2:runLinux
 -- OR --
gradlew projekt-2:runLinux > ./run-log.txt


SETUP cgroup:
docker run --rm=false --cap-add SYS_ADMIN -it -v /run -v /run/lock -v /sys/fs/cgroup:/sys/fs/cgroup:ro alexanderpeev/j-dev-cgroup:latest
THEN, RUN IN CONTAINER:
    cgcreate -g memory:/ioAlg && \
    echo $(( 5 * 1024 * 1024 )) > /sys/fs/cgroup/memory/ioAlg/memory.limit_in_bytes && \
    echo $(( 2 * 1024 * 1024 * 1024 )) > /sys/fs/cgroup/memory/ioAlg/memory.memsw.limit_in_bytes && \
    echo '#!/bin/bash' > /run-with-memory-limit.sh && \
    echo "" > /run-with-memory-limit.sh && \
    echo "cgexec -g memory:ioAlg /home/io-alg/projekt-2/run/io_alg_projekt-2" >> /run-with-memory-limit.sh && \
    chmod 0777 /run-with-memory-limit.sh
GET LAST CONTAINER NAME:
docker ps --all
COMMIT IT:
docker commit CONTAINER alexanderpeev/j-dev-cgroup-ioalg:latest
PUSH IT:
docker push alexanderpeev/j-dev-cgroup-ioalg:latest

RUN cgroup:
docker run --rm=false --cap-add SYS_ADMIN -it -v /run -v /run/lock -v /sys/fs/cgroup:/sys/fs/cgroup:ro alexanderpeev/io-alg:latest

